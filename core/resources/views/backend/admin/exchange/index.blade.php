@extends('backend.master')
@section('title',"Reservation Title")
@section('content')

    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}</h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Date</th>
                    <th>Transaction ID</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($data as $k => $value)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>
                        <a href="{{route('member.view',$value->from_user_id)}}">{{$value->from_first_name}} {{$value->from_last_name}}</a>
                      </td>
                      <td>
                        <a href="{{route('member.view',$value->to_user_id)}}">{{$value->to_first_name}} {{$value->to_last_name}}</a>
                      </td>
                      <td>
                        {{Carbon\Carbon::parse($value->date)->format('D jS M Y')}}
                      </td>
                      <td>{{$value->transaction_id}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $reservation->render() }} --}}
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.status').change(function(){
              window.location.href = '{{url('reservation-changestatus')}}/'+this.value;
              // $.ajax({
              //   url: '{{url('reservation-changestatus')}}/'+this.value,
              //   type: 'GET',
              //   dataType:'html',
              //   success : function (response) {
              //     if(response!=""){
              //
              //     }
              //   }
              // });
            })
        });

    </script>
@endsection
