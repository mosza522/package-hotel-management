@extends('backend.master')
@section('title',"Payment Title")
@section('content')

    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}</h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>User</th>
                    <th>Payment Id</th>
                    <th>Amount</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($payment as $k => $data)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>{{$data->first_name}} {{$data->last_name}}</td>
                      <td>{{$data->payment_id}}</td>
                      <td>{{$data->amount}}</td>
                      <td>{{$data->type}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $news->render() }} --}}
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.status').change(function(){
              window.location.href = '{{url('reservation-changestatus')}}/'+this.value;
            })
        });

    </script>
@endsection
