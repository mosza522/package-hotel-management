@extends('backend.master')
@section('title',"Transfer")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')
    {{-- {{dd(session()->all())}} --}}
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
      @if($deposite)
        <form role="form" method="POST" action="{{route('payment.deposite.update')}}" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-body">
                <input type="hidden" name="id" value="{{$deposite->id}}">
                    <div class="form-group">
                        <h5> ขันต่ำของการจ่ายมัดจำในการซื้อแพ็กเกจ (เปอร์เซนต์)</h5>
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg" value="{{$deposite->deposite}}"
                                   name="deposite">
                        </div>
                        @if ($errors->has('deposite'))
                            <div class="alert alert-danger">{{ $errors->first('deposite') }}</div>
                        @endif

                    </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Update</button>
            </div>

        </form>
      @else
        <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-body">
              <div class="form-group">
                  <h5> ขันต่ำของการจ่ายมัดจำในการซื้อแพ็กเกจ (เปอร์เซนต์)</h5>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-lg" value="" placeholder="50%"
                             name="deposite">
                  </div>
                  @if ($errors->has('deposite'))
                      <div class="alert alert-danger">{{ $errors->first('deposite') }}</div>
                  @endif

              </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Save</button>
            </div>

        </form>
      @endif
    </div>


@endsection

@section('import-script')
    <!-- <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script> -->
    <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script>
@stop
@section('script')
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
      bkLib.onDomLoaded(function() {
          // new nicEditor({fullPanel : true}).panelInstance('area1'); });
          new nicEditor({
              iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
              fullPanel : true
          }).panelInstance('area1');
          new nicEditor({
              iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
              fullPanel : true
          }).panelInstance('area2');
      });
  </script>
@stop
