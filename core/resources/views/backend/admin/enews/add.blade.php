@extends('backend.master')
@section('title',"E-News Title")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets\plugin\select2-bootstrap-theme\dist\select2.css')}}">
    <link rel="stylesheet" href="{{asset('assets\plugin\select2-bootstrap-theme\dist\select2-bootstrap.css')}}">
@stop
@section('content')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('admin.enews')}}" class="btn btn-success btn-md float-right">
                <i class="fa fa-eye"></i> E-News Letter
            </a>
        </div>

        <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
                <div class="form-group">
                    <h5> Subject</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('subject')}}"
                               name="subject">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-font"></i>
                            </span>
                        </div>
                    </div>
                    @if ($errors->has('subject'))
                        <div class="alert alert-danger">{{ $errors->first('subject') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> From</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="no-reply@pisona.com"
                               name="header">
                    </div>
                    @if ($errors->has('header'))
                        <div class="alert alert-danger">{{ $errors->first('header') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Attach File</h5>
                    <div class="input-group">
                        <input type="file" class="form-control form-control-lg" value="{{old('file')}}"
                               name="attach">
                    </div>
                    @if ($errors->has('attach'))
                        <div class="alert alert-danger">{{ $errors->first('attach') }}</div>
                    @endif

                </div>
                <div class="form-group">
                      <h5> Message</h5>
                      <textarea class="form-control" name="message" id="nicEdit" style="width: 100%;height: 600px"></textarea>
                  </div>

                </div>

                <div class="form-group">
                  <h5>To</h5>
                  {{-- <div class="row"> --}}
                    <div class="col-3">
                      <input type="radio" name="to_user" value="member" checked>
                      Member
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-3">
                      <input type="radio" name="to_user" value="guest">
                      Guest
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-3">
                      <input type="radio" name="to_user" value="all">
                      All
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-3">
                      <input id="some" type="radio" name="to_user" value="some">
                      some (Select below)
                    </div>
                    <div class="col-12">
                      <select id="select_email" class="select2 form-control" name="select_email[]" multiple disabled>
                        @foreach ($email as $key => $value)
                          <option value="{{$value}}">{{$value}}</option>
                        @endforeach
                      </select>
                    </div>
                  {{-- </div> --}}
                </div>
                </div>
            <div class="card-footer bg-white">
                <button class="btn btn-primary btn-block btn-lg" type="submit">Send E-News</button>
            </div>

        </form>
    </div>




@endsection
@section('script')
    <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
    <script>
        bkLib.onDomLoaded(function() {
            new nicEditor({
                iconsPath : '{{asset('assets/plugin/niceditor/nicEditorIcons.gif')}}',
                fullPanel : true
            }).panelInstance('nicEdit');
        });
        bkLib.onDomLoaded(function() {
            // new nicEditor({fullPanel : true}).panelInstance('area1'); });
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area1');
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area2');
        });
        $(document).ready(function(){
          $('input[type=radio][name=to_user]').change(function() {
              if (this.value == 'some') {
                  $('#select_email').prop('disabled',false)
              }
              else{
                  $('#select_email').prop('disabled',true)
              }
          });
        });
        $( function() {
          $( ".start_date" ).datepicker({ format: 'yyyy-mm-dd' }).val();
          $( ".end_date" ).datepicker({ format: 'yyyy-mm-dd' }).val();
        } );
    </script>
@stop
