@extends('backend.master')
@section('title',"Room management")
@section('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')

        <div class="card">
            <div class="card-header bg-white">
                <h2>Create
                    <a class="btn btn-tsk float-right" href="{{route('backend.admin.staff')}}"><i class="fa fa-list"></i> Room management</a>

                </h2>
            </div>
            <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">@csrf
            <div class="card-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <span>เลือกวันเริ่มต้น</span>
                        </div>
                        <div class="col-md-6">
                          <input type="text" autocomplete="off" class="form-control form-control-lg" name="date" id="date" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          จำนวนห้อง
                        </div>
                        <div class="col-md-6">
                          <input class="form-control " type="text" name="total_rooms" id="total_room" value="150" placeholder="จำนวนห้อง">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id='slot'></div>
              </div>
            </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-sm-12">
                        <hr/>
                        <button type="submit" class="btn btn-block btn-tsk"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>

@endsection
@section('script')
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
          $('#date').daterangepicker({
            // startDate: moment(),
            // endDate: moment(),
            minDate : moment("{{$last_date->date}}" , "YYYY/MM/DD").add(1,'day')
          },function(startDate, endDate){
            console.log($('#date').val())
          })
        });
    </script>
@endsection
