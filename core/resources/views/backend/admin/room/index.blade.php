@extends('backend.master')
@section('title',"Transfer")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')
    {{-- {{dd(session()->all())}} --}}

    <div class="card mb-4">
      <div class="card-header bg-white">
          <h2>Room Management
              <a class="btn btn-tsk float-right" href="{{route('room.create')}}"><i class="fa fa-plus"></i> Add Room</a>

          </h2>
      </div>
          <div class="card-body">

                <table class="table table-hover datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Available Rooms</th>
                        <th>Total Rooms</th>
                        <th>Update</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $k => $value)
                      {{-- <form role="form" method="POST" action="{{route('room.update')}}">
                        @csrf --}}

                      @csrf
                        <tr>
                          <td>{{$k+1}}</td>
                          <td>{{$value->date}}</td>
                          {{-- <form role="form" method="POST" action="{{route('transfer_detail.update')}}" name="editForm" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="" value="{{$value->id}}"> --}}
                            <td>
                              {{$value->total_rooms}}
                            </td>
                            <td>
                              <input class="id{{$value->id}}" type="hidden" name="id" value="{{$value->id}},{{\Auth::user()->id}}">
                              <input class="form-control all_rooms{{$value->id}}" type="text" name="all_rooms[]" value="{{$value->all_rooms}}" >
                            </td>
                            <td>
                              <button class="btn btn-warning update" type="button" data-id="{{$value->id}},{{\Auth::user()->id}}">Update</button>
                            </td>
                          {{-- </form> --}}
                          {{-- <td>{{$value->total_rooms}}</td> --}}
                        </tr>
                      {{-- </form> --}}
                    @endforeach
                    </tbody>
                </table>

              </div>
              {{-- <div class="pagination-center">
                  {{ $data->links() }}
              </div> --}}
            {{-- <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg submit" type="submit">Save</button>
            </div> --}}


    </div>


@endsection

@section('import-script')
    <!-- <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script> -->
    <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script>
@stop
@section('script')
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
  $(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    });
    var table = $('.datatable').DataTable()
    $('button').click(function(){
      var button = $(this)
      // var id_room = $(this).data("id")
      // console.log($('.total_rooms'+id_room).val())
      $(this).attr('disabled','true')
      $(this).html('<i class="fa fa-spinner" aria-hidden="true"></i>')
        $.ajax({
          url: '{{route('room.update')}}',
          data: {
            'id' : $(this).data("id"),
            'all_rooms' : $('.all_rooms'+$(this).data("id")).val()
          },
          type: 'POST',
          dataType:'json',
          success : function (response) {
            if(response.return=='success'){
              button.removeAttr('disabled')
              button.text('Update')
              toastr.success("Updated Successfully");
            }else{
              button.removeAttr('disabled')
              button.text('Update')
              $('.all_rooms'+button.data("id")).val(response.data.all_rooms)
              toastr.error(response.return);
            }
          }
        });
      // $(this).data("id")
      // var data = table.$('input').serialize();
      //   $.ajax({
      //     url: '{{route('room.update')}}',
      //     data: data,
      //     type: 'POST',
      //     dataType:'json',
      //     success : function (response) {
      //
      //     }
      //   });
    })
  })
  </script>
@stop
