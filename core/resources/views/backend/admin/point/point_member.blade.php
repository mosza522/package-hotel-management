@extends('backend.master')
@section('title',"Point Transaction")
@section('content')

    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}
                <a href="{{route('news.create')}}" class="btn btn-success btn-md float-right ">
                    <i class="fa fa-plus"></i> Add News
                </a>
            </h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>E-mail</th>
                    <th>Fullname</th>
                    <th>Point</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($list as $k => $data)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$data->email}}</td>
                        <td>{{$data->first_name}} {{$data->last_name}}</td>
                        <td>{{$data->point}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $news->render() }} --}}
        </div>
    </div>

@endsection
@section('script')

@endsection
