@extends('backend.master')
@section('title',"Point Transaction")
@section('content')

    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}
            </h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>E-mail</th>
                    <th>Fullname</th>
                    <th>Amount</th>
                    <th>After</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($list as $k => $data)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$data->email}}</td>
                        <td>{{$data->first_name}} {{$data->last_name}}</td>
                        <td>
                          <span class="badge  badge-pill  badge-{{ $data->flags == '+' ? 'success' : 'danger' }}">{{ $data->amount}}</span>
                        </td>
                        <td>{{$data->current}}</td>
                        <td>{{$data->created_at}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $news->render() }} --}}
        </div>
    </div>

@endsection
@section('script')

@endsection
