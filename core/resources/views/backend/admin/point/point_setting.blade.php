@extends('backend.master')
@section('title',"Setting Point")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')
    {{-- {{dd(session()->all())}} --}}
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
      @if($point_setting)
        <form role="form" method="POST" action="{{route('point.setting.update')}}" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-body">
                <input type="hidden" name="id" value="{{$point_setting->id}}">
                    <div class="form-group">
                      <h5> จำนวนเงินที่ใช้ต่อการได้ 1 point</h5>
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg" value="{{$point_setting->volume}}"
                                   name="volume">
                        </div>
                        @if ($errors->has('volume'))
                            <div class="alert alert-danger">{{ $errors->first('volume') }}</div>
                        @endif

                    </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Update</button>
            </div>

        </form>
      @else
        <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
                <div class="form-group">
                    <h5> จำนวนเงินที่ใช้ต่อการได้ 1 point</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('volume')}}"
                               name="volume">
                    </div>
                    @if ($errors->has('volume'))
                        <div class="alert alert-danger">{{ $errors->first('volume') }}</div>
                    @endif

                </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Save</button>
            </div>

        </form>
      @endif
    </div>


@endsection

@section('import-script')
    <!-- <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script> -->
    <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script>
@stop
@section('script')
@stop
