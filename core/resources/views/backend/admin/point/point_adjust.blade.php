@extends('backend.master')
@section('title',"Adjust Point")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets\plugin\select2-bootstrap-theme\dist\select2.css')}}">
    <link rel="stylesheet" href="{{asset('assets\plugin\select2-bootstrap-theme\dist\select2-bootstrap.css')}}">
@stop
@section('content')
    {{-- {{dd(session()->all())}} --}}
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
        <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
              <div class="form-group">
                  <h5> Member</h5>
                  <select class="select form-control form-control-lg" name="member">
                    <option value=""></option>
                  @foreach ($users as $key => $value)
                    <option value="{{$value->id}}">{{$value->username}}</option>
                  @endforeach
                  </select>
                  @if ($errors->has('amount'))
                      <div class="alert alert-danger">{{ $errors->first('amount') }}</div>
                  @endif

              </div>
              <div class="form-group">
                  <h5> Current Point</h5>
                  <div class="input-group">
                      <input type="text" id="current" class="form-control form-control-lg" value="" disabled
                             name="">
                  </div>
                </div>
                <div class="form-group">
                    <h5> Point adjust</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('amount')}}"
                               name="amount">
                    </div>
                    @if ($errors->has('amount'))
                        <div class="alert alert-danger">{{ $errors->first('amount') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Message</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('message')}}"
                               name="message">
                    </div>

                </div>

            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Save</button>
            </div>

        </form>
    </div>


@endsection

@section('import-script')
    <!-- <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script> -->
    <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script>
@stop
@section('script')
  <script type="text/javascript">
  $(document).ready(function() {
    $('.select').select2({
      placeholder: "Select Member",
      theme: "bootstrap"
    });
  });
  var point_user = {};
    <?php
    $arr = [];
    foreach($users as $key => $val){
      array_push($arr,array(
        $val['id'] => $val['point']
      ));
    }
    ?>
     var js_data = '<?php echo json_encode($users); ?>';
     var users = JSON.parse(js_data );
    console.log(users)
    // console.log(point_user)
  $('.select').change(function(){
    const point_current = users.filter((user) => {
      return user.id == $('.select').val()
    })
    console.log(point_current)
    $('#current').val(point_current[0].point)
  })
  </script>
@stop
