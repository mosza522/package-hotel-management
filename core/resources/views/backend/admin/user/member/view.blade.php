@extends('backend.master')
@section('title',"Member")
@section('style')
  <style>
  .vip {
    position: absolute;
    top:0;
    right: 0;
    background: green;
    font-weight: bold;
    padding: 5px 10px 5px 30px;
    color: white;
    border-radius:0 0 0 30px;
  }
  </style>
  <link rel="stylesheet" href="{{asset('assets\plugin\select2-bootstrap-theme\dist\select2.css')}}">
  <link rel="stylesheet" href="{{asset('assets\plugin\select2-bootstrap-theme\dist\select2-bootstrap.css')}}">
@endsection
@section('content')
  <div class="card">
    <div class="card-header bg-white">
      <h2>Member
        <a class="btn btn-tsk float-right" href="{{route('admin.member')}}"><i class="fa fa-list"></i> Member List</a>

      </h2>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="row mt-2">
          <div class="col-md-3">
            <div class="tile">
              <div class="card">
                @if($member->vip)
                  <div class="vip">VIP</div>
                @endif

                <div class="card-body text-center bg-tsk-o-1">
                  <div class="img">
                    <img src="{{$member->picture_path()}}" id="preview_img" data-oldimg="" class="img-thumbnail" style="max-height: 200px;border-radius: 50%" >
                  </div>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">User Name : </dt>
                    <dd class="col-md-6 text-md-left">{{$member->username}}</dd>
                  </dl>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">Name : </dt>
                    <dd class="col-md-6 text-md-left">{{$member->full_name}}</dd>
                  </dl>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">Email : </dt>
                    <dd class="col-md-6 text-md-left">{{$member->email}}</dd>
                  </dl>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">Phone : </dt>
                    <dd class="col-md-6 text-md-left">{{$member->phone}}</dd>
                  </dl>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">Sex : </dt>
                    <dd class="col-md-6 text-md-left">{{$member->sex()}}</dd>
                  </dl>

                  <dl class="row">
                    <dt class="col-md-6 text-md-right">Status : </dt>
                    <dd class="col-md-6 text-md-left"><span class="badge {{$member->status?'badge-success':'badge-danger'}}">{{$member->status?'ACTIVE':'INACTIVE'}}</span></dd>
                  </dl>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">User Type : </dt>
                    <dd class="col-md-6 text-md-left"><span class="badge">{{$member->user_type?$member->user_type:'NULL'}}</span></dd>
                  </dl>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">News Agreement : </dt>
                    <dd class="col-md-6 text-md-left"><span class="badge {{$member->news_agreement?'badge-success':'badge-danger'}}">{{$member->news_agreement?'AGREE':'DISAGREE'}}</span></dd>
                  </dl>
                  <dl class="row">
                    <dt class="col-md-6 text-md-right">Date Of Birth : </dt>
                    <dd class="col-md-6 text-md-left">{{$member->dob}}</dd>
                  </dl>
                  <p><strong>AGE :</strong><span class="text-muted"> {{\Carbon\Carbon::parse($member->dob)->diff(\Carbon\Carbon::now())->format('%y years, %m months and %d days')}}</span></p>
                </div>
              </div>
            </div>
            <div class="tile mt-2">
              <div class="card">
                <h4 class="text-center text-tsk">Receipt</h4>
                <div class="card-body text-center bg-tsk-o-1">
                  <div class="img">
                    <img src="{{$member->receipt_path()}}" class="img-thumbnail" style="max-height: 200px" >
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-9 ">

            <div class="tile">
              <div class="tile-body">

                <form action="{{route('member.update',$member->id)}}" method="post" enctype="multipart/form-data">@csrf
                  <div class="form-row justify-content-center">

                    <div class="form-group col-md-4">
                      <label><strong>First Name</strong> </label>
                      <input type="text" class="form-control form-control-lg" name="first_name" placeholder="First Name" value="{{$member->first_name}}">
                    </div>
                    <div class="form-group col-md-4">
                      <label><strong>Last Name</strong> </label>
                      <input type="text" class="form-control form-control-lg" name="last_name" placeholder="Last Name" value="{{$member->last_name}}">
                    </div>
                    <div class="form-group col-md-4">
                      <label><strong>Email</strong> <small class="text-danger">*</small></label>
                      <input type="email" class="form-control form-control-lg" name="email" placeholder="email" value="{{$member->email}}">
                    </div>

                  </div>
                  <div class="form-row justify-content-center">
                    <div class="form-group col-md-4">
                      <label><strong>Phone</strong> <small class="text-danger">*</small></label>
                      <input type="text" class="form-control form-control-lg" name="phone" placeholder="Phone" value="{{$member->phone}}">
                    </div>
                    <div class="form-group col-md-4">
                      <label><strong>Country</strong> <small class="text-danger">*</small></label>
                      <select id="country" class="select form-control form-control-lg" name="country" >
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label><strong>States</strong> <small class="text-danger">*</small></label>
                      <select id="states" class="select form-control form-control-lg" name="state" disabled>
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="form-row ">
                    <div class="form-group col-sm-4">
                      <label><strong>Sex</strong> <small class="text-danger">*</small></label>
                      <select  class="form-control form-control-lg" name="sex" >
                        <option value="M" {{$member->sex==='M'?'selected':''}}>Male</option>
                        <option value="F" {{$member->sex==='F'?'selected':''}}>Female</option>
                        <option value="O" {{$member->sex==='O'?'selected':''}}>Other</option>
                      </select>
                    </div>
                    <div class="form-group col-sm-4">
                      <label><strong>Image</strong></label>
                      <input type="file" class="form-control form-control-lg" name="picture">
                    </div>
                  </div>
                  <div class="form-row justify-content-center">
                    <div class="form-group col-md-12">
                      <label><strong>Remarks</strong></label>
                      <textarea  class="form-control form-control-lg" name="remarks">{{$member->remarks}}</textarea>
                    </div>

                  </div>

                  <div class="form-row justify-content-center">
                    <div class="form-group col-sm-4">
                      <label><strong>Image receipt</strong></label>
                      <input type="file" class="form-control form-control-lg" name="picture_receipt">
                    </div>
                    <div class="form-group col-md-4">
                      <label><strong>Date Of Birth</strong> <small class="text-danger">*</small></label>
                      <input type="text" class="form-control form-control-lg" name="dob" id="dob" value="{{date('Y/m/d',strtotime($member->dob))}}">
                    </div>
                    <div class="form-group col-sm-4">
                      <label for="news_agreement" class=" mr-5">News Agreement</label>
                      <input id="news_agreement"  type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" name="news_agreement" {{$member->news_agreement == 1 ? 'checked' :''}}>
                    </div>
                  </div>
                  <div class="form-row justify-content-center">
                    <div class="form-group col-sm-12">
                      <hr/>
                      <button type="reset" class="btn btn-outline-tsk"><i class="fa fa-refresh"></i> Reset</button>
                      <button type="submit" class="btn btn-tsk"><i class="fa fa-save"></i> Update</button>
                    </div>
                  </div>


                </form>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>



@endsection
@section('script')
  <script type="text/javascript">
  $(document).ready(function () {
    $('#country').select2({
      placeholder: "Select country",
      theme: "bootstrap"
    });
    $('#states').select2({
      placeholder: "Select state",
      theme: "bootstrap"
    });
    $('#dob').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'yyyy/mm/dd',
      footer: true, modal: true
    });
  });
  var authtoken = ''
  $.ajax({
    url: 'https://www.universal-tutorial.com/api/getaccesstoken',
    type: 'GET',
    headers: {
      "Accept": "application/json",
      "api-token": "3rczqv8faXeKjQKPOdtircNjO077eGXpb-Po1wUJ2Gy0oOI_NinpgHGSzpdBDTduzZg",
      "user-email": "mosza522@gmail.com"
    },
    dataType:'json',
    success : function (response) {
      authtoken = response.auth_token
      $.ajax({
        url: 'https://www.universal-tutorial.com/api/countries/',
        type: 'GET',
        headers: {
          "Accept": "application/json",
          "Authorization": 'Bearer '+authtoken,
        },
        dataType:'json',
        success : function (response) {
          for (var i = 0; i<response.length; i++){
            var opt = document.createElement('option');
            opt.value = response[i].country_name
            opt.innerHTML = response[i].country_name
            var select = document.getElementById('country');
            select.appendChild(opt);
            console.log(opt)
          }
          $('#country').val('{{$member->country}}');
          $('#country').trigger('change');
        },
      });
    },
  });
  $('#country').change(function(){
    $.ajax({
      url: 'https://www.universal-tutorial.com/api/states/'+$('#country').val(),
      type: 'GET',
      headers: {
        "Accept": "application/json",
        "Authorization": 'Bearer '+authtoken,
      },
      dataType:'json',
      success : function (response) {
        $('#states')
        .find('option')
        .remove()
        $('#states').removeAttr('disabled')
        for (var i = 0; i<response.length; i++){
          var opt = document.createElement('option');
          opt.value = response[i].state_name;
          opt.innerHTML = response[i].state_name;
          var select = document.getElementById('states');
          select.appendChild(opt);
        }
        $('#states').val('{{$member->state}}');
        $('#states').trigger('change');
      },
    });
  })
  </script>
@endsection
