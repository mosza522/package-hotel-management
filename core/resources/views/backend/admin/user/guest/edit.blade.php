@extends('backend.master')
@section('title',"Guest Title")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('admin.register')}}" class="btn btn-success btn-md float-right">
                <i class="fa fa-eye"></i> All Guest
            </a>
        </div>

        <form role="form" method="POST" action="{{route('register.update')}}" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$register->id}}">
            <div class="card-body">
                <div class="form-group">
                    <h5> Username</h5>
                        <input type="text" class="form-control form-control-lg" value="{{$register->username}}"
                               name="username">
                    @if ($errors->has('username'))
                        <div class="alert alert-danger">{{ $errors->first('username') }}</div>
                    @endif
                  </div>
                    <div class="form-group">
                        <h5> Email</h5>
                            <input type="text" class="form-control form-control-lg" value="{{$register->email}}"
                                   name="email">
                        @if ($errors->has('email'))
                            <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                        @endif
                      </div>
                      <div class="form-group">
                          <h5> First Name</h5>
                              <input type="text" class="form-control form-control-lg" value="{{$register->first_name}}"
                                     name="first_name">
                          @if ($errors->has('first_name'))
                              <div class="alert alert-danger">{{ $errors->first('first_name') }}</div>
                          @endif
                        </div>
                        <div class="form-group">
                            <h5> Last Name</h5>
                                <input type="text" class="form-control form-control-lg" value="{{$register->last_name}}"
                                       name="last_name">
                            @if ($errors->has('last_name'))
                                <div class="alert alert-danger">{{ $errors->first('last_name') }}</div>
                            @endif
                          </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Save Guest</button>
            </div>

        </form>
    </div>




@endsection
@section('script')
    <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
    <script>
        bkLib.onDomLoaded(function() {
            // new nicEditor({fullPanel : true}).panelInstance('area1'); });
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area1');
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area2');
        });
        $( function() {
          $( ".start_date" ).datepicker({ format: 'yyyy-mm-dd' }).val();
          $( ".end_date" ).datepicker({ format: 'yyyy-mm-dd' }).val();
        } );
    </script>
@stop
