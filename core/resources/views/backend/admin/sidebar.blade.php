<ul class="list-unstyled">
  <li class="{{active_menu([route('backend.admin.dashboard')],'active')}}"><a href="{{route('backend.admin.dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Dashboard </a></li>
  {{-- <li class="{{active_menu([route('backend.admin.reservation'),route('backend.admin.reservation.create')],'active')}}"><a href="{{route('backend.admin.reservation')}}"><i class="fa fa-fw fa-battery-half"></i> Reservation </a></li> --}}
  {{-- <li class="{{active_menu([route('backend.admin.guests'),route('backend.admin.guests.create')],'active',['backend.admin.guests.view'])}}"><a href="{{route('backend.admin.guests')}}"><i class="fa fa-fw fa-user-secret"></i> Guests </a></li> --}}
  @if(auth()->guard('admin')->user()->can_access('admin'))
    <li>
      <a href="#Hotel_configure" data-toggle="collapse">
        <i class="fa fa-fw fa-cubes"></i> Hotel Configuration
      </a>
      <ul id="Hotel_configure" class="list-unstyled collapse {{active_menu([
        route('backend.admin.amenities'),
        route('backend.admin.amenities.create'),
        route('backend.admin.room'),
        route('backend.admin.room.create'),
        route('backend.admin.paid_service'),
        route('backend.admin.paid_service.create'),
        route('backend.admin.coupon'),
        route('backend.admin.coupon.create'),
        route('backend.admin.room_type'),
        route('backend.admin.room_type.create'),
        route('backend.admin.floor'),
        route('backend.admin.tax'),
        route('backend.admin.tax.create'),
        route('backend.admin.floor.create'),
      ],'show',[
        'backend.admin.room_type.edit',
        'backend.admin.room_type.view',
        'backend.admin.amenities.edit',
        'backend.admin.floor.edit',
        'backend.admin.tax.edit',
        'backend.admin.paid_service.edit',
        'backend.admin.coupon.edit',
        'backend.admin.room.edit',
        ])}}">
        <li class="{{active_menu([route('backend.admin.room_type'),route('backend.admin.room_type.create')],'active',['backend.admin.room_type.edit','backend.admin.room_type.view'])}}"> <a href="{{route('backend.admin.room_type')}}"> <i class="fa fa-circle-o"></i> Room Types</a></li>
        <li class="{{active_menu([route('backend.admin.room'),route('backend.admin.room.create')],'active',['backend.admin.room.edit'])}}"> <a href="{{route('backend.admin.room')}}"> <i class="fa fa-circle-o"></i> Room</a></li>
        <li class="{{active_menu([route('backend.admin.paid_service'),route('backend.admin.paid_service.create')],'active',['backend.admin.paid_service.edit'])}}"> <a href="{{route('backend.admin.paid_service')}}"> <i class="fa fa-circle-o"></i> Paid Service</a></li>
        <li class="{{active_menu([route('backend.admin.coupon'),route('backend.admin.coupon.create')],'active',['backend.admin.coupon.edit'])}}"> <a href="{{route('backend.admin.coupon')}}"> <i class="fa fa-circle-o"></i> Coupon Master</a></li>
        <li class="{{active_menu([route('backend.admin.floor'),route('backend.admin.floor.create')],'active',['backend.admin.floor.edit'])}}"> <a href="{{route('backend.admin.floor')}}"> <i class="fa fa-circle-o"></i> Floors</a></li>
        <li class="{{active_menu([route('backend.admin.amenities'),route('backend.admin.amenities.create')],'active',['backend.admin.amenities.edit'])}}"> <a href="{{route('backend.admin.amenities')}}"> <i class="fa fa-circle-o"></i> Amenities</a></li>
        <li class="{{active_menu([route('backend.admin.tax'),route('backend.admin.tax.create')],'active',['backend.admin.tax.edit'])}}"> <a href="{{route('backend.admin.tax')}}"> <i class="fa fa-circle-o"></i> Tax</a></li>
      </ul>
    </li>
  @endif
  {{-- <li>
    <a href="#payments" data-toggle="collapse">
      <i class="fa fa-fw fa-credit-card"></i> Payment
    </a>
    <ul id="payments" class="list-unstyled collapse {{active_menu([],'show',['admin.gateway','admin.payment_log'])}}">
      <li class="{{active_menu([route('admin.gateway')],'active')}}"> <a href="{{route('admin.gateway')}}"> <i class="fa fa-credit-card"></i> Getaway</a></li>
      <li class="{{active_menu([route('admin.payment_log')],'active')}}"> <a href="{{route('admin.payment_log')}}"> <i class="fa fa-file-text"></i> Payment Log</a></li>
    </ul>
  </li> --}}
  @if(auth()->guard('admin')->user()->can_access('admin'))
    <li class="{{active_menu([route('backend.admin.staff'),route('backend.admin.staff.create')],'active',['backend.admin.staff.view'])}}"><a href="{{route('backend.admin.staff')}}"><i class="fa fa-fw fa-users"></i> Manage Staff </a></li>
  @endif
    {{-- <li>
      <a href="#Setting" data-toggle="collapse">
        <i class="fa fa-fw fa-cogs"></i>General Setting
      </a>
      <ul id="Setting" class="list-unstyled collapse {{active_menu([
        route('backend.admin.general_setting'),
        route('backend.admin.logo_and_fav_setting'),
        route('backend.admin.email_setting'),
        route('backend.admin.sms_setting')
      ],'show')}}">
      <li class="{{active_menu([route('backend.admin.general_setting')],'active')}}"> <a href="{{route('backend.admin.general_setting')}}"> <i class="fa fa-wrench"></i> General Setting</a></li>
      <li class="{{active_menu([route('backend.admin.logo_and_fav_setting')],'active')}}"><a href="{{route('backend.admin.logo_and_fav_setting')}}"><i class="fa fa-adn"></i> Logo & Favicon</a></li>
      <li class="{{active_menu([route('backend.admin.email_setting')],'active')}}"><a href="{{route('backend.admin.email_setting')}}"><i class="fa fa-envelope"></i> Email Setting</a></li>
      <li class="{{active_menu([route('backend.admin.sms_setting')],'active')}}"><a href="{{route('backend.admin.sms_setting')}}"><i class="fa fa-mobile-phone"></i> SMS Setting</a></li>
    </ul>
  </li> --}}
  {{-- <li>
    <a href="#Web_Setting" data-toggle="collapse">
      <i class="fa fa-fw fa-globe"></i>Web Setting
    </a>
    <ul id="Web_Setting" class="list-unstyled collapse {{active_menu([
      route('admin.blog'),
      route('admin.cat'),
    ],'show',['admin.web_setting.section'])}}">
    <li>
      <a href="#home" data-toggle="collapse">
        <i class="fa fa-fw fa-angle-double-down"></i> Home
      </a>
      <ul id="home" class="list-unstyled collapse {{active_menu([
        route('admin.web_setting.section',['home','banner-section']),
        route('admin.web_setting.section',['home','room-section']),
        route('admin.web_setting.section',['home','about-section']),
        route('admin.web_setting.section',['home','service-section']),
        route('admin.web_setting.section',['home','counter-section']),
        route('admin.web_setting.section',['home','testimonial-section']),
        route('admin.web_setting.section',['home','video-section']),
        route('admin.web_setting.section',['home','facility-section']),

      ],'show')}}">
      <li class="{{active_menu([route('admin.web_setting.section',['home','banner-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','banner-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Banner Area</a></li>
      <li class="{{active_menu([route('admin.web_setting.section',['home','about-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','about-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> About Area</a></li>
      <li class="{{active_menu([route('admin.web_setting.section',['home','room-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','room-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Our Room Area</a></li>
      <li class="{{active_menu([route('admin.web_setting.section',['home','service-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','service-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Service Area</a></li>
      <li class="{{active_menu([route('admin.web_setting.section',['home','counter-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','counter-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Counter Area</a></li>
      <li class="{{active_menu([route('admin.web_setting.section',['home','testimonial-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','testimonial-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Testimonial Area</a></li>
      <li class="{{active_menu([route('admin.web_setting.section',['home','video-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','video-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Video Area</a></li>
      <li class="{{active_menu([route('admin.web_setting.section',['home','facility-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['home','facility-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Our Facility Area</a></li>
    </ul>
  </li>
  <li>
    <a href="#gallery" data-toggle="collapse">
      <i class="fa fa-fw fa-angle-double-right"></i> Gallery Manage
    </a>
    <ul id="gallery" class="list-unstyled collapse {{active_menu([
      route('admin.web_setting.section',['gallery','category']),
      route('admin.web_setting.section',['gallery','gallery-section']),
    ],'show')}}">
    <li class="{{active_menu([route('admin.web_setting.section',['gallery','category'])],'active')}}"> <a href="{{route('admin.web_setting.section',['gallery','category'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Category</a></li>
    <li class="{{active_menu([route('admin.web_setting.section',['gallery','gallery-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['gallery','gallery-section'])}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Gallery</a></li>
  </ul>
</li>
<li>
  <a href="#blog" data-toggle="collapse">
    <i class="fa fa-fw fa-angle-double-right"></i> Blog
  </a>
  <ul id="blog" class="list-unstyled collapse {{active_menu([
    route('admin.blog'),
    route('admin.cat'),
  ],'show')}}">
  <li class="{{active_menu([route('admin.blog')],'active')}}"> <a href="{{route('admin.blog')}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Post</a></li>
  <li class="{{active_menu([route('admin.cat')],'active')}}"> <a href="{{route('admin.cat')}}"> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i> Category</a></li>
</ul>
</li>
<li class="{{active_menu([route('admin.web_setting.section',['points','point-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['points','point-section'])}}"> <i class="fa fa-angle-double-right"></i> Points</a></li>
<li class="{{active_menu([route('admin.web_setting.section',['faq','faq-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['faq','faq-section'])}}"> <i class="fa fa-angle-double-right"></i> FAQ</a></li>
<li class="{{active_menu([route('admin.web_setting.section',['contact','all-section'])],'active')}}"> <a href="{{route('admin.web_setting.section',['contact','all-section'])}}"> <i class="fa fa-angle-double-right"></i> Contact</a></li>
<li class="{{active_menu([route('admin.web_setting.section',['general','social'])],'active')}}"> <a href="{{route('admin.web_setting.section',['general','social'])}}"> <i class="fa fa-angle-double-right"></i> Social Link</a></li>
<li class="{{active_menu([route('admin.web_setting.section',['general','footer-content'])],'active')}}"> <a href="{{route('admin.web_setting.section',['general','footer-content'])}}"> <i class="fa fa-angle-double-right"></i> Footer Content</a></li>
<li class="{{active_menu([route('admin.web_setting.section',['general','fb-comment-script'])],'active')}}"> <a href="{{route('admin.web_setting.section',['general','fb-comment-script'])}}"> <i class="fa fa-angle-double-right"></i> FB Comment Script id</a></li>
<li class="{{active_menu([route('admin.web_setting.section',['general','login-and-breadcrumb-img'])],'active')}}"> <a href="{{route('admin.web_setting.section',['general','login-and-breadcrumb-img'])}}"> <i class="fa fa-angle-double-right"></i> Login & Breadcrumb</a></li>
</ul>
</li>
<li class="{{active_menu([route('admin.promotions'), route('promotions.create')],'active',['promotions.edit'])}}"> <a href="{{route('admin.promotions')}}"> <i class="fa fa-product-hunt"></i> Promotions</a></li>
@endif --}}
<li class="{{active_menu([route('admin.news'), route('news.create')],'active',['news.edit'])}}"> <a href="{{route('admin.news')}}"> <i class="fa fa-newspaper-o"></i> News Management</a></li>
<li class="{{active_menu([route('admin.promotion'), route('promotion.create')],'active',['promotion.edit'])}}"> <a href="{{route('admin.promotion')}}"> <i class="fa fa-product-hunt"></i> Promotion Management</a></li>
{{-- <li class="{{active_menu([route('admin.partner'), route('partner.create')],'active',['partner.edit'])}}"> <a href="{{route('admin.partner')}}"> <i class="fa fa-address-book"></i> Partner Management</a></li> --}}
<li>
  <a href="#transfer" data-toggle="collapse">
    <i class="fa fa-taxi"></i> Transfer Management
  </a>
  <ul id="transfer" class="list-unstyled collapse {{active_menu([
    route('admin.cars_transfer'),
    route('cars_transfer.create'),
    route('admin.transfer_detail'),
    route('admin.transfer_book')
  ],'show',[
    'cars_transfer.create',
    'cars_transfer.edit'
    ])}}">
  {{-- <li class="{{active_menu([route('admin.cars_transfer'),route('cars_transfer.create')],'active',['cars_transfer.edit'])}}"> <a href="{{route('admin.cars_transfer')}}"> <i class="fa fa-taxi"></i> Cars</a></li> --}}
  <li class="{{active_menu([route('admin.transfer_detail')],'active')}}"> <a href="{{route('admin.transfer_detail')}}"> <i class="fa fa-wrench"></i> Detail</a></li>
  <li class="{{active_menu([route('admin.transfer_book')],'active')}}"><a href="{{route('admin.transfer_book')}}"><i class="fa fa-list"></i> Book</a></li>
</ul>
</li>
<li class="{{active_menu([route('admin.enews'), route('enews.create')],'active',['enews.edit'])}}"> <a href="{{route('admin.enews')}}"> <i class="fa fa-paper-plane-o"></i> E-News Letter</a></li>
<li>
  <a href="#user" data-toggle="collapse">
    <i class="fa fa-user-circle"></i> User Management
  </a>
  <ul id="user" class="list-unstyled collapse {{active_menu([
    route('admin.register'),
    route('register.create'),
    route('admin.member'),
    route('member.create')
  ],'show',[
    'register.create',
    'register.edit',
    'member.create',
    'member.view'
    ])}}">
  <li class="{{active_menu([route('admin.register'),route('register.create')],'active',['register.edit'])}}"> <a href="{{route('admin.register')}}"> <i class="fa fa-fw fa-user-secret"></i> Guest</a></li>
  <li class="{{active_menu([route('admin.member'),route('member.create')],'active',['member.view'])}}"> <a href="{{route('admin.member')}}"> <i class="fa fa-fw fa-users"></i> Member</a></li>
  {{-- <li class="{{active_menu([route('admin.member')],'active')}}"> <a href="{{route('admin.member')}}"> <i class="fa fa-wrench"></i> Detail</a></li> --}}
  {{-- <li class="{{active_menu([route('admin.transfer_book')],'active')}}"><a href="{{route('admin.transfer_book')}}"><i class="fa fa-list"></i> Book</a></li> --}}
</ul>
</li>
{{-- <li>
  <a href="#Hotel_config" data-toggle="collapse">
    <i class="fa fa-fw fa-cubes"></i> Hotel Configuration
  </a>
  <ul id="Hotel_config" class="list-unstyled collapse {{active_menu([
    route('admin.room_type'),
    route('room_type.create'),
    route('admin.floor'),
    route('floor.create'),
    route('admin.room'),
    route('room.create')
  ],'show',[
    'room_type.edit',
    'room_type.view',
    'floor.edit',
    'floor.create',
    'room.edit',
    'room.create',
    ])}}">
    <li class="{{active_menu([route('admin.room_type'),route('room_type.create')],'active',['room_type.edit','room_type.view'])}}"> <a href="{{route('admin.room_type')}}"> <i class="fa fa-circle-o"></i> Room Types</a></li>
    <li class="{{active_menu([route('admin.floor'),route('floor.create')],'active',['floor.edit'])}}"> <a href="{{route('admin.floor')}}"> <i class="fa fa-circle-o"></i> Floors</a></li>
    <li class="{{active_menu([route('admin.room'),route('room.create')],'active',['room.edit'])}}"> <a href="{{route('admin.room')}}"> <i class="fa fa-circle-o"></i> Room</a></li>
  </ul>
</li> --}}
<li>
  <a href="#point" data-toggle="collapse">
    <i class="fa fa-star"></i> Point Management
  </a>
  <ul id="point" class="list-unstyled collapse {{active_menu([
    route('point.setting'),
    route('point.adjust'),
    route('point.history'),
    route('point.member')
  ],'show',[
    'point.setting',
    'point.adjust',
    'point.history',
    'point.member'
    ])}}">
    <li class="{{active_menu([route('point.setting'),route('point.setting')],'active',['point.setting'])}}"> <a href="{{route('point.setting')}}"> <i class="fa fa-circle-o"></i> Setting Point</a></li>
    <li class="{{active_menu([route('point.adjust'),route('point.adjust')],'active',['point.adjust'])}}"> <a href="{{route('point.adjust')}}"> <i class="fa fa-circle-o"></i> Adjust Point</a></li>
    <li class="{{active_menu([route('point.history'),route('point.history')],'active',['point.history'])}}"> <a href="{{route('point.history')}}"> <i class="fa fa-circle-o"></i> Point History</a></li>
    <li class="{{active_menu([route('point.member'),route('point.member')],'active',['point.member'])}}"> <a href="{{route('point.member')}}"> <i class="fa fa-circle-o"></i> Member Point</a></li>
  </ul>
</li>
<li>
  <a href="#package" data-toggle="collapse">
    <i class="fa fa-cube"></i> Package Management
  </a>
  <ul id="package" class="list-unstyled collapse {{active_menu([
    route('admin.package'),
    route('package.member'),
  ],'show',[
    'package.create',
    'package.store',
    'package.view',
    'package.update',
    'slot.create',
    'slot.store',
    'package.member',
    'package.member.detail',
    'package.member.choose'
    ])}}">
    <li class="{{active_menu([route('admin.package')],'active',['admin.package','package.create','package.store','package.view','package.update','slot.create','slot.store'])}}"> <a href="{{route('admin.package')}}"> <i class="fa fa-circle-o"></i> Package</a></li>
    <li class="{{active_menu([route('package.member')],'active',['package.member.detail','package.member.choose'])}}"> <a href="{{route('package.member')}}"> <i class="fa fa-circle-o"></i> Package Member</a></li>
  </ul>
</li>
<li class="{{active_menu([route('admin.reservation')],'active')}}"><a href="{{route('admin.reservation')}}"><i class="fa fa-fw fa-battery-half"></i> Reservation </a></li>
<li>
  <a href="#payment" data-toggle="collapse">
    <i class="fa fa-fw fa-credit-card"></i> Payment Management
  </a>
  <ul id="payment" class="list-unstyled collapse {{active_menu([
    route('admin.payment'),
    route('payment.deposite'),
  ],'show',[
    'admin.payment',
    'payment.deposite',
    ])}}">
    <li class="{{active_menu([route('admin.payment')],'active',['admin.payment'])}}"> <a href="{{route('admin.payment')}}"> <i class="fa fa-circle-o"></i> Payment</a></li>
    <li class="{{active_menu([route('payment.deposite')],'active',['payment.deposite'])}}"> <a href="{{route('payment.deposite')}}"> <i class="fa fa-circle-o"></i> Deposite</a></li>
  </ul>
</li>
<li class="{{active_menu([route('admin.roommanagement')],'active',['room.create'])}}"><a href="{{route('admin.roommanagement')}}"><i class="fa fa-fw fa-cubes"></i> Room Management </a></li>
<li class="{{active_menu([route('admin.exchange')],'active')}}"><a href="{{route('admin.exchange')}}"><i class="fa fa-exchange"></i> Exchange </a></li>
</ul>
