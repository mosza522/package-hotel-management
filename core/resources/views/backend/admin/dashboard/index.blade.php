@extends('backend.master')
@section('title',"Dashboard")

@section('style')
    <link rel="stylesheet" href="{{asset('assets/plugin/morris/morris.css')}}">
    <style>
        .btn-room{
            width: 100px;
        }
    </style>
    @endsection
@section('content')
  {{-- {{dd(Auth::user())}} --}}
    <h2 class="mb-4">DASHBOARD <small>STATISTICS</small></h2>
    <div class="row ">
      <div class="col-md-12">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Birth Date Member</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Warning Confirm</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Member Owing</a>

          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Birth date</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($birthday30 as $k => $value)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>
                        <a href="{{route('member.view',$value->id)}}">{{$value->first_name}} {{$value->last_name}}</a>
                      </td>
                      <td>
                        {{Carbon\Carbon::parse($value->date)->format('D jS M Y')}}
                      </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
          </div>
          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Package Name</th>
                    <th>เหลือเวลา</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($warning[0]['diff_date'])}} --}}
                @foreach($warning as $k => $value)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>
                        <a href="{{route('member.view',$value['user_id'])}}">{{$value['fullname']?$value['fullname']:""}}</a>
                      </td>
                      <td>
                        <a href="{{route('package.view',$value['package_id'])}}">{{$value['package_name']?$value['package_name']:""}}</a>
                      </td>
                      <td>
                        {{$value['diff_date']}}
                      </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
          </div>
          <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Package Name</th>
                    <th>Dept</th>
                    <th>Deadline</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($warning[0]['diff_date'])}} --}}
                @foreach($accrued as $k => $value)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>
                        <a href="{{route('member.view',$value['id'])}}">{{$value['fullname']?$value['fullname']:""}}</a>
                      </td>
                      <td>
                        <a href="{{route('package.view',$value['package_id'])}}">{{$value['package_name']?$value['package_name']:""}}</a>
                      </td>
                      <td>
                        {{$value['dept']}}
                      </td>
                      <td>
                        {{Carbon\Carbon::parse($value['deadline'])->format('D jS M Y')}}
                      </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/plugin/morris/raphael-min.js')}}"></script>
    <script src="{{asset('assets/plugin/morris/morris.min.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#date').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy/mm/dd',
              });
        });
    </script>
@endsection
