@extends('backend.master')
@section('title',"Reservation Title")
@section('content')

    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}</h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Owner</th>
                    <th>Checkin</th>
                    <th>Guest</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($reservation as $k => $data)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td data-label=""><a href="{{route('member.view',$data->user_id)}}">{{$data->first_name.' '.$data->last_name}}</a></td>
                        <td>{{Carbon\Carbon::parse($data->date)->format('D jS M Y')}}</td>
                        <td>{{$data->guest_name?$data->guest_name:"N/A"}}</td>
                        <td>
                          <select class="form-control status" name="status">
                            <option value="reserved,{{$data->id}},{{Auth::user()->id}}" data="{{$data->id}}" {{($data->status == "reserved")?"selected":""}}>reserved</option>
                            <option value="confirm,{{$data->id}},{{Auth::user()->id}}" data="{{$data->id}}" {{($data->status == "confirm")?"selected":""}}>confirm</option>
                            <option value="success,{{$data->id}},{{Auth::user()->id}}" data="{{$data->id}}" {{($data->status == "success")?"selected":""}}>success</option>
                            <option value="cancle,{{$data->id}},{{Auth::user()->id}}" data="{{$data->id}}" {{($data->status == "cancle")?"selected":""}}>cancle</option>
                          </select>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $reservation->render() }} --}}
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.status').change(function(){
              window.location.href = '{{url('reservation-changestatus')}}/'+this.value;
              // $.ajax({
              //   url: '{{url('reservation-changestatus')}}/'+this.value,
              //   type: 'GET',
              //   dataType:'html',
              //   success : function (response) {
              //     if(response!=""){
              //
              //     }
              //   }
              // });
            })
        });

    </script>
@endsection
