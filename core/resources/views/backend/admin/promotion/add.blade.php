@extends('backend.master')
@section('title',"Promotion Title")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')

    <h2 class="mb-4">{{$page_title}}</h2>
    {{-- {{dd($errors)}} --}}
    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('admin.promotion')}}" class="btn btn-success btn-md float-right">
                <i class="fa fa-eye"></i> All Promotion
            </a>
        </div>

        <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
                <div class="form-group">
                    <h5> Title</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('news_title')}}"
                               name="news_title">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-font"></i>
                            </span>
                        </div>
                    </div>
                    @if ($errors->has('news_title'))
                        <div class="alert alert-danger">{{ $errors->first('news_title') }}</div>
                    @endif

                </div>

                {{-- <div class="form-group">
                    <h5>Category</h5>
                    <select name="cat_id" id="cat_id" class="form-control form-control-lg" disabled>
                        <option value="">Select Category</option>
                        @foreach($category as $data)
                            <option value="{{$data->id}}" selected>{{$data->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('cat_id'))
                        <div class="error">{{ $errors->first('cat_id') }}</div>
                    @endif
                </div> --}}


                <div class="form-group">
                    <h5>Image</h5>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                            <img style="width: 200px" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image" alt="...">

                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                        <div>
                                                <span class="btn btn-info btn-file">
                                                    <span class="fileinput-new bold uppercase"><i class="fa fa-file-image-o"></i> Select image</span>
                                                    <span class="fileinput-exists bold uppercase"><i class="fa fa-edit"></i> Change</span>
                                                    <input type="file" name="picture" accept="image/*" >
                                                </span>
                            <a href="#" class="btn btn-danger fileinput-exists bold uppercase" data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                        </div>
                    </div>
                    @if ($errors->has('picture'))
                        <div class="alert alert-danger">{{ $errors->first('picture') }}</div>
                    @endif
                </div>


                <div class="form-group">
                    <h5>Details</h5>
                    <textarea name="content" id="area1" cols="30" rows="12" class="form-control">{{old('content')}}</textarea>
                    @if ($errors->has('content'))
                        <div class="alert alert-danger">{{ $errors->first('content') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <h5>Agreement</h5>
                    <textarea name="agreement" id="area2" cols="30" rows="12" class="form-control">{{old('agreement')}}</textarea>
                    @if ($errors->has('agreement'))
                        <div class="alert alert-danger">{{ $errors->first('agreement') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <h5> Expire Date</h5>
                        <input type="text" class="expire_date form-control form-control-lg" value="{{old('expire_date')}}"
                               name="expire_date">
                    @if ($errors->has('expire_date'))
                        <div class="alert alert-danger">{{ $errors->first('expire_date') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Price</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('price')}}"
                               name="price">
                    </div>
                    @if ($errors->has('price'))
                        <div class="alert alert-danger">{{ $errors->first('price') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Discount</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('discount')}}"
                               name="discount">
                    </div>
                    @if ($errors->has('discount'))
                        <div class="alert alert-danger">{{ $errors->first('discount') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Special Price</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('special_price')}}"
                               name="special_price">
                    </div>
                    @if ($errors->has('special_price'))
                        <div class="alert alert-danger">{{ $errors->first('special_price') }}</div>
                    @endif

                </div>

                {{-- <div class="form-group">
                    <h5>Agreements</h5>
                    <textarea name="agreements" id="area2" cols="30" rows="12" class="form-control">{{old('agreements')}}</textarea>
                </div> --}}


                <div class="form-group">
                    <h5>Status</h5>
                    <input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" name="status">
                </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-primary btn-block btn-lg" type="submit">Save Promotions</button>
            </div>

        </form>
    </div>




@endsection
@section('script')
    <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
    <script>
        bkLib.onDomLoaded(function() {
            // new nicEditor({fullPanel : true}).panelInstance('area1'); });
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area1');
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area2');
        });
        // $( function() {
        //   $( ".expire_date" ).datepicker({ format: 'yyyy-mm-dd' }).val();
        // } );
        $(document).ready(function () {
            $('.expire_date').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy/mm/dd',
                footer: true, modal: true
            });
        });
    </script>
@stop
