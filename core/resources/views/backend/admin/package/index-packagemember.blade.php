@extends('backend.master')
@section('title',"Package Title")
@section('content')
    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}
                {{-- <a href="{{route('package.create')}}" class="btn btn-success btn-md float-right ">
                    <i class="fa fa-plus"></i> Add Package
                </a> --}}
            </h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Email</th>
                    {{-- <th>Description</th> --}}
                    <th>Package</th>
                    <th>Slot</th>
                    <th>Detail</th>
                </tr>
                </thead>
                <tbody>

                @foreach($package_member as $k=>$data)
                    <tr>
                        <td data-label="SL">{{++$k}}</td>
                        <td data-label="package_name"><a href="{{route('member.view',$data->id)}}">{{$data->first_name.' '.$data->last_name}}</a></td>
                        <td data-label="package_code">{{($data->email) ?? ''}}</td>
                        {{-- <td data-label="package_description"><strong>{{($data->package_description) ?? ''}}</strong></td> --}}
                        <td data-label="package_price">{{($data->package_name) ?? 'N/A'}}</td>
                        <td>
                          {{Carbon\Carbon::parse($data->start_date)->format('m/d/Y').' - '.Carbon\Carbon::parse($data->end_date)->format('m/d/Y')}}
                        </td>
                        <td data-label="Category">
                          @if ($data->slot_id)
                            <a href="{{route('package.member.detail',$data->id)}}" class="btn btn-primary">Detail</a>
                          @else
                            <a href="{{route('package.member.choose',$data->id)}}" class="btn btn-warning">Choose Package</a>
                          @endif

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $posts->render() }} --}}
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on("click", '.delete_button', function (e) {
                var id = $(this).data('id');
                $(".abir_id").val(id);
            });
        });
    </script>
@endsection
