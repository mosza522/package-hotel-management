@extends('backend.master')
@section('title',"Create Package")
@section('style')
  <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')

  <h2 class="mb-4">{{$page_title}}</h2>

  <div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
      <a href="{{route('package.member')}}" class="btn btn-success btn-md float-right">
        <i class="fa fa-eye"></i> All Member Pacakge
      </a>
    </div>

    <form role="form" method="POST" action="" name="editForm">
      {{ csrf_field() }}
      <input type="hidden" name="user_id" value="{{$data->id}}">
      <div class="card-body">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Fullname</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->first_name.' '.$data->last_name}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Email</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->email}}">
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Packge</label>
                <select class="form-control" name="package" id="packages">
                  <option value="00"> Packages</option>
                  @foreach ($packages as $key => $value)
                    <option value="{{$value->id}}">{{$value->package_name.' '.$value->package_duration.' Days'}}</option>
                  @endforeach
                </select>
                {{-- <input type="text" class="form-control"  placeholder="" readonly value="{{$data->email}}"> --}}
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Slot</label>
                <select class="form-control select2" name="slot" id="slot" disabled>
                  <option value="">Select package</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Slot</label>
                <input class="form-control" type="text" name="" id="week_no" value="" disabled>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="" id="creat_period">

              </div>
            </div>
          </div>
          {{-- <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Start</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{Carbon\Carbon::parse($data->start_date)->format('m/d')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package End</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{Carbon\Carbon::parse($data->end_date)->format('m/d')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Period</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->year}} - {{$data->year+5}}">
              </div>
            </div>
          </div> --}}
      </div>
      <div class="card-footer bg-white">
        <button class="btn btn-primary btn-block btn-lg" type="submit">Next</button>
      </div>

    </form>
  </div>




@endsection
@section('script')
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
  $( document ).ready(function() {
    $('#packages').on('change', function() {
      if(this.value != 00){
        $.ajax({
          url: '{{url('getslot')}}/'+this.value,
          type: 'GET',
          dataType:'json',
          success : function (response) {
            console.log(response)
            $('#slot').removeAttr('disabled')
            .find('option')
            .remove()
            for (var i = 0; i<response.length; i++){
              var opt = document.createElement('option');
              var select = document.getElementById('slot');
                opt.value = response[i].id;
                opt.innerHTML = moment(response[i].start_date).format('L')+' - '+moment(response[i].end_date).format('L');
                select.appendChild(opt);
                if(i==0){
                  $('#slot').val(response[i].id).trigger("change")
                }

            }
          }
        });
      }
    });
    $('#slot').change(function(){
      $.ajax({
        url: '{{url('getperiod')}}/'+this.value,
        type: 'GET',
        dataType:'json',
        success : function (response) {
          console.log(response)
          $('#week_no').val('Slot '+response.period[0].week_no)
          $('#creat_period').text('')
          $('#creat_period').append('<h5>Period</h5>'+response.period_text)
        }
      });
    })
  });
  // $(document).ready(function(){
  //   $('#package').on('change',function(){
  //     console.log( "ready!" );
  //   })
  // })
  </script>
@stop
