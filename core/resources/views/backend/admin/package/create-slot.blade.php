@extends('backend.master')
@section('title',"Create Package")
@section('style')
  <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" />
@stop
@section('content')

  <h2 class="mb-4">{{$page_title}}</h2>

  <div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
      {{-- <a href="{{route('admin.package')}}" class="btn btn-success btn-md float-right">
        <i class="fa fa-eye"></i> All Package
      </a> --}}
    </div>
    {{-- {{(dd($date))}} --}}
    <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{$package->id}}">
      <input type="hidden" name="duration" value="{{$package->package_duration}}">
      <div class="card-body">
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-6 text-right">
                  {{-- <span>{{date('Y')}}</span> --}}
                </div>
                <div class="col-md-6 text-right">
                  <button class="btn btn-success add-slot" disabled type="button" name="button">สร้าง Slot</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="row">
                <div class="col-md-2">
                  <span>เลือกวันเริ่มต้น</span>
                </div>
                <div class="col-md-6">
                  <input type="text" autocomplete="off" class="form-control form-control-lg" name="" id="date" value="">
                </div>
                <div class="col-md-2">
                  <input class="form-control " type="text" name="" id="total_room" value="150" placeholder="จำนวนห้อง">
                </div>
                <div class="col-md-2">
                  {{-- <input class="status" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" name="status[]"> --}}
                </div>
              </div>
            </div>
          </div>
          <div id='slot'></div>
        </div>
        </div>
      </div>
      <div class="card-footer bg-white">
        <button class="btn btn-primary btn-block btn-lg" type="submit">Save</button>
      </div>

    </form>
  </div>



@endsection
@section('script')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
  $(document).ready(function () {
    var start = moment("{{$date}}", "YYYY-MM-DD");
    var end = moment(start).add({{$package->package_duration-1}} ,'days');
    // console.log('{{$date}}');
    // console.log(start);]
    // console.log(end);
    $('.date').daterangepicker({
      startDate: start,
      endDate: end,
    },function(startDate, endDate){
      // console.log(moment(startDate).format('L'))
      start = moment(startDate)
      end = moment(endDate)
    })
    $('#date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy',
    }).on('change', function(e) {
      console.log($('#date').val())
      start = moment($('#date').val(), "MM/DD/YYYY");
      $('.add-slot').removeAttr('disabled')
      // console.log(e)
    });
    $('.add-slot').click(function(){
      var text = "";
      var test = [];
      var count_check = moment(start)
      var limit = moment(start).add('years',6).format('L')
      var year = "" ;
      var week = 1;
      var temp = 0 ;
      console.log(moment(limit).format('gggg'))
      while(moment(count_check).format('gggg') <= moment(limit).format('gggg')){
        if(moment(count_check).format('gggg') != year){
          if(text!=""){
            text += '</div>'
          }
          week=1
          if(year == ""){
            text += '<div class="row">'
          }
          if(moment(count_check).format('gggg')%2 != moment(start).format('gggg')%2 && temp > 1){
            text += '</div>'
          }else{
            temp == 1;
          }
          if(week == 1 && moment(count_check).format('gggg')%2 != moment(start).format('gggg')%2 && temp > 1){
            text += '<div class="row">'
          }
          if(week == 1){
            text += '<div class="col-md-6">'
          }
          year = moment(count_check).format('gggg')
          // text += '<div class="col-md-6">'
            text += '<div class="form-group">'
              text += '<div class="row">'
                text += '<div class="col-md-6 text-right">'
                  text += '<span>'+year+'</span>'
                text += '</div>'
              text += '</div>'
            text += '</div>'
          // text += '</div>'
        }
        // text += '<div class="col-md-6">'
        text += '<input type="hidden" name="year[]" value="'+year+'">'
        text += '<input type="hidden" name="week[]" value="'+week+'">'
         text += '<div class="form-group">'
           text += '<div class="row">'
             text += '<div class="col-md-2">'
               text += '<span>week '+(week++)+'</span>'
             text += '</div>'
             text += '<div class="col-md-6">'
               text += '<input readonly type="text" class="form-control" name="date[]" value="'+moment(count_check).format('L')+' - '+moment(count_check).add({{$package->package_duration-1}},'days').format('L')+'">'
             text += '</div>'
             text += '<div class="col-md-2">'
               text += '<input class="form-control " type="text" name="total_room[]" value="'+$('#total_room').val()+'" placeholder="จำนวนห้อง">'
             text += '</div>'
             text += '<div class="col-md-2">'
               text += '<input class="status" type="checkbox" checked data-onstyle="success" data-offstyle="danger" data-toggle="toggle" name="status[]">'
             text += '</div>'
           text += '</div>'
         text += '</div>'
       // text += '</div>'
       // if(moment(count_check).format('L') == '12/23/2024'){
       //   console.log('problem')
       //   console.log(moment(count_check).format('L'))
       //   console.log(moment(count_check).week())
       //   console.log(moment(moment(count_check).format('gggg')+'-12-31', "YYYY-MM-DD").week())
       // }
       count_check = moment(count_check).add({{$package->package_duration}},'days')
       // if(moment(count_check).format('L') >= moment(moment(count_check).format('gggg')+'-12-25', "YYYY-MM-DD").format('L')){
       //   while(moment(count_check).week() != 2){
       //     count_check = moment(count_check).add(1,'day')
       //   }
       // }
       // if(moment(count_check).week() == moment(moment(count_check).format('gggg')+'-12-31', "YYYY-MM-DD").week() == 1){
       //   if(moment(count_check).week() == 52 || moment(count_check).week()== 1){
       //     console.log(moment(count_check).format('gggg'))
       //     console.log(moment(count_check).week())
       //     while(moment(count_check).week() != 2){
       //       count_check = moment(count_check).add(1,'day')
       //     }
       //     // console.log(moment(count_check).week())
       //     continue;
       //   }
       // }else{
       //   if(moment(count_check).week() == 53 || moment(count_check).week()== 1){
       //     console.log(moment(count_check).format('gggg'))
       //     console.log(moment(count_check).week())
       //     while(moment(count_check).week() != 2){
       //       count_check = moment(count_check).add(1,'day')
       //     }
       //     // console.log(moment(count_check).week())
       //     continue;
       //   }
       // }
       if(moment(count_check).format('L') == '01/15/2023'){
         console.log('problem')
         console.log(moment(moment(count_check).format('gggg')+'-12-31').startOf('week').format('L'))
         console.log(moment(count_check).startOf('week').format('L'))
         console.log(moment(count_check).format('L'))
         console.log(moment(count_check).week())
         console.log(moment(moment(count_check).format('gggg')+'-12-31', "YYYY-MM-DD").week())
       }
       if(moment(count_check).add({{$package->package_duration}},'days').format('L') >= moment(moment(count_check).format('gggg')+'-12-31').startOf('week').format('L')){
         while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
           count_check = moment(count_check).add(1,'day')
           temp ++
         }
       }
        if(moment(count_check).week() == moment(moment(count_check).format('gggg')+'-12-31', "YYYY-MM-DD").week() || moment(count_check).format('gggg') != year ){
          console.log('now')
          console.log(moment(count_check).week())
          console.log('last')
          console.log(moment(moment(count_check).format('gggg')+'-12-31', "YYYY-MM-DD").week())
          console.log(moment(count_check).format('L'))
          while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
            count_check = moment(count_check).add(1,'day')
            temp ++
          }
          // console.log(moment(count_check).format('L'))
          if(moment(count_check).week() == 1 || moment(count_check).week() == 2){
            while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
              count_check = moment(count_check).add(1,'day')
              temp ++
            }
          }
        }
        // else if(moment(count_check).format('L') >= moment(moment(count_check).format('gggg')+'-12-23', "YYYY-MM-DD").format('L')){
        else if(moment(count_check).format('L') >= moment(moment(count_check).format('gggg')+'-12-25', "YYYY-MM-DD").format('L')){
          while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
            count_check = moment(count_check).add(1,'day')
            temp ++
          }
        }
        console.log('temp: '+temp)
        if(temp > 17){
          count_check = moment(count_check).subtract('days',7).startOf('week')
        }
        temp = 0
      }
      $('#slot').text('');
      $('#slot').append(text);
      $('.status').bootstrapToggle();
      // console.log(text)
    })
  })
  </script>

@stop
