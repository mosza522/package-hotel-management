@extends('backend.master')
@section('title',"Create Package")
@section('style')
  <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@stop
@section('content')

  <h2 class="mb-4">{{$page_title}}</h2>

  <div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
      <a href="{{route('admin.package')}}" class="btn btn-success btn-md float-right">
        <i class="fa fa-eye"></i> All Package
      </a>
    </div>

    <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" name="package_id" value="{{$package->id}}">
      <div class="card-body">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <h5> Name</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{$package->package_name}}"
                  name="package_name">
                </div>
                @if ($errors->has('package_name'))
                  <div class="error">{{ $errors->first('package_name') }}</div>
                @endif

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <h5> Code</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{$package->package_code}}"
                  name="package_code">
                </div>
                @if ($errors->has('package_code'))
                  <div class="error">{{ $errors->first('package_code') }}</div>
                @endif

              </div>

            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <h5> Price</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{$package->package_price}}"
                  name="package_price">
                </div>
                @if ($errors->has('package_price'))
                  <div class="error">{{ $errors->first('package_price') }}</div>
                @endif

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <h5> Duration</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{$package->package_duration}}"
                  name="package_duration">
                </div>
                @if ($errors->has('package_duration'))
                  <div class="error">{{ $errors->first('package_duration') }}</div>
                @endif

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <h5>Image</h5>
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"
                           data-trigger="fileinput">
                          @if($package->package_image == null)
                              <img style="width: 200px"
                                   src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image"
                                   alt="...">
                          @else
                              <img style="width: 200px" src="{{ asset('assets/backend/image/package') }}/{{ $package->package_image }}"
                                   alt="...">
                          @endif
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail"
                           style="max-width: 200px; max-height: 150px"></div>
                      <div>
                                          <span class="btn btn-info btn-file">
                                              <span class="fileinput-new bold uppercase"><i
                                                          class="fa fa-file-image-o"></i> Select image</span>
                                              <span class="fileinput-exists bold uppercase"><i
                                                          class="fa fa-edit"></i> Change</span>
                                              <input type="file" name="package_image" accept="image/*">
                                          </span>
                          <a href="#" class="btn btn-danger fileinput-exists bold uppercase"
                             data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                      </div>
                  </div>
                  @if ($errors->has('package_image'))
                      <div class="error">{{ $errors->first('package_image') }}</div>
                  @endif

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <h5>Description</h5>
                <textarea name="package_description" id="area1" cols="30" rows="12" class="form-control">{{$package->package_description}}</textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <h5>Agreements</h5>
                <textarea name="package_agreement" id="area2" cols="30" rows="12" class="form-control">{{$package->package_agreement}}</textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <h5>Slot</h5>
              </div>
            </div>
          </div>
          {{-- <div class="form-row justify-content-center">
              <div class="col-md-6">
                  <label><strong>Short Code</strong> <small class="text-danger">*</small></label>
                  <input type="text" class="form-control  form-control-lg" name="short_code" placeholder="Short Code" required>
              </div>
          </div> --}}
          <div class="row justify-content-center" style="padding-bottom:20px">
            <div class="col-md-6 ">
              <div class="row">
                {{-- <div class="form-group"> --}}
                  <div class="col-md-6" style="width:100%">
                    <select class="form-control" name="" id="selectyears">
                      @foreach ($years as $key => $value)
                        <option value="{{$value}}">{{$value}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-6">
                    <button class="btn btn-success add-slot" type="button" name="button">+ 1 year</button>
                  </div>
                {{-- </div> --}}
              </div>
            </div>
          </div>
          @php
            $year = "";
            $temp = 1;
          @endphp
          @foreach ($slots as $key => $slot)
            @if ($year != $slot->year)
              @if ($slots[0]->year != $slot->year)
                </div>
              @endif
              @if ($temp%2 == 1 and $temp!=1)
                </div>
              @endif
              @if ($year=="")
                <div class="row justify-content-center">
              @endif
              @if ($slot->year%2 == $slots[0]->year%2 and $year!=$slot->year and $year!="")
                <div class="row justify-content-center">
              @endif
              @php
                $year = $slot->year;
              @endphp
              @if ($year != "" and $year!=$slot->year)
              </div>
              @else
              @php
                $year = $slot->year;
              @endphp
              @endif
              @if ($slot->week_no==1)
                <div class="col-md-8 {{$slot->year}}">
              @endif
              @php
                $temp++;
              @endphp
              {{-- <div class="form-group">
                <div class="row">
                  <div class="col-md-6 text-right">
                    <h5>{{$slot->year}}</h5>
                  </div>
                </div>
              </div> --}}
            @endif
            <input type="hidden" name="year[]" value="{{$slot->year}}">
            <input type="hidden" name="week[]" value="{{$slot->week_no}}">
             <div class="form-group">
               <div class="row">
                 <div class="col-md-2">
                   <span>week {{$slot->week_no}}</span>
                 </div>
                 <div class="col-md-6">
                   <input type="text" class="form-control date" name="date[]" value="{{Carbon\Carbon::parse($slot->start_date)->format('m/d/Y')}} - {{Carbon\Carbon::parse($slot->end_date)->format('m/d/Y')}}">
                 </div>
                 <div class="col-md-2">
                   <input class="form-control " type="text" name="total_room[]" value="{{$slot->total_room}}" placeholder="จำนวนห้อง">
                 </div>
                 <div class="col-md-2">
                   <input {{$slot->status == "1" ? 'checked' : '' }} class="status" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" name="status[{{$key}}]">
                 </div>
               </div>
             </div>
          @endforeach
        </div>
        </div>
        <div id="divAddSlot" class="justify-content-center">

        </div>
        {{-- {{dd($year[0])}} --}}




        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <h5>Status</h5>
              <input data-toggle="toggle" {{$package->package_status == "1" ? 'checked' : '' }} data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" name="package_status">
            </div>
          </div>
        </div>

      </div>
      <div class="card-footer bg-white">
        <button class="btn btn-primary btn-block btn-lg" type="submit">update</button>
      </div>

    </form>
  </div>




@endsection
@section('script')
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
  bkLib.onDomLoaded(function() {
    // new nicEditor({fullPanel : true}).panelInstance('area1'); });
    new nicEditor({
      iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
      fullPanel : true
    }).panelInstance('area1');
    new nicEditor({
      iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
      fullPanel : true
    }).panelInstance('area2');
  });
  $(document).ready(function () {
    $('.date').daterangepicker()
    var years = {{json_encode($years)}}
    for (var i = 0; i < years.length; i++) {
      if(i!=0){
        $('.'+years[i]).hide()
      }
    }
    $('#selectyears').change(function(){
      for (var i = 0; i < years.length; i++) {
          $('.'+years[i]).hide()
      }
      $('.'+$('#selectyears').val()).show()
      console.log($('#selectyears').val())
    })
    $('.add-slot').click(function(){
      var text = "";
      var test = [];
      var start = moment(years[years.length-1]+1+"-01-01","YYYY-MM-DD")
      var count_check = moment(start)
      // console.log(moment(count_check).week())
      while(moment(count_check).week() < 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
        count_check = moment(count_check).add(1,'day')
      }
      var limit = moment(years[years.length-1]+1,"YYYY")
      var year = "" ;
      var week = 1;
      var temp = 0 ;
      years.push(years[years.length-1]+1)
      $('#selectyears')
         .append($("<option></option>")
                    .attr("value",years[years.length-1])
                    .attr("checked","checked")
                    .text(years[years.length-1]));
      for (var i = 0; i < years.length; i++) {
          $('.'+years[i]).hide()
      }
      $('#selectyears').val(years[years.length-1])
      $('.'+$('#selectyears').val()).show()
      while(moment(count_check).format('gggg') <= moment(limit).format('gggg')){
        if(moment(count_check).format('gggg') != year){
          if(text!=""){
            text += '</div>'
          }
          week=1
          if(year == ""){
            text += '<div class="row justify-content-center">'
          }
          if(moment(count_check).format('gggg')%2 != moment(start).format('gggg')%2 && temp > 1){
            text += '</div>'
          }else{
            temp = 1;
          }
          if(week == 1 && moment(count_check).format('gggg')%2 != moment(start).format('gggg')%2 && temp > 1){
            text += '<div class="row justify-content-center">'
          }
          if(week == 1){
            text += '<div class="col-md-8 '+years[years.length-1]+'">'
          }
          year = moment(count_check).format('gggg')
          // text += '<div class="col-md-6">'
            // text += '<div class="form-group">'
            //   text += '<div class="row">'
            //     text += '<div class="col-md-6 text-right">'
            //       text += '<h5>'+year+'</h5>'
            //     text += '</div>'
            //   text += '</div>'
            // text += '</div>'
          // text += '</div>'
        }
        // text += '<div class="col-md-6">'
        text += '<input type="hidden" name="year_add[]" value="'+year+'">'
        text += '<input type="hidden" name="week_add[]" value="'+week+'">'
         text += '<div class="form-group">'
           text += '<div class="row">'
             text += '<div class="col-md-2">'
               text += '<span>week '+(week++)+'</span>'
             text += '</div>'
             text += '<div class="col-md-6">'
               text += '<input type="text" class="form-control date" name="date_add[]" value="'+moment(count_check).format('L')+' - '+moment(count_check).add({{$package->package_duration-1}},'days').format('L')+'">'
             text += '</div>'
             text += '<div class="col-md-2">'
               text += '<input class="form-control " type="text" name="total_room_add[]" value="{{$slots[count($slots)-1]->total_room}}" placeholder="จำนวนห้อง">'
             text += '</div>'
             text += '<div class="col-md-2">'
               text += '<input class="status" type="checkbox" checked data-onstyle="success" data-offstyle="danger" data-toggle="toggle" name="status_add[]">'
             text += '</div>'
           text += '</div>'
         text += '</div>'
       count_check = moment(count_check).add({{$package->package_duration}},'days')
       if(moment(count_check).add({{$package->package_duration}},'days').format('L') >= moment(moment(count_check).format('gggg')+'-12-31').startOf('week').format('L')){
         while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
           count_check = moment(count_check).add(1,'day')
           temp ++
         }
       }
        if(moment(count_check).week() == moment(moment(count_check).format('gggg')+'-12-31', "YYYY-MM-DD").week() || moment(count_check).format('gggg') != year ){
          console.log('now')
          console.log(moment(count_check).week())
          console.log('last')
          console.log(moment(moment(count_check).format('gggg')+'-12-31', "YYYY-MM-DD").week())
          console.log(moment(count_check).format('L'))
          while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
            count_check = moment(count_check).add(1,'day')
            temp ++
          }
          // console.log(moment(count_check).format('L'))
          if(moment(count_check).week() == 1 || moment(count_check).week() == 2){
            while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
              count_check = moment(count_check).add(1,'day')
              temp ++
            }
          }
        }
        // else if(moment(count_check).format('L') >= moment(moment(count_check).format('gggg')+'-12-23', "YYYY-MM-DD").format('L')){
        else if(moment(count_check).format('L') >= moment(moment(count_check).format('gggg')+'-12-25', "YYYY-MM-DD").format('L')){
          while(moment(count_check).week() <= 2 || moment(count_check).week() == 52  ||moment(count_check).week() == 53){
            count_check = moment(count_check).add(1,'day')
            temp ++
          }
        }
        console.log('temp: '+temp)
        temp = 0
      }
      // $('#divAddSlot').text('');
      $('#divAddSlot').append(text);
      $('.status').bootstrapToggle();
      $('.date').daterangepicker()
      // console.log(text)
    })
  })
  </script>
@stop
