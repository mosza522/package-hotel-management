@extends('backend.master')
@section('title',"Create Package")
@section('style')
  <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')

  <h2 class="mb-4">{{$page_title}}</h2>

  <div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
      <a href="{{route('admin.package')}}" class="btn btn-success btn-md float-right">
        <i class="fa fa-eye"></i> All Package
      </a>
    </div>

    <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="card-body">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <h5> Name</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{old('package_name')}}"
                  name="package_name">
                </div>
                @if ($errors->has('package_name'))
                  <div class="error">{{ $errors->first('package_name') }}</div>
                @endif

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <h5> Code</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{old('package_code')}}"
                  name="package_code">
                </div>
                @if ($errors->has('package_code'))
                  <div class="error">{{ $errors->first('package_code') }}</div>
                @endif

              </div>

            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <h5> Price</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{old('package_price')}}"
                  name="package_price">
                </div>
                @if ($errors->has('package_price'))
                  <div class="error">{{ $errors->first('package_price') }}</div>
                @endif

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <h5> Duration</h5>
                <div class="input-group">
                  <input type="text" class="form-control form-control-lg" value="{{old('package_duration')}}"
                  name="package_duration">
                </div>
                @if ($errors->has('package_duration'))
                  <div class="error">{{ $errors->first('package_duration') }}</div>
                @endif

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <h5>Image</h5>
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                          <img style="width: 200px" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image" alt="...">

                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                      <div>
                                              <span class="btn btn-info btn-file">
                                                  <span class="fileinput-new bold uppercase"><i class="fa fa-file-image-o"></i> Select image</span>
                                                  <span class="fileinput-exists bold uppercase"><i class="fa fa-edit"></i> Change</span>
                                                  <input type="file" name="package_image" accept="image/*" >
                                              </span>
                          <a href="#" class="btn btn-danger fileinput-exists bold uppercase" data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                      </div>
                  </div>
                  @if ($errors->has('package_image'))
                      <div class="error">{{ $errors->first('package_image') }}</div>
                  @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <h5>Description</h5>
                <textarea name="package_description" id="area1" cols="30" rows="12" class="form-control">{{old('package_description')}}</textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <h5>Agreements</h5>
                <textarea name="package_agreement" id="area2" cols="30" rows="12" class="form-control">{{old('package_agreement')}}</textarea>
              </div>
            </div>
          </div>
        </div>






        <div class="form-group">
          <h5>Status</h5>
          <input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" name="status">
        </div>
      </div>
      <div class="card-footer bg-white">
        <button class="btn btn-primary btn-block btn-lg" type="submit">Next</button>
      </div>

    </form>
  </div>




@endsection
@section('script')
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
  bkLib.onDomLoaded(function() {
    // new nicEditor({fullPanel : true}).panelInstance('area1'); });
    new nicEditor({
      iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
      fullPanel : true
    }).panelInstance('area1');
    new nicEditor({
      iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
      fullPanel : true
    }).panelInstance('area2');
  });
  </script>
@stop
