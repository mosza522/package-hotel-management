@extends('backend.master')
@section('title',"Package Title")
@section('content')
    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}
                <a href="{{route('package.create')}}" class="btn btn-success btn-md float-right ">
                    <i class="fa fa-plus"></i> Add Package
                </a>
            </h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Package name</th>
                    <th>Package code</th>
                    {{-- <th>Description</th> --}}
                    <th>Price</th>
                    <th>Image</th>
                    <th>Duration</th>

                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>

                @foreach($package as $k=>$data)
                    <tr>
                        <td data-label="SL">{{++$k}}</td>
                        <td data-label="package_name">{{($data->package_name) ?? ''}}</td>
                        <td data-label="package_code"><strong>{{($data->package_code) ?? ''}}</strong></td>
                        {{-- <td data-label="package_description"><strong>{{($data->package_description) ?? ''}}</strong></td> --}}
                        <td data-label="package_price"><strong>{{($data->package_price) ?? ''}}</strong></td>
                        <td>
                          @if($data->package_image == null)
                              <img style="width: 200px"
                                   src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image"
                                   alt="...">
                          @else
                              <img style="width: 200px" src="{{ asset('assets/backend/image/package/') }}/{{ $data->package_image }}"
                                   alt="...">
                          @endif
                        </td>
                        <td data-label="Category"><strong>{{($data->package_duration) ?? ''}}</strong></td>
                        {{-- <td data-label="Category"><a href="{{route('package.detail',$data->id)}}" class="btn btn-info">Detail</a></td> --}}
                        {{-- <td data-label="Status">
                            <span class="badge  badge-pill  badge-{{ $data->package_status ==0 ? 'warning' : 'success' }}">{{ $data->package_status == 0 ? 'Deactive' : 'Active' }}</span>
                        </td> --}}
                        <td class="text-right" data-label="Action">
                            <a href="{{route('package.view',$data->id)}}" class="btn btn-outline-tsk"><i class="fa fa-eye"></i> </a>


                            {{-- <button type="button" class="btn btn-danger  btn-icon btn-pill delete_button" title="Delete"
                                    data-toggle="modal" data-target="#DelModal"
                                    data-id="{{ $data->id }}">
                                <i class='fa fa-trash'></i>
                            </button> --}}

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $posts->render() }} --}}
        </div>
    </div>


    <div class="modal fade" id="DelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2"><i class='fa fa-trash'></i> Delete !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <form method="post" action="{{ route('blog.delete') }}">@csrf
                    {{ method_field('DELETE') }}

                    <div class="modal-body">
                        <input type="hidden" name="id" class="abir_id" value="0">
                        <strong>Are you sure you want to Delete ?</strong>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success"> Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>&nbsp;
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on("click", '.delete_button', function (e) {
                var id = $(this).data('id');
                $(".abir_id").val(id);
            });
        });
    </script>
@endsection
