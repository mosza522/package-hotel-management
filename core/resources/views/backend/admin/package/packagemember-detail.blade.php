@extends('backend.master')
@section('title',"Create Package")
@section('style')
  <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')

  <h2 class="mb-4">{{$page_title}}</h2>

  <div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
      <a href="{{route('package.member')}}" class="btn btn-success btn-md float-right">
        <i class="fa fa-eye"></i> All Member Pacakge
      </a>
    </div>

    <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="card-body">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Fullname</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->first_name.' '.$data->last_name}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Email</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->email}}">
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Name</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->package_name}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Duration</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->duration}} days">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Slot</label>
                <input type="text" class="form-control"  placeholder="" readonly value="Slot {{$data->week_no}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h5>Period</h5>
              <table class="table table-hover">
                <thead>
                  <th>Date</th>
                  <th>Year</th>
                </thead>
                <tbody>
                  @foreach ($period as $key => $value)
                    <tr>
                      <td>{{Carbon\Carbon::parse($value->start_date)->format('D jS M Y')}} - {{Carbon\Carbon::parse($value->end_date)->format('D jS M Y')}}</td>
                      <td>{{$value->year}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          {{-- <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Start</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{Carbon\Carbon::parse($data->start_date)->format('m/d')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package End</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{Carbon\Carbon::parse($data->end_date)->format('m/d')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Package Period</label>
                <input type="text" class="form-control"  placeholder="" readonly value="{{$data->year}} - {{$data->year+5}}">
              </div>
            </div>
          </div> --}}
      </div>
      <div class="card-footer bg-white">
        <a href="{{route('package.member')}}" class="btn-block btn-lg btn btn-success">
          Back
        </a>
      </div>

    </form>
  </div>




@endsection
@section('script')
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
  bkLib.onDomLoaded(function() {
    // new nicEditor({fullPanel : true}).panelInstance('area1'); });
    new nicEditor({
      iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
      fullPanel : true
    }).panelInstance('area1');
    new nicEditor({
      iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
      fullPanel : true
    }).panelInstance('area2');
  });
  </script>
@stop
