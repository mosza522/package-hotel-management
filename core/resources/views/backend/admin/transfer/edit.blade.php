@extends('backend.master')
@section('title',"Partner Title")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('admin.partner')}}" class="btn btn-success btn-md float-right">
                <i class="fa fa-eye"></i> All Partner
            </a>
        </div>

        <form role="form" method="POST" action="{{route('partner.update')}}" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$partner->id}}">
            <div class="card-body">
                <div class="form-group">
                    <h5> Name</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{$partner->name}}"
                               name="name">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-font"></i>
                            </span>
                        </div>
                    </div>
                    @if ($errors->has('name'))
                        <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Description</h5>
                    <div class="input-group">
                      <textarea class="form-control form-control-lg" name="description" rows="8" cols="80">{{$partner->description}}</textarea>
                        {{-- <input type="text" class="form-control form-control-lg" value="{{old('description')}}"
                               name="name">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-font"></i>
                            </span>
                        </div> --}}
                    </div>
                    @if ($errors->has('description'))
                        <div class="alert alert-danger">{{ $errors->first('description') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Start</h5>
                    <div class="input-group">
                        <input type="text" class="datepicker form-control form-control-lg" value="{{$partner->start_date}}"
                               name="start_date">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                    </div>
                    @if ($errors->has('start_date'))
                        <div class="alert alert-danger">{{ $errors->first('start_date') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> End</h5>
                    <div class="input-group">
                        <input type="text" class="datepicker form-control form-control-lg" value="{{$partner->end_date}}"
                               name="end_date">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                    </div>
                    @if ($errors->has('end_date'))
                        <div class="alert alert-danger">{{ $errors->first('end_date') }}</div>
                    @endif

                </div>


                <div class="form-group">
                    <h5>Status</h5>
                    <input data-toggle="toggle" data-onstyle="success" data-offstyle="danger"
                           data-width="100%" type="checkbox"
                           name="status" {{$partner->partner_status == "1" ? 'checked' : '' }}>
                </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-primary btn-block btn-lg" type="submit">Save Partner</button>
            </div>

        </form>
    </div>




@endsection
@section('script')
    <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
    <script>
        bkLib.onDomLoaded(function() {
            // new nicEditor({fullPanel : true}).panelInstance('area1'); });
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area1');
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area2');
        });
        $( function() {
          $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        } );
    </script>
@stop
