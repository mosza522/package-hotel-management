@extends('backend.master')
@section('title',"Transfer")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')
    {{-- {{dd(session()->all())}} --}}
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
      @if($cars_transfer)
        <form role="form" method="POST" action="{{route('transfer_detail.update')}}" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-body">
                <input type="hidden" name="id" value="{{$cars_transfer->id}}">
                    <div class="form-group">
                        <h5> Van Price</h5>
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg" value="{{$cars_transfer->van_price}}"
                                   name="van_price">
                        </div>
                        @if ($errors->has('van_price'))
                            <div class="alert alert-danger">{{ $errors->first('van_price') }}</div>
                        @endif

                    </div>
                    <div class="form-group">
                        <h5> Saloon Price</h5>
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg" value="{{$cars_transfer->saloon_price}}"
                                   name="saloon_price">
                        </div>
                        @if ($errors->has('saloon_price'))
                            <div class="alert alert-danger">{{ $errors->first('saloon_price') }}</div>
                        @endif

                    </div>
                    <div class="form-group">
                        <h5>Van Image</h5>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"
                                 data-trigger="fileinput">
                                @if($cars_transfer->van_picture == null)
                                    <img style="width: 200px"
                                         src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image"
                                         alt="...">
                                @else
                                    <img style="width: 200px" src="{{ asset('assets/backend/image/transfer') }}/{{ $cars_transfer->van_picture }}"
                                         alt="...">
                                @endif
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
                                                <span class="btn btn-info btn-file">
                                                    <span class="fileinput-new bold uppercase"><i
                                                                class="fa fa-file-image-o"></i> Select image</span>
                                                    <span class="fileinput-exists bold uppercase"><i
                                                                class="fa fa-edit"></i> Change</span>
                                                    <input type="file" name="van_picture" accept="image/*">
                                                </span>
                                <a href="#" class="btn btn-danger fileinput-exists bold uppercase"
                                   data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                        @if ($errors->has('van_picture'))
                            <div class="alert alert-danger">{{ $errors->first('van_picture') }}</div>
                        @endif

                    </div>
                    <div class="form-group">
                        <h5>Saloon Image</h5>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"
                                 data-trigger="fileinput">
                                @if($cars_transfer->saloon_picture == null)
                                    <img style="width: 200px"
                                         src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image"
                                         alt="...">
                                @else
                                    <img style="width: 200px" src="{{ asset('assets/backend/image/transfer') }}/{{ $cars_transfer->saloon_picture }}"
                                         alt="...">
                                @endif
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
                                                <span class="btn btn-info btn-file">
                                                    <span class="fileinput-new bold uppercase"><i
                                                                class="fa fa-file-image-o"></i> Select image</span>
                                                    <span class="fileinput-exists bold uppercase"><i
                                                                class="fa fa-edit"></i> Change</span>
                                                    <input type="file" name="saloon_picture" accept="image/*">
                                                </span>
                                <a href="#" class="btn btn-danger fileinput-exists bold uppercase"
                                   data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                        @if ($errors->has('saloon_picture'))
                            <div class="alert alert-danger">{{ $errors->first('saloon_picture') }}</div>
                        @endif

                    </div>

                    <div class="form-group">
                        <h5>Van Descrioption</h5>
                        <textarea name="van_description" id="area1" cols="30" rows="12" class="form-control form-control-lg">{{$cars_transfer->van_description}}</textarea>
                        @if ($errors->has('van_description'))
                            <div class="alert alert-danger">{{ $errors->first('van_description') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <h5>Saloon Descrioption</h5>
                        <textarea name="saloon_description" id="area2" cols="30" rows="12" class="form-control form-control-lg">{{$cars_transfer->saloon_description}}</textarea>
                        @if ($errors->has('saloon_description'))
                            <div class="alert alert-danger">{{ $errors->first('saloon_description') }}</div>
                        @endif
                    </div>

                    {{-- <div class="form-group">
                        <h5>Status</h5>
                        <input data-toggle="toggle" data-onstyle="success" data-offstyle="danger"
                               data-width="100%" type="checkbox"
                               name="status" {{$cars_transfer->status == "1" ? 'checked' : '' }}>
                    </div> --}}
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Update</button>
            </div>

        </form>
      @else
        <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
                {{-- <div class="form-group">
                    <h5> Car</h5>
                    <div class="">
                      <select class="form-control" name="car_id">
                        @foreach ($cars as $key => $value)
                          <option value="{{$value->id}}">{{$value->brand}} | {{$value->model}} | {{$value->plate}}</option>
                        @endforeach
                      </select>
                    </div>
                    @if ($errors->has('car_id'))
                        <div class="alert alert-danger">{{ $errors->first('car_id') }}</div>
                    @endif

                </div> --}}
                <div class="form-group">
                    <h5> Van Price</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('van_price')}}"
                               name="van_price">
                    </div>
                    @if ($errors->has('van_price'))
                        <div class="alert alert-danger">{{ $errors->first('van_price') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Saloon Price</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('saloon_price')}}"
                               name="saloon_price">
                    </div>
                    @if ($errors->has('saloon_price'))
                        <div class="alert alert-danger">{{ $errors->first('saloon_price') }}</div>
                    @endif

                </div>

                {{-- <div class="form-group">
                    <h5>Category</h5>
                    <select name="cat_id" id="cat_id" class="form-control form-control-lg" disabled>
                        <option value="">Select Category</option>
                        @foreach($category as $data)
                            <option value="{{$data->id}}" selected>{{$data->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('cat_id'))
                        <div class="error">{{ $errors->first('cat_id') }}</div>
                    @endif
                </div> --}}


                <div class="form-group">
                    <h5>Van Image</h5>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                            <img style="width: 200px" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image" alt="...">

                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                        <div>
                                                <span class="btn btn-info btn-file">
                                                    <span class="fileinput-new bold uppercase"><i class="fa fa-file-image-o"></i> Select image</span>
                                                    <span class="fileinput-exists bold uppercase"><i class="fa fa-edit"></i> Change</span>
                                                    <input type="file" name="van_picture" accept="image/*" >
                                                </span>
                            <a href="#" class="btn btn-danger fileinput-exists bold uppercase" data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                        </div>
                    </div>
                    @if ($errors->has('van_picture'))
                        <div class="alert alert-danger">{{ $errors->first('van_picture') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <h5>Saloon Image</h5>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                            <img style="width: 200px" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image" alt="...">

                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                        <div>
                                                <span class="btn btn-info btn-file">
                                                    <span class="fileinput-new bold uppercase"><i class="fa fa-file-image-o"></i> Select image</span>
                                                    <span class="fileinput-exists bold uppercase"><i class="fa fa-edit"></i> Change</span>
                                                    <input type="file" name="saloon_picture" accept="image/*" >
                                                </span>
                            <a href="#" class="btn btn-danger fileinput-exists bold uppercase" data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                        </div>
                    </div>
                    @if ($errors->has('saloon_picture'))
                        <div class="alert alert-danger">{{ $errors->first('saloon_picture') }}</div>
                    @endif
                </div>


                <div class="form-group">
                    <h5>Van Descrioption</h5>
                    <textarea name="van_description" id="area1" cols="30" rows="12" class="form-control">{{old('van_description')}}</textarea>
                    @if ($errors->has('van_description'))
                        <div class="alert alert-danger">{{ $errors->first('van_description') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <h5>Saloon Descrioption</h5>
                    <textarea name="saloon_description" id="area2" cols="30" rows="12" class="form-control">{{old('saloon_description')}}</textarea>
                    @if ($errors->has('saloon_description'))
                        <div class="alert alert-danger">{{ $errors->first('saloon_description') }}</div>
                    @endif
                </div>

                {{-- <div class="form-group">
                    <h5>Agreements</h5>
                    <textarea name="agreements" id="area2" cols="30" rows="12" class="form-control">{{old('agreements')}}</textarea>
                </div> --}}


                {{-- <div class="form-group">
                    <h5>Status</h5>
                    <input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" name="status">
                </div> --}}
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Save</button>
            </div>

        </form>
      @endif
    </div>


@endsection

@section('import-script')
    <!-- <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script> -->
    <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script>
@stop
@section('script')
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
  <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
  <script>
      bkLib.onDomLoaded(function() {
          // new nicEditor({fullPanel : true}).panelInstance('area1'); });
          new nicEditor({
              iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
              fullPanel : true
          }).panelInstance('area1');
          new nicEditor({
              iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
              fullPanel : true
          }).panelInstance('area2');
      });
  </script>
@stop
