@extends('backend.master')
@section('title',"Promotion Title")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')

    <h2 class="mb-4">{{$page_title}}</h2>
    {{-- {{dd($errors)}} --}}
    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('admin.promotion')}}" class="btn btn-success btn-md float-right">
                <i class="fa fa-eye"></i> All Cars
            </a>
        </div>

        <form role="form" method="POST" action="" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
                <div class="form-group">
                    <h5> Brand</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('brand')}}"
                               name="brand">
                    </div>
                    @if ($errors->has('brand'))
                        <div class="alert alert-danger">{{ $errors->first('brand') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Model</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('model')}}"
                               name="model">
                    </div>
                    @if ($errors->has('model'))
                        <div class="alert alert-danger">{{ $errors->first('model') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Plate</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('plate')}}"
                               name="plate">
                    </div>
                    @if ($errors->has('plate'))
                        <div class="alert alert-danger">{{ $errors->first('plate') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Seat</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('seats')}}"
                               name="seats">
                    </div>
                    @if ($errors->has('seats'))
                        <div class="alert alert-danger">{{ $errors->first('seats') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Luggage</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('luggage')}}"
                               name="luggage">
                    </div>
                    @if ($errors->has('luggage'))
                        <div class="alert alert-danger">{{ $errors->first('luggage') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <h5> Color</h5>
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" value="{{old('color')}}"
                               name="color">
                    </div>
                    @if ($errors->has('color'))
                        <div class="alert alert-danger">{{ $errors->first('color') }}</div>
                    @endif

                </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Add car</button>
            </div>

        </form>
    </div>




@endsection
@section('script')
    <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <!-- <script src="{{ asset('assets/admin/js/nicEdit-latest.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>
    <script>
        bkLib.onDomLoaded(function() {
            // new nicEditor({fullPanel : true}).panelInstance('area1'); });
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area1');
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area2');
        });
        $( function() {
          $( ".expire_date" ).datepicker({ format: 'yyyy-mm-dd' }).val();
        } );
    </script>
@stop
