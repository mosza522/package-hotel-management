@extends('backend.master')
@section('title',"Transfer Booking")
@section('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" />
@endsection
@section('content')
    <!-- <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 > Cms Content
            </h2>

        </div>
        <div class="card-body ">
            <form action="{{route('admin.web_setting.section.store',['promotions','promotions-section'])}}" method="post">@csrf
                <div class="form-group">
                    <label for="title_1">Title 1</label>
                    <input type="text" class="form-control form-control-lg" id="title_1" name="title_1" value="{{web_setting()->blog_blog_section_title_1}}">
                </div>
                <div class="form-group">
                    <label for="title_2">Title 2</label>
                    <input type="text" class="form-control form-control-lg" id="title_2" name="title_2" value="{{web_setting()->blog_blog_section_title_2}}">
                </div>
                <div class="form-group">
                    <hr/>
                    <button type="submit" class="btn btn-tsk btn-block"><i class="fa fa-save"></i> Save</button>
                </div>

            </form>
        </div>
    </div> -->
    <div class="card">
      <div class="card-header bg-white font-weight-bold">
          <h2 >{{$page_title}}
          </h2>

      </div>
        <div class="card-body ">
          <form method="POST" action="{{route('transfer.sort')}}">@csrf
          <div class="row">
            <div class="col-md-4">
                <input type="text" class="form-control form-control-lg" name="daterange" id="sort" value="">
            </div>
            <div class="col-md-4">
              <button class="btn btn-primary"name="button">Sort</button>
              @if (Request::route()->getName() == 'transfer.sort')
                <a href="{{route('admin.transfer_book')}}" class="btn btn-warning">reset</a>
              @endif
            </div>
          </div>
          </form>
            <table id='table_book' class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Car</th>
                    <th>Time</th>
                    <th>Flight Time</th>
                    <th>Arrive Time</th>
                    <th>Airlines</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Another Contact</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($cars_transaction as $k => $data)
                    <tr>
                        <td data-label="SL">{{++$k}}</td>
                        <td data-label="Title">{{($data->car_type) ?? ''}}</a></td>

                        <td data-label="Title">{{($data->transaction_date) ?? ''}}</td>
                        {{-- <td data-label="Content"><strong>{{($data->content) ?? ''}}</strong></td> --}}
                        <td data-label="Title">{{($data->flight_number) ?? ''}}</td>
                        <td data-label="Title">{{($data->arrival_time) ?? ''}}</td>
                        <td data-label="Title">{{($data->airline) ?? ''}}</td>
                        <td data-label="Title">{{$data->first_name.' '.$data->last_name}}</td>
                        <td>{{$data->phone}}</td>
                        <td>{{$data->email}}</td>
                        <td data-label="Title">{{($data->another_contact) ?? ''}}</td>
                         {{-- <td data-label="Content"><button class="btn btn-primary" data-toggle="modal" data-target="#Detail" onClick="details('{{$data->id}}')">detail</button></td> --}}
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $news->render() }} --}}
        </div>
    </div>


    <div class="modal fade" id="Detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                {{-- <form method="post" action="{{ route('blog.delete') }}">@csrf
                    {{ method_field('DELETE') }} --}}

                    <div class="modal-body">
                        <div class="col-12">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-3">
                                Full name :
                              </div>
                              <div class="col-3">
                                <strong id="fullname"></strong>
                              </div>
                              <div class="col-3">
                                Phone :
                              </div>
                              <div class="col-3">
                                <strong id="phone_number"></strong>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-3">
                                Arrival time:
                              </div>
                              <div class="col-3">
                                <strong id="arrival"></strong>
                              </div>
                              <div class="col-3">
                                Flight Number :
                              </div>
                              <div class="col-3">
                                <strong id="flight"></strong>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              {{-- <div class="col-3">
                                Passport ID :
                              </div>
                              <div class="col-3">
                                <strong id="passport_id"></strong>
                              </div> --}}
                              <div class="col-3">
                                Email :
                              </div>
                              <div class="col-3">
                                <strong id="email"></strong>
                              </div>
                            </div>
                          </div>
                          {{-- <div class="form-group">
                            <div class="row">
                              <div class="col-3">
                                Another contact :
                              </div>
                              <div class="col-3">
                                <strong id="another_contact"></strong>
                              </div>
                            </div>
                          </div> --}}
                        </div>
                        <hr>
                    </div>

                    <div class="modal-footer">
                        {{-- <button type="submit" class="btn btn-success"> Yes</button> --}}
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>&nbsp;
                    </div>

                {{-- </form> --}}

            </div>
        </div>
    </div>

@endsection
@section('script')
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> --}}
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> --}}
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  {{-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script> --}}
    <script>
        $(document).ready(function () {
          $('#table_book').DataTable({
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
            $('#sort').daterangepicker({
              @if (Request::route()->getName() == 'transfer.sort')
                startDate: '{{$daterange['start']}}',
                endDate: '{{$daterange['end']}}'
              @endif
            })
          });
          $('.buttons-excel').addClass('btn btn-success')
    </script>
@endsection
