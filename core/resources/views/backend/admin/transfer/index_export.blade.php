@extends('backend.master')
@section('title',"Transer data")
@section('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@stop
@section('content')

    <div class="card">
      <div class="card-header bg-white font-weight-bold">
          <h2 >Data export
          </h2>
        </div>
        <div class="card-body ">
          <table class="table_export table-hover">
              <thead>
              <tr>
                  <th>#</th>
                  <th>Car</th>
                  <th>Time</th>
                  <th>Flight Time</th>
                  <th>Arrive Time</th>
                  <th>Airlines</th>
                  <th>Fullname</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Another contact</th>
              </tr>
              </thead>
              <tbody>
              {{-- {{dd($news)}} --}}
              @foreach($data_export as $k => $data)
                  <tr>
                      <td data-label="SL">{{++$k}}</td>
                      <td data-label="Title">{{($data->car_type) ?? ''}}</a></td>

                      <td data-label="Title">{{($data->transaction_date) ?? ''}}</td>
                      {{-- <td data-label="Content"><strong>{{($data->content) ?? ''}}</strong></td> --}}
                      <td data-label="Title">{{($data->flight_number) ?? ''}}</td>
                      <td data-label="Title">{{($data->arrival_time) ?? ''}}</td>
                      <td data-label="Title">{{($data->airline) ?? ''}}</td>
                      <td data-label="Title">{{$data->first_name.' '.$data->last_name}}</td>
                      <td data-label="Title">{{$data->phone}}</td>
                      <td data-label="Title">{{$data->email}}</td>
                      <td data-label="Title">{{$data->another_contact}}</td>
                       {{-- <td data-label="Content"><button class="btn btn-primary" data-toggle="modal" data-target="#Detail" onClick="details('{{$data->id}}')">detail</button></td> --}}
                  </tr>
              @endforeach

              </tbody>
          </table>
        </div>
    </div>

@endsection
@section('script')
    <script>
    $(document).ready(function() {
      // $('.table_export').DataTable();
    } );
    $('.table_export').DataTable();
        // $( "#detail" ).on( "click", function() {
        function details(id){
          $.ajax({
            url: '{{url('transfer/detail')}}/'+id,
            type: 'GET',
            success : function (response) {
              $('#fullname').text(response.first_name+' '+response.last_name)
              $('#phone_number').text(response.phone ?response.phone :'null')
              $('#arrival').text(response.arrival_time)
              $('#flight').text(response.flight_number)
              $('#passport_id').text(response.passport_id)
              $('#email').text(response.email)
              console.log(response)
            },
          });
        }
          // $.ajax({
          //   url: '{{url('/confirmcompanylicense/allow')}}/'+id,
          //   type: 'GET',
          //   success : function (response) {
          //     $('#divLoading').removeClass('show');
          //     $('#background').removeClass('show');
          //     // alert(response);
          //     swal({
          //       title: 'อนุมัติ!',
          //       text: 'อนุมัติใบอนุญาตบริษัทนี้แล้ว',
          //       type: 'success',
          //       confirmButtonClass: "btn btn-success",
          //       buttonsStyling: false
          //     }).catch(swal.noop)
          //     table.clear();
          //     table.rows.add(response).draw();
          //     baguetteBox.run('.tz-gallery');
          //   },
          //   cache: false,
          //   contentType: false,
          //   processData: false
          // });
        // });
    </script>
@endsection
