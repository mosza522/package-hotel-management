@extends('backend.master')
@section('title',"News Title")
@section('content')
    <!-- <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 > Cms Content
            </h2>

        </div>
        <div class="card-body ">
            <form action="{{route('admin.web_setting.section.store',['promotions','promotions-section'])}}" method="post">@csrf
                <div class="form-group">
                    <label for="title_1">Title 1</label>
                    <input type="text" class="form-control form-control-lg" id="title_1" name="title_1" value="{{web_setting()->blog_blog_section_title_1}}">
                </div>
                <div class="form-group">
                    <label for="title_2">Title 2</label>
                    <input type="text" class="form-control form-control-lg" id="title_2" name="title_2" value="{{web_setting()->blog_blog_section_title_2}}">
                </div>
                <div class="form-group">
                    <hr/>
                    <button type="submit" class="btn btn-tsk btn-block"><i class="fa fa-save"></i> Save</button>
                </div>

            </form>
        </div>
    </div> -->
    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <h2 >{{$page_title}}
                <a href="{{route('news.create')}}" class="btn btn-success btn-md float-right ">
                    <i class="fa fa-plus"></i> Add News
                </a>
            </h2>

        </div>
        <div class="card-body ">

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>title</th>
                    <th>Content</th>
                    <th>Image</th>
                    <th>Expire Date</th>
                    <th>Status</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                {{-- {{dd($news)}} --}}
                @foreach($news as $k => $data)
                    <tr>
                        <td data-label="SL">{{++$k}}</td>
                        <td data-label="Title">{{($data->news_title) ?? ''}}</td>
                        {{-- <td data-label="Content"><strong>{{($data->content) ?? ''}}</strong></td> --}}
                        <td data-label="Content"><button class="btn btn-primary" data-toggle="modal" data-target="#ContentModal" onClick="details('{{$data->id}}')">detail</button></td>
                        <td>
                          @if($data->picture == null)
                              <img style="width: 200px"
                                   src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image"
                                   alt="...">
                          @else
                              <img style="width: 200px" src="{{ asset('assets/backend/image/news') }}/{{ $data->picture }}"
                                   alt="...">
                          @endif
                        </td>
                        <td data-label="Date">{{$data->expire_date}}</td>
                        <td data-label="Status">
                            <span class="badge  badge-pill  badge-{{ $data->status ==0 ? 'warning' : 'success' }}">{{ $data->status == 0 ? 'Deactive' : 'Active' }}</span>
                        </td>
                        <td data-label="Action">
                            <a href="{{route('news.edit',$data->id)}}" class="btn btn-primary  btn-icon btn-pill" title="Edit">
                                <i class="fa fa-edit"></i>
                            </a>


                            <button type="button" class="btn btn-danger  btn-icon btn-pill delete_button" title="Delete"
                                    data-toggle="modal" data-target="#DelModal"
                                    data-id="{{ $data->id }}">
                                <i class='fa fa-trash'></i>
                            </button>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{-- {{ $news->render() }} --}}
        </div>
    </div>


    <div class="modal fade" id="DelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2"><i class='fa fa-trash'></i> Delete !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <form method="post" action="{{ route('news.delete') }}">@csrf
                    {{ method_field('DELETE') }}

                    <div class="modal-body">
                        <input type="hidden" name="id" class="abir_id" value="0">
                        <strong>Are you sure you want to Delete ?</strong>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success"> Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>&nbsp;
                    </div>

                </form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="ContentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Content</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                {{-- <form method="post" action="{{ route('blog.delete') }}">@csrf
                    {{ method_field('DELETE') }} --}}

                    <div class="modal-body">
                        <input type="hidden" name="id" class="abir_id" value="0">
                        <strong id="content"></strong>
                    </div>

                    <div class="modal-footer">
                        {{-- <button type="submit" class="btn btn-success"> Yes</button> --}}
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>&nbsp;
                    </div>

                {{-- </form> --}}

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on("click", '.delete_button', function (e) {
                var id = $(this).data('id');
                $(".abir_id").val(id);
            });
        });
        // $( "#detail" ).on( "click", function() {
        function details(id){
          $.ajax({
            url: '{{url('news/detail')}}/'+id,
            type: 'GET',
            success : function (response) {
              $('#content').text(response)
            },
          });
        }
          // $.ajax({
          //   url: '{{url('/confirmcompanylicense/allow')}}/'+id,
          //   type: 'GET',
          //   success : function (response) {
          //     $('#divLoading').removeClass('show');
          //     $('#background').removeClass('show');
          //     // alert(response);
          //     swal({
          //       title: 'อนุมัติ!',
          //       text: 'อนุมัติใบอนุญาตบริษัทนี้แล้ว',
          //       type: 'success',
          //       confirmButtonClass: "btn btn-success",
          //       buttonsStyling: false
          //     }).catch(swal.noop)
          //     table.clear();
          //     table.rows.add(response).draw();
          //     baguetteBox.run('.tz-gallery');
          //   },
          //   cache: false,
          //   contentType: false,
          //   processData: false
          // });
        // });
    </script>
@endsection
