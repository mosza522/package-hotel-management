@extends('backend.master')
@section('title',"News")
@section('style')
    <link href="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet">
@stop
@section('content')
    {{-- {{dd(session()->all())}} --}}
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('admin.news')}}" class="btn btn-success btn-md float-right">
                <i class="fa fa-eye"></i> All news
            </a>
        </div>

        <form role="form" method="POST" action="{{route('news.update')}}" name="editForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
                <input type="hidden" name="id" value="{{$news->id}}">
                    <div class="form-group">
                        <h5> Title</h5>
                        <div class="input-group">
                            <input type="text" class="form-control form-control-lg" value="{{$news->news_title}}"
                                   name="news_title">
                            <div class="input-group-append"><span class="input-group-text">
                                            <i class="fa fa-font"></i>
                                            </span>
                            </div>
                        </div>
                        @if ($errors->has('news_title'))
                            <div class="error">{{ $errors->first('news_title') }}</div>
                        @endif

                    </div>


                    {{-- <div class="form-group">
                        <h5>Category</h5>
                        <select name="cat_id" id="cat_id" class="form-control form-control-lg" disabled>
                            <option value="">Select Category</option>
                            @foreach($category as $data)
                                <option value="{{$data->id}}"  @if($post->cat_id == $data->id) selected @endif>{{$data->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('cat_id'))
                            <div class="error">{{ $errors->first('cat_id') }}</div>
                        @endif
                    </div> --}}


                    <div class="form-group">
                        <h5>Image</h5>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"
                                 data-trigger="fileinput">
                                @if($news->picture == null)
                                    <img style="width: 200px"
                                         src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Fetured Image"
                                         alt="...">
                                @else
                                    <img style="width: 200px" src="{{ asset('assets/backend/image/news') }}/{{ $news->picture }}"
                                         alt="...">
                                @endif
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
                                                <span class="btn btn-info btn-file">
                                                    <span class="fileinput-new bold uppercase"><i
                                                                class="fa fa-file-image-o"></i> Select image</span>
                                                    <span class="fileinput-exists bold uppercase"><i
                                                                class="fa fa-edit"></i> Change</span>
                                                    <input type="file" name="picture" accept="image/*">
                                                </span>
                                <a href="#" class="btn btn-danger fileinput-exists bold uppercase"
                                   data-dismiss="fileinput"><i class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                        @if ($errors->has('picture'))
                            <div class="error">{{ $errors->first('picture') }}</div>
                        @endif

                    </div>


                    <div class="form-group">
                        <h5>Content</h5>
                        <textarea name="content" id="area1" cols="30" rows="12" class="form-control form-control-lg">{{$news->content}}</textarea>
                        @if ($errors->has('content'))
                            <div class="error">{{ $errors->first('content') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <h5> Expire Date</h5>
                        <div class="input-group">
                            <input type="text" class="expire_date form-control form-control-lg" value="{{$news->expire_date}}"
                                   name="expire_date">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-clock-o"></i>
                                </span>
                            </div>
                        </div>
                        @if ($errors->has('expire_date'))
                            <div class="alert alert-danger">{{ $errors->first('expire_date') }}</div>
                        @endif

                    </div>
                    {{-- <div class="form-group">
                        <h5>Agreements</h5>
                        <textarea name="agreements" id="area2" cols="30" rows="12" class="form-control form-control-lg">{{$post->agreements}}</textarea>
                        @if ($errors->has('agreements'))
                            <div class="error">{{ $errors->first('agreements') }}</div>
                        @endif
                    </div> --}}


                    <div class="form-group">
                        <h5>Status</h5>
                        <input data-toggle="toggle" data-onstyle="success" data-offstyle="danger"
                               data-width="100%" type="checkbox"
                               name="status" {{$news->status == "1" ? 'checked' : '' }}>
                    </div>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-primary btn-block btn-lg" type="submit">Update News</button>
            </div>

        </form>
    </div>


@endsection

@section('import-script')
    <!-- <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script> -->
    <script src="{{ asset('assets/admin/js/bootstrap-fileinput.js') }}"></script>
@stop
@section('script')
  <script src="{{ asset('assets/plugin/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('assets/admin/js/nicEdit-latest.js') }} "></script> -->
    <script type="text/javascript" src="{{ asset('assets/plugin/niceditor/nicEdit.js') }} "></script>

    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            // new nicEditor({fullPanel: true}).panelInstance('area1');
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area1');
            new nicEditor({
                iconsPath : '{{ asset('assets/plugin/niceditor/nicEditorIcons.gif') }}',
                fullPanel : true
            }).panelInstance('area2');
        });
        $( function() {
          $( ".expire_date" ).datepicker({ format: 'yyyy-mm-dd' }).val();
        } );
    </script>
@stop
