<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <link href="{{ URL::asset('assets/plugin/bootstrap-4.0.0/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
    <title>Booking Test</title>
</head>

<body>
    <div class="container">
        <h1>Booking Test</h1>
        <hr>
        <h4>Input section</h4>
        <form action="http://127.0.0.1:8000/api/test/rooms/booking" method="post">
            <div class="form-group">
                <div class="form-group">
                    <label for="username">username:</label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="checkin_date">checkin_date:</label>
                        <input type="text" class="form-control" id="checkin_date" name="checkin_date">
                    </div>
                    <div class="col">
                        <label for="checkout_date">checkout_date:</label>
                        <input type="text" class="form-control" id="checkout_date" name="checkout_date">
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="total_rooms">Room</label>
                        <input type="number" class="form-control" id="total_rooms" name="total_rooms">
                    </div>
                    <div class="col">
                        <label for="total_guest">Guest</label>
                        <input type="number" class="form-control" id="total_guest" name="total_guest">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>

        <hr>
        <h4>Table: rooms_keeper</h4>
        <div class="text-center" style="">
            <table class="table table-bordered table-sm" style="width: 50%;">
                <thead>
                    <tr>
                        <th scope="col" width="50%">Date</th>
                        <th scope="col" width="50%">Rooms Available</th>
                    </tr>
                </thead>
                @foreach ($rooms_per_dates as $date)
                <tbody>
                    <tr>
                        <td>{{$date['date']}}</td>
                        <td>{{$date['total_rooms']}}</td>
                    </tr>
                </tbody>
                @endforeach
            </table>
            {{$rooms_per_dates->links()}}
        </div>

        <hr>
        <h4>Table: room_handle</h4>
        <div class="text-center" style="">
            <div class="text-center" style="">
                <table class="table table-bordered table-sm" style="">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">User_id</th>
                            <th scope="col">Guest Name</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    @foreach ($rooms as $date)
                    <tbody>
                        <tr>
                            <td>{{$date['date']}}</td>
                            <td>{{$date['user_id']}}</td>
                            <td>{{$date['guest_name']}}</td>
                            <td>{{$date['status']}}</td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
                {{$rooms->links()}}
            </div>
        </div>

        <hr>
        <h4>Table: room_history_logs</h4>
        <div class="text-center" style="">
            <div class="text-center" style="">
                <table class="table table-bordered table-sm" style="">
                    <thead>
                        <tr>
                            <th scope="col">user_id</th>
                            <th scope="col">activity</th>
                            <th scope="col">created_at</th>
                        </tr>
                    </thead>
                    @foreach ($histories as $history)
                    <tbody>
                        <tr>
                            <td>{{$history['user_id']}}</td>
                            <td>{{$history['activity']}}</td>
                            <td>{{$history['created_at']}}</td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
                {{$histories->links()}}
            </div>
        </div>
    </div>

</body>

</html>
