<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// AUTHENTICATION API
Route::post('login','apis\AuthController@login')->name('login-api');

Route::group(['middleware' => ['auth:api-user']],function () {
  Route::get('posts', 'apis\AppPostsController@getAllPosts')->name('getAllPosts');

  // cancle room **request authen and date**
  Route::post('rooms/cancle', 'apis\AppRoomsController@cancle') -> name('cancleRoom');
  // cancle room **request authen , package_id and slot_id**
  Route::post('rooms/cancleAllPackage', 'apis\AppRoomsController@cancleAllPackage') -> name('cancleAllPackage');
});

Route::group(['middleware' => ['auth:api-register']],function () {

  // PACKAGE AND BUYING PACKAGES
  Route::get('getPackages', 'apis\AppPackageController@getPackage')->name('getPackage'); // To get packages in the system.
  Route::post('packages/booking', 'apis\AppPackageController@mainPackageBooking') -> name('mainPackageBooking'); // To booking package.
  Route::get('packages', 'apis\AppPackageController@getMyPackageV2') -> name('getMyPackageV2'); // To get packages that user has own.


  // ROOMS AND BOOKING ROOMS
  Route::get('rooms', 'apis\AppRoomsController@getRoomsAllDays') -> name('getRoomsAllDays'); // Get all rooms.
  Route::post('rooms/booking', 'apis\AppRoomsController@booking') -> name('booking'); // Booking room.
  Route::put('rooms/confirm', 'apis\AppRoomsController@confirmReservation') -> name('confirmReservation'); // To confirm or changes guest of the room.


  // NEWS
  Route::get('getNews', 'apis\AppNewsController@getNewsV2')->name('getNewsV2');

});

///
Route::get('test', 'apis\TestController@index')->name('test');
Route::get('testResponse', 'apis\TestController@testResponse')->name('testResponse');

// Member Management
Route::get('getAppID', 'apis\AppRegisterController@getAppID')->name('getAppID');
Route::post('loginMember', 'apis\AppRegisterController@loginMember')->name('loginMember');
Route::post('registerMember', 'apis\AppRegisterController@registerMember')->name('registerMember');
Route::post('resetPasswordMember', 'apis\AppRegisterController@resetPasswordMember')->name('resetPasswordMember');

// Profile
Route::post('getProfiles', 'apis\AppProfileController@getProfile')->name('getProfile');
Route::post('getHistorys', 'apis\AppProfileController@getHistory')->name('getHistory');

// Package Management
Route::post('registerPackages', 'apis\AppPackageController@registerPackage')->name('registerPackage');
Route::post('registerPackageNews', 'apis\AppPackageController@registerPackageNew')->name('registerPackageNew');
Route::post('addPackages', 'apis\AppPackageController@addPackage')->name('addPackage');
// Route::post('getPackages', 'apis\AppPackageController@getPackage')->name('getPackage');
Route::post('storePackages', 'Backend\Admin\HotelConfigure\PackageMasterController@store')->name('storePackage');
Route::post('getPackageDetails', 'apis\AppPackageController@getPackageDetail')->name('getPackageDetail');
Route::post('getMyPackages', 'apis\AppPackageController@getMyPackage')->name('getMyPackage');
Route::post('getMyAvailablePackages', 'apis\AppPackageController@getMyAvailablePackage')->name('getMyAvailablePackage');
Route::post('getMyAvailablePackageDetails', 'apis\AppPackageController@getMyAvailablePackageDetail')->name('getMyAvailablePackageDetail');
Route::post('getPromotions', 'apis\AppPackageController@getPromotion')->name('getPromotion');
Route::post('getPromotionDetails', 'apis\AppPackageController@getPromotionDetail')->name('getPromotionDetail');
Route::post('getPromotionCodes', 'apis\AppPromotionController@getPromotionCode')->name('getPromotionCode');

// Room
Route::post('getRooms', 'apis\AppRoomController@getRoom')->name('getRoom');
Route::post('getRoomDetails', 'apis\AppRoomController@getRoomDetail')->name('getRoomDetail');
Route::post('getAvailableRooms', 'apis\AppRoomController@getAvailableRoom')->name('getAvailableRoom');
Route::post('getAvailableRoombyFloors', 'apis\AppRoomController@getAvailableRoombyFloor')->name('getAvailableRoombyFloor');
Route::get('getFloors', 'apis\AppRoomController@getFloor')->name('getFloor');
Route::get('getRoomTypes', 'apis\AppRoomController@getRoomType')->name('getRoomType');


// News
// Route::get('getNews', 'apis\AppNewsController@getNews')->name('getNews');
Route::get('getNewsDetails/{newsId}', 'apis\AppNewsController@getNewsDetail')->name('getNewsDetail');

// Point&Reward Management
Route::post('getPoints', 'apis\AppPointController@getPoint')->name('getPoint');
Route::post('getRewards', 'apis\AppRewardController@getReward')->name('getReward');
Route::post('getRewardDetails', 'apis\AppRewardController@getRewardDetail')->name('getRewardDetail');
Route::post('getMyRewards', 'apis\AppRewardController@getMyReward')->name('getMyReward');

Route::post('addPoints', 'apis\AppPointController@addPoint')->name('addPoint');
Route::post('redeemPoints', 'apis\AppPointController@redeemPoint')->name('redeemPoint');

Route::post('redemptionRewards', 'apis\AppRewardController@redemptionReward')->name('redemptionReward');

// Partner
Route::post('addPartners', 'apis\AppPartnerController@addPartner')->name('addPartner');
Route::post('editPartners', 'apis\AppPartnerController@editPartner')->name('editPartner');

//Payment
Route::post('requestPayments', 'apis\AppReservationController@index')->name('requestPayment');
Route::post('requestPaymentMethods', 'apis\AppReservationController@index')->name('requestPaymentMethod');

// Reservation
Route::post('bookingRooms', 'apis\AppReservationController@bookingRoom')->name('bookingRoom');
Route::post('calculatePrices', 'apis\AppReservationController@calculatePrice')->name('calculatePrice');
Route::post('reservations', 'apis\AppReservationController@reservations')->name('reservations');

// Assignment
Route::post('assignmentPackages', 'apis\AppAssignmentController@assignmentPackage')->name('assignmentPackage');

//Package Exchange
Route::post('postPackageMemberExchanges', 'apis\AppPackageExchangeController@postPackageMemberExchange')->name('postPackageMemberExchange');
// Route::post('requestPackageMemberExchanges', 'apis\AppPackageExchangeController@requestPackageMemberExchange')->name('requestPackageMemberExchange');
Route::post('requestPackageMemberExchanges', 'apis\AppPackageExchangeController@requestOnPostPackageMemberExchange')->name('requestOnPostPackageMemberExchange');
Route::post('approvePackageExchanges', 'apis\AppPackageExchangeController@approvePackageExchange')->name('approvePackageExchange');
Route::post('rejectPackageExchanges', 'apis\AppPackageExchangeController@rejectPackageExchange')->name('rejectPackageExchange');
Route::post('showPackageExchanges', 'apis\AppPackageExchangeController@showPackageExchange')->name('showPackageExchange');
Route::post('historyPackageExchanges', 'apis\AppPackageExchangeController@historyPackageExchange')->name('historyPackageExchange');
Route::post('packageExchangesDetails', 'apis\AppPackageExchangeController@packageExchangesDetail')->name('packageExchangesDetail');
Route::post('availablePackageExchanges', 'apis\AppPackageExchangeController@availablePackageExchange')->name('availablePackageExchange');

// Admin slot
Route::post('storeSlots', 'Backend\Admin\AvailableSlotController@store')->name('storeSlot');
// Route::post('showSlotbyYears', 'Backend\Admin\AvailableSlotController@showSlotbyYear')->name('showSlotbyYear');
// Route::post('showSlotYears', 'Backend\Admin\AvailableSlotController@showSlotYear')->name('showSlotYear');

// App slot
Route::post('getSlotYears', 'apis\AppAvailableSlotController@getSlotYear')->name('getSlotYear');
Route::post('getSlotbyYears', 'apis\AppAvailableSlotController@getSlotbyYear')->name('getSlotbyYear');
Route::post('getAvailableSlotbyYears', 'apis\AppAvailableSlotController@getAvailableSlotbyYear')->name('getAvailableSlotbyYear');


// ========= API Updated on 21 Jan 2020 =========
// API Test section
Route::group(['prefix' => 'v0', 'as' => 'v0'], function () {
    // Rooms
    // Route::get('rooms', 'apis\AppRoomsController@getRoomsAllDays') -> name('getRoomsAllDays'); //Get all rooms
    // Route::post('rooms/booking', 'apis\AppRoomsController@booking') -> name('booking'); //Booking room
    // Route::put('rooms/confirm', 'apis\AppRoomsController@confirmReservation') -> name('confirmReservation');

    // Package
    // Route::get('packages/{username}', 'apis\AppPackageController@getMyPackageV2') -> name('getMyPackageV2');
    // Route::post('packages/booking', 'apis\AppPackageController@mainPackageBooking') -> name('mainPackageBooking');
    // Route::get('packages/{package_id}', 'apis\AppPackageController@checkDateInPeriods') -> name('checkDateInPeriods');
    Route::get('packages/slots', 'apis\AppPackageController@showSlot') -> name('packageSlot');
    Route::get('packages/{package_id}/slots', 'apis\AppPackageController@getSlotsByPackageId') -> name('getSlotsByPackageId'); //getSlotsByPackageId

    // Get own package/rooms
    Route::get('rooms/{username}', 'apis\AppMyOwnController@getAllMine') ->name('getAllMine');

    // Mobile App users
    // This API will support user that just want to register as nomal register only
    Route::post('users', 'apis\AppRegisterController@regist') ->name('regist');

    // Mobile App Car Transfer
    Route::get('cartransfer', 'apis\AppCarTransferController@getCarTransferType') -> name('cartransfer');
    Route::post('cartransfer/booking', 'apis\AppCarTransferController@bookingCartransfer') -> name('bookingCartransfer');

    // Posts
    Route::get('posts', 'apis\AppPostsController@getAllPosts')->name('getAllPosts');
    Route::get('posts/{username}', 'apis\AppPostsController@getPostByUsername')->name('getPostByUsername');
    Route::post('posts', 'apis\AppPostsController@postsOffer')->name('postsOffer');
    Route::get('allmine/{username}', 'apis\AppMyOwnController@getAllMine') -> name('getAllMine');
    Route::put('posts/requests/accept','apis\AppPostsController@acceptRequests') -> name('acceptRequests');
    Route::put('posts/requests/reject','apis\AppPostsController@rejectRequest')->name('rejectRequest');
    Route::delete('posts','apis\AppPostsController@deletePosts')->name('deletePosts');

    // Requests
    Route::post('requests', 'apis\AppRequestsController@postOffer')->name('postOffer');
    Route::get('requests', 'apis\AppRequestsController@getMyRequest')->name('getMyRequest');

    // News
    Route::get('getNews', 'apis\AppNewsController@getNewsV2')->name('getNewsV2');

    // Users verify
    Route::put('users/verify', 'apis\AppRegisterController@verifyUser')->name('verifyUser');

    // Payments
    Route::get('payments/history/{user_id}', 'apis\AppPaymentsHistory@paymentsHistory')->name('paymentsHistory');
    Route::put('packages/paymore', 'apis\AppPaymentsController@paymore')->name('paymore');
});
///////////////

// Route::get(
//     'rooms/available/{date}', //Ex: /rooms/available/2020-1-10
//     'apis\AppAvailableRoomsController@availableOnDate'
// ) -> name('availableOnDate');

// Route::post(
//     'booking',
//     'apis\AppBookingRoomsController@bookingRoom'
// ) -> name('bookingRoom');

// Route::get('/test', function () {
//     // $check_in = "2020-1-1 00:00:00";
//     // $check_out = Carbon::parse($check_in);
//     // $dfInDay = $check_in -> diffInDays($check_out);
//     $total = \App\Model\RoomHandle::whereIn("date", ["2020-1-31","2020-1-22","2020-1-23"]) -> get();
//     // $lastTenDaysRecord = \App\Model\RoomHandle::whereDateBetween('date',(new Carbon)->subDays(30)->toDateString(),(new Carbon)->now()->toDateString() )->get();
//     return $total;
// });

// Route::get('booking/test', function () {
//     // Paginator::setPageName('rooms_keeper');
//     // $rooms_keeper = \App\Model\RoomsKeeper::paginate(15);
//     // Paginator::setPageName('room_handle');
//     // $room_handle = \App\Model\RoomHandle::paginate(15);
//     $rooms_keeper= \App\Model\RoomsKeeper::paginate(15);
//     $room_handle= \App\Model\RoomHandle::paginate(15);
//     $room_history_logs = \App\Model\AppRoomsHistoryLogsController::paginate(15);
//
//     // dd($rooms_keeper);
//     return View('booking_test')->with(
//       ['rooms_per_dates' => $rooms_keeper,'rooms' => $room_handle, 'histories' => $room_history_logs]
//   );
// });
