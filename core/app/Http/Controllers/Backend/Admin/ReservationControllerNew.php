<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\RoomHandle;
use Carbon\Carbon;
use Auth;
class ReservationControllerNew extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = "Reservation";
        $data['reservation'] = RoomHandle::leftJoin('users','room_handle.user_id','users.id')
        ->where('date','>=',Carbon::now())
        ->where('room_handle.status','=','reserved')
        ->where('room_handle.user_id','!=','')
        ->select('room_handle.*','users.first_name','users.last_name','users.id as user_id')
        ->orderBy('date')
        // ->paginate(15);
        ->get();
        // dd($data['reservation']);
        return view('backend.admin.reservationnew.index',$data);
    }
    public function change_status($data)
    {
      $data = explode(',',$data);
      $roomhandle = RoomHandle::find($data[1]);
      
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $data[1];
      $logs->value_past = $roomhandle ->status;
      $logs->value_current = $data[0];
      $logs->created_by = $data[2];
      $logs->table = 'roomhandle';
      $logs->save();

      $roomhandle ->status = $data[0];

      if($roomhandle ->save()){
        $notification = array('success' => 'Changed Status Successfully');
        return redirect()->back()->with($notification);
      }else{
        $notification = array('error' => 'Problem With Changing Status');
        return redirect()->back()->with($notification);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
