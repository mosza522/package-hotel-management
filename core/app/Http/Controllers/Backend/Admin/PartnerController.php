<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Partner;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use File;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         $data['page_title'] = "All Partner";
         $data['partner'] = Partner::get();
         // dd($data);
         return view('backend.admin.partner.index', $data);
         //
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Add Partner';
      return view('backend.admin.partner.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'name' => 'required',
          'description' => 'required',
          'start_date' => 'required | date',
          'end_date' => 'required | date'
          // 'image' => 'required | mimes:jpeg,jpg,png | max:1000'
      ]
      );
      // $start_date_timestamp = strtotime($request->start_date);
      // $end_date_timestamp = strtotime($request->end_date);
      // return date('Y-m-d H:i:s', $old_date_timestamp);
      $in = Input::except('_token');
      // $in['start_date'] = date('Y-m-d H:i:s', $start_date_timestamp);
      // $in['end_date'] = date('Y-m-d H:i:s', $end_date_timestamp);
      $in['partner_status'] =  $request->status == 'on' ? '1' : '0';
      $res = Partner::create($in);
      if ($res) {
          $notification = array('success' => 'Updated Successfully!');
          return redirect()->route('admin.partner')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Updating Partner');
          return redirect()->route('admin.partner')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Edit Partner';
      $data['partner'] = Partner::findOrFail($id);
      // $data['category'] = BlogCategory::where('id', $this->cat_id)->whereStatus(1)->get();
      return view('backend.admin.partner.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $data = Partner::find($request->id);
      $request->validate([
          'name' => 'required',
          'description' => 'required',
          'start_date' => 'required | date',
          'end_date' => 'required | date'
          // 'image' => 'required | mimes:jpeg,jpg,png | max:1000'
      ],
          [
              'name.required' => 'News Title Must not be empty',
              'description.required' => 'News Description Must not be empty'
          ]
      );


      $in = Input::except('_token');
      $in['partner_status'] =  $request->status == 'on' ? '1' : '0';
      $res = $data->fill($in)->save();

      if ($res) {
          $notification = array('success' => 'Updated Successfully!');
          return back()->with($notification);
      } else {
          $notification = array('error' => 'Problem With Updating News!');
          return back()->with($notification);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      // $request->validate([
      //     'id' => 'required'
      // ]);
      $data = Partner::findOrFail($request->id);
      // $path = 'assets/backend/image/news/';
      // @unlink($path.$data->image);
      // @unlink($path.$data->thumb);
      $res =  $data->delete();

      if ($res) {
          $notification = array('success' => 'Partner Delete Successfully!');
          return back()->with($notification);
      } else {
          $notification = array('error' => 'Problem With Deleting Partner!');
          return back()->with($notification);
      }
    }
    public function detail($id)
    {
      $data =  Partner::where('id',$id)->select('description')->first();
      return $data->description;
    }
}
