<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\RoomsKeeper;
use Carbon\Carbon;
class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = "Room Management";
        $data['data'] = RoomsKeeper::where('date','>',Carbon::now())->get();
        return view('backend.admin.room.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = "Room Management";
        $data['last_date'] = RoomsKeeper::orderBy('id','DESC')->limit(1)->first();
        // dd($data);
        return view('backend.admin.room.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = explode(' - ',$request->date);
        $date_start = Carbon::parse($date[0]);
        $date_end = Carbon::parse($date[1]);
        while ($date_start->diffInDays($date_end) >= 0) {
          $roomsKeeper = new RoomsKeeper;
          $roomsKeeper->date = $date_start;
          $roomsKeeper->total_rooms = $request->total_rooms;
          $roomsKeeper ->save();
          if($date_start->diffInDays($date_end) == 0){
            break;
          }else{
            $date_start = $date_start->addDays(1);
          }
        }
        $notification = array('success' => 'Created Successfully');
        return redirect()->route('admin.roommanagement')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = explode(",",$request->id);
        if (is_numeric($request->all_rooms) ) {
          $roomkeeper =  RoomsKeeper::find($data[0]);

          $logs = new \App\Model\LogsHistory;
          $logs->activity = 'update';
          $logs->id_data = $data[0];
          $logs->value_past = $roomkeeper->all_rooms;
          $logs->value_current = $request->all_rooms;
          $logs->created_by = $data[1];
          $logs->table = 'roomhandle';
          $logs->save();

          $roomkeeper->all_rooms = $request->all_rooms;
          $roomkeeper->save();
          return response()->json(['return'=>'success']);
        }else{
          $roomkeeper =  RoomsKeeper::find($data[0]);
          return response()->json(['return'=>'Value is not number','data'=>$roomkeeper]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
