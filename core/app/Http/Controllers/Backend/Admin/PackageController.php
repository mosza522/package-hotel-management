<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PackageMaster;
use App\Model\PackageMember;
use App\Model\User;
use App\Model\Payment;
use App\Model\RoomHandle;
use File;
use Image;
use App\Http\Helper\MimeCheckRules;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Model\PackageSlot;
use App\Model\RoomsKeeper;
class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['page_title'] = "Package";
      $data['package'] = PackageMaster::where('package_duration','>',0)->get();
        return view('backend.admin.package.index',$data);
    }
    public function index_package_member()
    {
      $data['page_title'] = "Member Package";
      $data['package_member'] = User::where('user_type','=','member')
      ->leftJoin('package_members','users.id','=','package_members.user_id')
      ->leftJoin('package_masters','package_members.package_id','=','package_masters.id')
      ->leftJoin('package_slot','package_members.slot_id','=','package_slot.id')
      ->select('users.*','package_masters.package_name','package_members.slot_id','package_slot.start_date','package_slot.end_date')
      ->get();
      // dd($data);
      return view('backend.admin.package.index-packagemember',$data);
    }
    public function package_member_choose($id)
    {
      $data['page_title'] = "Member Package";
      $data['packages'] = PackageMaster::where('package_duration','>',0)
      ->where('package_status',1)
      ->get();
      $data['data'] = User::where('user_type','=','member')->where('id',$id)->first();
      return view('backend.admin.package.packagemember-choose',$data);
    }
    public function package_member_choose_store(Request $r)
    {
      $r->validate([
        'package' => 'required',
        'slot' => 'required',
      ]);
      $slot = PackageSlot::find($r->slot);
      $period = PackageSlot::leftJoin('package_masters','package_slot.package_id','package_masters.id')
      ->where('package_id',$slot->package_id)
      ->where('week_no',$slot->week_no)
      ->where('year','>=',$slot->year)
      ->select('package_slot.*','package_masters.package_price')
      ->get();
      // dd($period[count($period)-1]->start_date);
      $last_payment = Payment::orderBy('id','DESC')->get();
        $payment_id = "";
      foreach ($last_payment as $key => $value) {
        $last = explode('-',$value->payment_id);
        if($last[0]=="offline" && intval($last[1])>0){
          if(strlen(intval($last[1])+1) == 1){
            $payment_id="offline-000".string(intval($last[1])+1);
          }elseif (strlen(intval($last[1])+1) == 2) {
            $payment_id="offline-00".string(intval($last[1])+1);
          }elseif (strlen(intval($last[1])+1) == 3) {
            $payment_id="offline-0".string(intval($last[1])+1);
          }elseif (strlen(intval($last[1])+1) == 4) {
            $payment_id="offline-".string(intval($last[1])+1);
          }
          break;

        }else{
          $payment_id="offline-0001";
        }
      }
      $payment = new Payment;
      $payment ->user_id = $r->user_id;
      $payment ->gateway_id = 0;
      $payment ->amount = $period[0]->package_price;
      $payment ->type = "offline";
      $payment ->payment_id = $payment_id;
      $payment ->save();


      $store = new PackageMember;
      $store ->user_id = $r->user_id;
      $store ->package_id = $r->package;
      $store ->slot_id = $r->slot;
      $store ->package_start_time = $period[0]->start_date;
      $store ->package_end_time = $period[count($period)-1]->end_date;
      $store ->package_member_status = 1;
      $store ->payment_id =$payment_id;
      $store->save();
      $user = User::find($r->user_id);
      $transaction = uniqid();
      foreach ($period as $key => $value) {
        $start=Carbon::parse($value->start_date);
        $end=Carbon::parse($value->end_date);

        while ($start->diffInDays($end) >= 0) {
          $room_keeper = RoomsKeeper::where('date',$start)->first();
          if($room_keeper->total_rooms > 0){
            $room_keeper->total_rooms = $room_keeper->total_rooms-1;
            $room_keeper->save();

            $roomhandle = new RoomHandle;
            $roomhandle ->user_id = $r->user_id;
            $roomhandle ->date =$start;
            $roomhandle->transaction_type = "package";
            $roomhandle ->transaction_id= $transaction;
            $roomhandle ->guest_name = $user->first_name;
            $roomhandle ->status = "reserved";
            $roomhandle ->package_id = $r->package;
            $roomhandle ->slot_id = $r->slot;
            $roomhandle ->save();
            if($start->diffInDays($end) == 0){
              break;
            }else{
              $start = $start->addDays(1);
            }
          }else{
            $notification = array('error' => $start.' Not enought room');
            return redirect()->route('package.member')->with($notification);
          }
        }
      }
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $id;
      // $logs->value_past = $data->volume;
      $logs->value_current = $r->user_id ;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'PackageMember';
      $logs->save();
      $notification = array('success' => 'Choose Package Successfully');
      return redirect()->route('package.member')->with($notification);
      // dd($r->all());
    }
    public function getslot($id)
    {
      $data = PackageSlot::where('package_id',$id)->get();
      return response()->json($data);
    }
    public function getperiod($id)
    {
      $data = PackageSlot::find($id);
      $period = PackageSlot::where('package_id',$data->package_id)
      ->where('week_no',$data->week_no)
      ->where('year','>=',$data->year)
      ->get();
      $text = '<table class="table table-hover">
        <thead>
          <th>Date</th>
          <th>Year</th>
        </thead>
        <tbody>
        ';
        foreach ($period as $key => $value) {
          $text.='<tr><td>'
            .Carbon::parse($value->start_date)->format('D jS M Y').' - '.Carbon::parse($value->end_date)->format('D jS M Y').'</td>'
            .'<td>'.$value->year.'</td>
          </tr>';
        }
        $text.='
        </tbody>
      </table>';
      return response()->json(array('period'=>$period,'period_text'=>$text));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create Package';
        return view('backend.admin.package.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'package_name' => 'required',
          'package_code' => 'required',
          'package_image' => 'required | mimes:jpeg,jpg,png | max:2048',
          'package_price' => 'required | numeric',
          'package_duration' => 'required | numeric',
          'package_description' => 'required',
          'package_agreement' => 'required',
      ]
      );

      $in = Input::except('_token');
      if($request->hasFile('package_image')){
          $file = explode('.',$request->file('package_image')->getClientOriginalName());
          $image = $request->file('package_image');
          $filename = 'package_'.time().$file[0].'.'.$file[1];
          $location = 'assets/backend/image/package/' . $filename;
          Image::make($image)->save($location);
          $in['package_image'] = $filename;
      }

      // if($request->hasFile('image')){
      //     $image = $request->file('image');
      //     $filename = 'news_thumb'.time().'.jpg';
      //     $location = 'assets/backend/image/news/' . $filename;
      //     Image::make($image)->resize(350,213)->save($location);
      //     $in['thumb'] = $filename;
      // }


      $in['package_status'] =  $request->status == 'on' ? '1' : '0';
      $res = PackageMaster::create($in);
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $id;
      // $logs->value_past = $data->volume;
      $logs->value_current = $request->package_name;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'Package';
      $logs->save();
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          $notification['package'] = PackageMaster::orderBy('id','desc')->select('id','package_duration')->first();
          return redirect()->route('slot.create',$notification['package']->id)->with($notification);
      } else {
          $notification = array('error' => 'Problem With Createing Package');
          return redirect()->route('admin.package')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function package_member_detail($id)
    {
      $data['page_title'] = "Package Detail";
      $data['data'] = User::leftJoin('package_members','users.id','=','package_members.user_id')
      ->leftJoin('package_masters','package_members.package_id','=','package_masters.id')
      ->leftJoin('package_slot','package_members.slot_id','=','package_slot.id')
      ->select('users.*','package_masters.package_name','package_masters.id as package_master_id','package_members.slot_id','package_slot.start_date'
      ,'package_slot.end_date','package_slot.week_no','package_slot.duration','package_slot.year')
      ->where('users.id','=',$id)
      ->first();
      $data['period'] = PackageSlot::where('package_id',$data['data']->package_master_id)
      ->where('week_no',$data['data']->week_no)
      ->where('year','>=',$data['data']->year)
      ->get();
      return view('backend.admin.package.packagemember-detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $request->validate([
          'package_name' => 'required',
          'package_code' => 'required',
          'package_price' => 'required | numeric',
          'package_duration' => 'required | numeric',
          'package_description' => 'required',
          'package_agreement' => 'required',
          'date' => 'required',
          'total_room' => 'required',
          'package_image' => 'nullable | mimes:jpeg,jpg,png | max:2048',
      ]);
      $package = PackageMaster::find($id);
      if($request->has('package_image')){
          $package_image = 'assets/backend/image/package/';
          @unlink($package_image.$package->package_image);
          $package->package_image = 'package_'.time().'.'.$request->package_image->getClientOriginalExtension();
          Image::make($request->package_image)->save($package_image.$package->package_image);
      }
      $package->package_name = $request->package_name;
      $package->package_code = $request->package_code;
      $package->package_price = $request->package_price;
      $package->package_duration = $request->package_duration;
      $package->package_description = $request->package_description;
      $package->package_agreement = $request->package_agreement;
      $package->package_status = $request->package_status == 'on' ? '1' : '0';
      $package->save();
      $slots = PackageSlot::where('package_id','=',$id)->get();
      $all_change = false;
      $data_change = array();
      $round_change = false;
      $status = array();
      for ($i=0; $i <count($slots) ; $i++) {
        if(!isset($request->status[$i])){
          array_push($status,0);
        }else{
          array_push($status,1);
        }
        $date = explode(' - ',$request->date[$i]);
        // $slot->start_date = Carbon::createFromFormat('m/d/Y H:i:s',$date[0].' 00:00:00');
        // $slot->end_date = Carbon::createFromFormat('m/d/Y H:i:s',$date[1].' 00:00:00');
        ($slots[$i]->start_date != Carbon::createFromFormat('m/d/Y H:i:s',$date[0].' 00:00:00')) ? $round_change = true : $round_change = false ;
        ($slots[$i]->end_date != Carbon::createFromFormat('m/d/Y H:i:s',$date[1].' 00:00:00')) ? $round_change = true :$round_change = false ;
        ($slots[$i]->total_room != $request->total_room[$i]) ? $round_change = true :$round_change = false ;
        ($slots[$i]->status != $status[$i]) ? $round_change = true :$round_change = false ;
        if($round_change){
          array_push($data_change,['id'=>$slots[$i]->id,'index'=>$i]);
          $all_change=true;
        }
      }

      if($all_change){
        foreach ($data_change as $key => $value) {
          $date = explode(' - ',$request->date[$value['index']]);
          $slot = PackageSlot::where('id','=',$value['id'])->first();
          $slot->start_date = Carbon::createFromFormat('m/d/Y H:i:s',$date[0].' 00:00:00');
          $slot->end_date = Carbon::createFromFormat('m/d/Y H:i:s',$date[1].' 00:00:00');
          $slot->year = Carbon::parse($slot->start_date)->format('Y');
          $slot->total_room = $request->total_room[$value['index']];
          $slot->status = $status[$value['index']];
          $slot->save();
        }
      }
      $status = array();
      for ($i=0; $i < count($request->date_add); $i++) {
        if($request->has('date_add')){
          if(!isset($request->status_add[$i])){
            array_push($status,0);
          }else{
            array_push($status,1);
          }
        $date = explode(' - ',$request->date_add[$i]);
        $start = Carbon::createFromFormat('m/d/Y H:i:s',$date[0].' 00:00:00');
        $end =Carbon::createFromFormat('m/d/Y H:i:s',$date[1].' 00:00:00');
        $slot = new PackageSlot;
        $slot->package_id = $id;
        $slot->week_no = $request->week_add[$i];
        $slot->year = $request->year_add[$i];
        $slot->duration = $request->package_duration;
        $slot->start_date = $start;
        $slot->end_date = $end;
        $slot->total_room = $request->total_room_add[$i];
        $slot->status = $status[$i];
        $slot->save();
        }
        for ($j=0; $j < $request->package_duration; $j++) {
          $roomkeeper = RoomsKeeper::where('date','=',$start)->first();
          if(count($roomkeeper)>0){
            $roomkeeper ->total_rooms = $request->total_room[$i]+$roomkeeper ->total_rooms;
            // $roomkeeper ->total_rooms = 99;
            $roomkeeper->save();
          }else{
            $roomkeeper = new RoomsKeeper;
            $roomkeeper ->date = $start;
            $roomkeeper ->total_rooms = $request->total_room[$i];
            $roomkeeper->save();
          }
            $start = $start->addDays(1);
        }
      }
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $id;
      // $logs->value_past = $data->volume;
      // $logs->value_current = $r->amount;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'Package';
      $logs->save();
      $notification = array('success' => 'Updated Successfully');
      return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function createSlot($id)
    {
      $data['package'] = PackageMaster::orderBy('id','desc')->select('id','package_duration')->first();
      $data['page_title'] = 'Create Slot';
      $data['date'] = Carbon::now();
      $data['date']->setISODate($data['date']->year,1);
      $data['date']->startOfWeek();
      // dd(($data['date']));
      return view('backend.admin.package.create-slot',$data);
    }
    public function storeSlot(Request $r)
    {
      // ini_set('max_input_vars','5000' );
      // dd($r->all());
      // dd(phpinfo());
      // ini_set('max_input_vars', 3000);
      // $r->validate([
      //     'date' => 'required',
      //     'total_room' => 'required',
      // ]
      // );
      // dd($r->date[]);
      // dd($r->all());
      // phpinfo();
      // ini_set('max_input_vars', 3000);
      $r->validate([
          'date' => 'required',
          'total_room' => 'required',
      ]
      );
      for ($i=0; $i < count($r->date); $i++) {
        $date = explode(' - ',$r->date[$i]);
        $start = Carbon::createFromFormat('m/d/Y H:i:s',$date[0].' 00:00:00');
        $end = Carbon::createFromFormat('m/d/Y H:i:s',$date[1].' 00:00:00');
        if($end->format('Y') > Carbon::now()->format('Y')+10){
          $notification = array('error' => 'error data it\'s too much ');
          return redirect()->route('admin.package')->with($notification);
        }
        $slot = new PackageSlot;
        $slot->package_id = $r->id;
        $slot->week_no = $r->week[$i];
        $slot->year = $r->year[$i];
        $slot->duration = $r->duration;
        $slot->start_date = $start;
        $slot->end_date = $end;
        $slot->total_room = $r->total_room[$i];
        $slot->status = $r->status[$i] == 'on' ? '1' : '0';
        $slot->save();
        for ($j=0; $j < $r->duration; $j++) {
          $roomkeeper = RoomsKeeper::where('date','=',$start)->first();
          if(count($roomkeeper)>0){
            $roomkeeper ->total_rooms = $r->total_room[$i]+$roomkeeper ->total_rooms;
            // $roomkeeper ->total_rooms = 99;
            $roomkeeper->save();
          }else{
            $roomkeeper = new RoomsKeeper;
            $roomkeeper ->date = $start;
            $roomkeeper ->total_rooms = $r->total_room[$i];
            $roomkeeper->save();
          }
            $start = $start->addDays(1);
        }
      }
      // $duration = microtime(true) - $start_time;
      // dd($duration/60);
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      $logs->id_data = $r->id;
      // $logs->value_past = $data->volume;
      // $logs->value_current = $r->amount;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'slot';
      $logs->save();
      $notification = array('success' => 'Created Successfully!');
      return redirect()->route('admin.package')->with($notification);
    }
    public function view($id)
    {
      $data['page_title'] = 'package' ;
      $data['package'] = PackageMaster::find($id);
      $data['slots'] = PackageSlot::where('package_id','=',$id)->get();
      $data['years'] = PackageSlot::where('package_id','=',$id)->select('year')->groupBy('year')->get();
      for ($i=0; $i < count($data['years']) ; $i++) {
        $data['years'][$i] = $data['years'][$i]->year;
      }
      return view('backend.admin.package.view',$data);
    }
}
