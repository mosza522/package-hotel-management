<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ENews;
use Illuminate\Support\Facades\Input;
use App\Mail\sendMail;
use Illuminate\Support\Facades\Mail;
use App\Model\User;
use App\Model\Register;
use Illuminate\Support\Facades\Storage;
class eNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['page_title'] = "E-News Letter";
      $data['enews'] = ENews::get();
      // dd($data);
      return view('backend.admin.enews.index', $data);
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = "E-News Letter";
      $user = User::where('user_type','member')->get();
      $guest = Register::get();
      $data['email'] = array();
      foreach ($user as $key => $value) {
        array_push($data['email'],$value->email);
      }
      foreach ($guest as $key => $value) {
        array_push($data['email'],$value->email);
      }
      return view('backend.admin.enews.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'subject' => 'required',
          'header' => 'required | email',
          'attach' => 'max:10240'
      ],
          [
              'subject.required' => 'Subject Must not be empty',
              'header.required' => 'From Must not be empty'
          ]
      );

      $in = Input::except('_token');
      if($request->to_user == 'some'){
        $in['to_user'] = json_encode($request->select_email);
      }
      if($request->to_user == 'member'){
        // dd($request->all());
        $to = User::where('user_type','member')->get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
        }
      }else if ($request->to_user == 'guest') {
        $to = Register::get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
        }
      }
      else if ($request->to_user == 'all') {
        $to = Register::get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
          // sleep(0.5);
        }
        $to = User::where('user_type','member')->get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
          // sleep(1);
        }
      }
      else{
        foreach ($request->select_email as $key => $value) {
          $data_mail = [
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value)->send($mail);
          // sleep(1);
        }

      }
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $request->id;
      // $logs->value_past = $data->van_price.' ,'.$data->saloon_price;
      $logs->value_current = $request->Subject;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'Enews';
      $logs->save();
      $res = ENews::create($in);


      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.enews')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Creating E-News');
          return redirect()->route('admin.enews')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Edit E-News';
      $data['enews'] = ENews::findOrFail($id);
      $user = User::where('user_type','member')->get();
      $guest = Register::get();
      $data['email'] = array();
      foreach ($user as $key => $value) {
        array_push($data['email'],$value->email);
      }
      foreach ($guest as $key => $value) {
        array_push($data['email'],$value->email);
      }
      // dd($data);
      // $data['category'] = BlogCategory::where('id', $this->cat_id)->whereStatus(1)->get();
      return view('backend.admin.enews.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $data = ENews::find($request->id);
      $request->validate([
          'subject' => 'required',
          'header' => 'required | email',
          'attach' => 'max:10240'
      ],
          [
              'subject.required' => 'Subject Must not be empty',
              'header.required' => 'From Must not be empty'
          ]
      );

      $in = Input::except('_token');
      if($request->to_user == 'some'){
        $in['to_user'] = json_encode($request->select_email);
      }
      if($request->to_user == 'member'){
        // dd($request->all());
        $to = User::where('user_type','member')->get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
        }
      }else if ($request->to_user == 'guest') {
        $to = Register::get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
        }
      }
      else if ($request->to_user == 'all') {
        $to = Register::get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
          // sleep(0.5);
        }
        $to = User::where('user_type','member')->get();
        foreach ($to as $key => $value) {
          $data_mail = [
            'user'=>$value,
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value->email)->send($mail);
          // sleep(1);
        }
      }
      else{
        foreach ($request->select_email as $key => $value) {
          $data_mail = [
            'detail'=>$request->all(),
            'attach'=>$request->has('attach')?$request->attach:null
          ];
          $mail = new sendMail($data_mail);
          Mail::to($value)->send($mail);
          // sleep(1);
        }

      }
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $request->id;
      $logs->value_past = $data->subject;
      $logs->value_current = $request->subject;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'Enews';
      $logs->save();

      $in = Input::except('_token');
      if($request->to_user == 'some'){
        $in['to_user'] = json_encode($request->select_email);
      }
      $res = $data->fill($in)->save();
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.enews')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Creating E-News');
          return redirect()->route('admin.enews')->with($notification);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $data = ENews::findOrFail($request->id);
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'delete';
      $logs->id_data = $request->id;
      // $logs->value_past = $data->subject;
      $logs->value_current = $data->subject;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'Enews';
      $logs->save();
      $res =  $data->delete();

      if ($res) {
          $notification = array('success' => 'E-News Delete Successfully!');
          return back()->with($notification);
      } else {
          $notification = array('error' => 'Problem With Deleting E-NEws!');
          return back()->with($notification);
      }
    }
}
