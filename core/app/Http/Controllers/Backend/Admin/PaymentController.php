<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\Model\PaymentDeposite;
class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = "Payment";
        $data['payment'] = Payment::leftJoin('users','payments.user_id','users.id')
        ->get();
        return view('backend.admin.payment-deposite.index',$data);

    }
    public function index_deposite()
    {
      $data['page_title'] = "Deposite";
      $data['deposite'] = PaymentDeposite::orderBy('id','DESC')->first();
      return view('backend.admin.payment-deposite.deposite',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function deposite_store(Request $r)
    {
      $r->validate([
        'deposite' => 'required | numeric'
      ]);
      $deposite = new PaymentDeposite;
      $deposite->deposite = $r->deposite;

      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $request->id;
      $logs->value_current = $r->deposite;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'PaymentDeposite';
      $logs->save();

      $deposite->save();

      $notification = array('success' => 'Created deposite Successfully');
      return redirect()->back()->with($notification);
    }
    public function deposite_update(Request $r)
    {
      $r->validate([
        'deposite' => 'required | numeric'
      ]);
      $deposite = PaymentDeposite::find($r->id);

      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $r->id;
      $logs->value_past = $deposite->deposite;
      $logs->value_current = $r->deposite;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'PaymentDeposite';
      $logs->save();
      
      $deposite->deposite = $r->deposite;
      $deposite->save();

      $notification = array('success' => 'Updated deposite Successfully');
      return redirect()->back()->with($notification);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
