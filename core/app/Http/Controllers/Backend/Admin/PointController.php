<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PointSetting;
use App\Model\Point;
use App\Model\User;
use App\Model\PointTransaction;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function point_setting_index()
    {
      $data['page_title'] = "Point Setting";
      $data['point_setting'] = PointSetting::first();
      return view('backend.admin.point.point_setting', $data);
    }
    public function point_setting_store(Request $r)
    {
      $r->validate([
          'volume' => 'required | numeric',
      ]);
      $store = new PointSetting;
      $store->volume = $r->volume;

      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $request->id;
      // $logs->value_past = $data->first_name.' '.$data->last_name;
      $logs->value_current = $request->volume;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'Point';
      $logs->save();

      $store->save();

      return redirect()->route('point.setting')->with('success','Save successful');
    }
    public function point_setting_update(Request $r)
    {
      $r->validate([
          'volume' => 'required | numeric',
      ]);
      $store = PointSetting::find($r->id);
      
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $r->id;
      $logs->value_past = $data->volume;
      $logs->value_current = $r->volume;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'Point';
      $logs->save();

      $store->volume = $r->volume;
      $store->save();
      return redirect()->route('point.setting')->with('success','Updated successful');
    }
    public function point_adjust_index()
    {
      $data['page_title'] = "Point Adjust";
      $data['users'] = User::get();
      return view('backend.admin.point.point_adjust', $data);
    }
    public function point_adjust_store(Request $r)
    {
      $r->validate([
          'amount' => 'required | numeric',
          'member' => 'required'
      ]);
      $user = User::find($r->member);
      $data = [
          'pointable_type' => 'ADDITION',
          'message' => $r->message
      ];
      if($r->amount < 0){
        $data = [
            'pointable_type' => 'ADDITION',
            'flags' => '-',
            'message' => $r->message
        ];
      }
      $transaction = $user->addPoints($r->amount,'',$data);
      $user = User::find($r->member);
      $user->point = $user->currentPoints();
      $user->save();

      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $r->id;
      // $logs->value_past = $data->volume;
      $logs->value_current = $r->amount;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'PointTransaction';
      $logs->save();

      return redirect()->route('point.adjust')->with('success','Adjust successful');
    }
    public function point_history_index()
    {
      $data['page_title'] = "Point Transaction";
      $data['list'] = PointTransaction::leftJoin('users','point_transactions.user_id','=','users.id')
      ->get();
      return view('backend.admin.point.point_transaction', $data);
    }
    public function point_member_index()
    {
      $data['page_title'] = "Point Member";
      $data['list'] = User::get();
      return view('backend.admin.point.point_member', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
