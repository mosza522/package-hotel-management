<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cars;
use App\Model\CarTransfer;
use App\Model\CarTransaction;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
class CarsTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function detail_index()
     {
       $data['page_title'] = "Detail Transfer";
       $data['cars_transfer'] = CarTransfer::get();
       $data['cars'] = Cars::all();
       // dd($data);
       if(count($data['cars_transfer'])>0){
         $data['cars_transfer'] = $data['cars_transfer'][0];
       }
       return view('backend.admin.transfer.index_detail', $data);
     }
     public function book_index()
     {
       $data['page_title'] = "Transfer Booking";
       $data['daterange'] = "";
       $data['cars_transaction'] = CarTransaction::leftJoin('users','cars_transaction.user_id','=','users.id')
       ->where('transaction_date','>',Carbon::now())->get();
       return view('backend.admin.transfer.index_book', $data);
     }
    public function index()
    {
      $data['page_title'] = "All Cars";
      $data['cars'] = Cars::get();
      // dd($data);
      return view('backend.admin.transfer.cars.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Add Cars';
      return view('backend.admin.transfer.cars.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'brand' => 'required',
          'model' => 'required',
          'plate' => 'required',
          'seats' => 'required',
          'luggage' => 'required',
          'color' => 'required'
        ]
      );
      $in = Input::except('_token');
      $res = Cars::create($in);
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.cars_transfer')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Creating Cars');
          return redirect()->route('admin.cars_transfer')->with($notification);
      }
    }
    public function detail_store(Request $request)
    {
      $request->validate([
          // 'car_id' => 'required',
          'van_picture' => 'required | mimes:jpeg,jpg,png | max:1000',
          'saloon_picture' => 'required | mimes:jpeg,jpg,png | max:1000',
          'van_price' => 'required | numeric',
          'saloon_price' => 'required | numeric',
          'van_description' => 'required',
          'saloon_description' => 'required'
      ]
      );

      $in = Input::except('_token');
      if($request->hasFile('picture')){
          $image = $request->file('picture');
          $filename = 'transfer_'.time().'.jpg';
          $location = 'assets/backend/image/transfer/' . $filename;
          Image::make($image)->save($location);
          $in['picture'] = $filename;
      }
      // $in['status'] =  $request->status == 'on' ? '1' : '0';
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $request->id;
      // $logs->value_past = $data->van_price.' ,'.$data->saloon_price;
      $logs->value_current = $request->van_price.' ,'.$request->saloon_price;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'CarsTransfer';
      $res = CarTransfer::create($in);
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.transfer_detail')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Creating');
          return redirect()->route('admin.transfer_detail')->with($notification);
      }
    }
    public function detail_update(Request $request)
    {
      $data = CarTransfer::find($request->id);
      $request->validate([
          // 'car_id' => 'required',
          'van_price' => 'required | numeric',
          'saloon_price' => 'required | numeric',
          'van_description' => 'required',
          'saloon_description' => 'required'
      ]
      );
      $in = Input::except('_token');
      if($request->hasFile('van_picture')){
          $file = explode('.',$request->file('van_picture')->getClientOriginalName());
          $image = $request->file('van_picture');
          $filename = 'van_'.time().$file[0].'.'.$file[1];
          $location = 'assets/backend/image/transfer/' . $filename;
          Image::make($image)->save($location);
          $path = 'assets/backend/image/transfer/';
          @unlink($path.$data->van_picture);
          $in['van_picture'] = $filename;
      }else{
        $in['van_picture'] = $data->van_picture;
      }
      if($request->hasFile('saloon_picture')){
          $file = explode('.',$request->file('saloon_picture')->getClientOriginalName());
          $image = $request->file('saloon_picture');
          $filename = 'saloon_'.time().$file[0].'.'.$file[1];
          $location = 'assets/backend/image/transfer/' . $filename;
          Image::make($image)->save($location);
          $path = 'assets/backend/image/transfer/';
          @unlink($path.$data->saloon_picture);
          $in['saloon_picture'] = $filename;
      }else{
        $in['saloon_picture'] = $data->saloon_picture;
      }
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $request->id;
      $logs->value_past = $data->van_price.' ,'.$data->saloon_price;
      $logs->value_current = $request->van_price.' ,'.$request->saloon_price;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'CarsTransfer';
      $logs->save();

      $res = $data->fill($in)->save();
      if ($res) {
          $notification = array('success' => 'Update Successfully!');
          return redirect()->route('admin.transfer_detail')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Updating');
          return redirect()->route('admin.transfer_detail')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Edit Car';
      $data['cars'] = Cars::findOrFail($id);
      // $data['category'] = BlogCategory::where('id', $this->cat_id)->whereStatus(1)->get();
      return view('backend.admin.transfer.cars.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $data = Cars::findOrFail($request->id);
      $request->validate([
          'brand' => 'required',
          'model' => 'required',
          'plate' => 'required',
          'seats' => 'required',
          'luggage' => 'required',
          'color' => 'required'
        ]
      );
      $in = Input::except('_token');
      $res = $data->fill($in)->save();
      if ($res) {
          $notification = array('success' => 'Updated Successfully!');
          return redirect()->route('admin.cars_transfer')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Updating Cars');
          return redirect()->route('admin.cars_transfer')->with($notification);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $data = Cars::findOrFail($request->id);
      $res =  $data->delete();

      if ($res) {
          $notification = array('success' => 'Cars Delete Successfully!');
          return back()->with($notification);
      } else {
          $notification = array('error' => 'Problem With Deleting Cars!');
          return back()->with($notification);
      }
    }
    public function detail($id)
    {
      $data = CarTransaction::leftJoin('users','cars_transaction.user_id','=','users.id')
      ->select('cars_transaction.*','users.phone','users.email','users.first_name','users.last_name')
      ->first();
      return response()->json($data);
    }
    public function book_sort(Request $r)
    {

      $tempDate = explode(" - ",$r->daterange);
      $start = $datetime = Carbon::createFromFormat('m/d/Y', $tempDate[0]);
      $end = $datetime = Carbon::createFromFormat('m/d/Y', $tempDate[1]);
      $data['cars_transaction'] = CarTransaction::leftJoin('users','cars_transaction.user_id','=','users.id')
      ->where('cars_transaction.transaction_date','>',$start)
      ->where('cars_transaction.transaction_date','<',$end)
      ->get();
      $data['page_title'] = 'Transfer booking';
      $data['daterange']['start'] = $tempDate[0];
      $data['daterange']['end'] = $tempDate[1];
      return view('backend.admin.transfer.index_book', $data);
    }
}
