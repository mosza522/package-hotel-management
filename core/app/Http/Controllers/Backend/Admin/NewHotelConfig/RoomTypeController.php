<?php

namespace App\Http\Controllers\Backend\Admin\NewHotelConfig;

use App\Http\Helper\MimeCheckRules;
use App\Model\Amenity;
use App\Model\RegularPrice;
use App\Model\RoomType;
use App\Model\RoomTypeImage;
use App\Model\SpecialPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Image;

class RoomTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
      * @var RoomType
      */
     private $roomType;
     /**
      * @var Amenity
      */
     private $amenity;
     /**
      * @var RoomTypeImage
      */
     private $roomTypeImage;
     /**
      * @var RegularPrice
      */
     private $regularPrice;
     /**
      * @var SpecialPrice
      */
     private $specialPrice;
     public  function __construct(RoomType $roomType,Amenity $amenity,RoomTypeImage $roomTypeImage,RegularPrice $regularPrice,SpecialPrice $specialPrice)
     {
         $this->roomType = $roomType;
         $this->amenity = $amenity;
         $this->roomTypeImage = $roomTypeImage;
         $this->regularPrice = $regularPrice;
         $this->specialPrice = $specialPrice;
     }
    public function index()
    {
      $roomTypes = $this->roomType->get();
      return view('backend.admin.hotel_config.room_type.index',compact('roomTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $amenities = $this->amenity->where('status',1)->get();
      return view('backend.admin.hotel_config.room_type.create',compact('amenities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function view($id){
         $roomType = $this->roomType->with('roomTypeImage')->findOrFail($id);
         return view('backend.admin.hotel_config.room_type.view',compact('roomType'));
     }
    public function store(Request $request)
    {
      $request['slug'] = str_slug($request->title);
      $this->validate($request,[
          'title'=>'required|max:191|unique:room_types',
          'slug'=>'unique:room_types',
          'short_code'=>'required|max:191|unique:room_types',
          'higher_capacity'=>'required|integer|min:1',
          'base_price'=>'required|numeric|min:0',
      ]);
      $in = Input::except('_token');
      $in['slug'] = str_slug($request->title);
      $res = RoomType::create($in);
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.room_type')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Creating Roomtype');
          return redirect()->route('admin.room_type')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id){
         $roomType = $this->roomType->findOrFail($id);
         return view('backend.admin.hotel_config.room_type.edit',compact('roomType'));
     }
     public function update(Request $request,$id){
       $data = RoomType::findOrFail($id);
       $request['slug'] = str_slug($request->title);
       $this->validate($request,[
         'title'=>'required|max:191|unique:room_types,title,'.$id,
         'slug'=>'unique:room_types,slug,'.$id,
         'short_code'=>'required|max:191|unique:room_types,short_code,'.$id,
         'higher_capacity'=>'required|integer|min:1',
         'base_price'=>'required|numeric|min:0',
         'amenities'=>'nullable'
       ]);
       $in = Input::except('_token');
       $in['slug'] = str_slug($request->title);
       $res = $data->fill($in)->save();
       if ($res) {
         $notification = array('success' => 'Updated Successfully!');
         return redirect()->back()->with($notification);
       } else {
         $notification = array('error' => 'Problem With Updating Roomtype');
         return redirect()->back()->with($notification);
       }
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
