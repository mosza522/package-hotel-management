<?php

namespace App\Http\Controllers\Backend\Admin\NewHotelConfig;

use App\Http\Helper\MimeCheckRules;
use App\Model\Floor;
use App\Model\Room;
use App\Model\RoomType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\RoomImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Image;
class RoomController extends Controller
{
    /**
     * @var Room
     */
    private $room;
    /**
     * @var Floor
     */
    private $floor;
    /**
     * @var RoomType
     */
    private $roomType;

    public  function __construct(Room $room,Floor $floor,RoomType $roomType)
    {
        $this->room = $room;
        $this->floor = $floor;
        $this->roomType = $roomType;
    }

    public function index(){
        $rooms = $this->room->get();
        return view('backend.admin.hotel_config.room.index',compact('rooms'));
    }
    public function create(){
        $floors = $this->floor->where('status',1)->get();
        $room_types = $this->roomType->where('status',1)->get();
        return view('backend.admin.hotel_config.room.create',compact('floors','room_types'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'room_type'=>'required|integer',
            'floor'=>'required|integer',
            'number'=>'required|integer|unique:rooms',
            'image'=>[new  MimeCheckRules(['jpg']),'max:2048','image'],
        ]);
        $room = new $this->room;
        $room->room_type_id = $request->room_type;
        $room->floor_id = $request->floor;
        $room->number = $request->number;
        if($request->hasFile('image')){
            $path = 'assets/backend/image/room/';
            $room->image = time().'.png';
            Image::make($request->image)->save($path.$room->image);
        }
        $room->status = $request->has('status')?1:0;
        $room->save();
        return redirect()->route('admin.room')->with('success','Save successful');
    }
    public function edit($id){
        $room = $this->room->findOrFail($id);
        $floors = $this->floor->where('status',1)->get();
        $room_types = $this->roomType->where('status',1)->get();
        return view('backend.admin.hotel_config.room.edit',compact('room','floors','room_types'));
    }
    public function update(Request $request,$id){
        $this->validate($request,[
            'room_type'=>'required|integer',
            'number'=>'required|integer|unique:rooms,number,'.$id,
            'image'=>[new  MimeCheckRules(['jpg']),'max:2048','image'],
        ]);

        $room = $this->room->findOrFail($id);
        $room->room_type_id = $request->room_type;
        $room->number = $request->number;
        if($request->hasFile('image')){
            $path = 'assets/backend/image/room/';
            @unlink($path.$room->image);
            $room->image = time().'.png';
            Image::make($request->image)->save($path.$room->image);
        }
        $room->status = $request->has('status')?1:0;
        $room->save();
        return redirect()->back()->with('success','Update successful');
    }
    public function delete($id){
        $this->room->findOrFail($id)->delete();
        return redirect()->back()->with('success','Delete successful');
    }
    public function import(Request $request)
    {
      $this->validate($request,[
        'file'=>'mimes:xlsx,xls',
      ],[
        'mimes' => 'Allow only xlsx,xls'
      ]);
      $rows = Excel::toArray(new RoomImport, request()->file('file'));
      // return response()->json($rows[0][0][0]);
      $room = Room::select('number')->get();
      $res = true ;
      $duplicate = '';
      // return $return[0]['number'];
      foreach ($rows[0] as $key => $value) {
        foreach ($room as $room_key => $room_value) {
          if($room_value->number == $value[0]){
            $res = false;
            return back()->with('error','Room number cannot duplicate \' room number : '.$room_value->number.'\'');
          }
        }
      }
      $duplicate = substr($duplicate,0,strlen($duplicate)-1);
      if($res){
        Excel::import(new RoomImport,request()->file('file'));
        return back()->with('success','Import Room successful');
      }else{
        return back()->with('error','Problem With Importing Room!');
      }
    }
}
