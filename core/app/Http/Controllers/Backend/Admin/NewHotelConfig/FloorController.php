<?php

namespace App\Http\Controllers\Backend\Admin\NewHotelConfig;

use App\Model\Floor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
class FloorController extends Controller
{
    /**
     * @var Floor
     */
    private $floor;

    public  function __construct(Floor $floor)
    {
        $this->floor = $floor;
    }

    public function index(){
        $floors = $this->floor->get();
        return view('backend.admin.hotel_config.floor.index',compact('floors'));
    }
    public function create(){
        return view('backend.admin.hotel_config.floor.create');
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required|max:191|unique:floors',
            'number'=>'required|integer|unique:floors',
        ]);
        $in = Input::except('_token');
        $in['status'] = $request->has('status')?1:0;
        $res = floor::create($in);
        if ($res) {
            $notification = array('success' => 'Created Successfully!');
            return redirect()->route('admin.floor')->with($notification);
        } else {
            $notification = array('error' => 'Problem With Creating Floor');
            return redirect()->route('admin.floor')->with($notification);
        }
    }
    public function edit($id){
        $floor = $this->floor->findOrFail($id);
        return view('backend.admin.hotel_config.floor.edit',compact('floor'));
    }
    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>'required|max:191|unique:floors,name,'.$id,
            'number'=>'required|integer|unique:floors,number,'.$id,
        ]);
        $floor = $this->floor->findOrFail($id);
        $in = Input::except('_token');
        $in['status'] = $request->has('status')?1:0;
        $res = $floor->fill($in)->save();
        if ($res) {
            $notification = array('success' => 'Updated Successfully!');
            return redirect()->back()->with($notification);
        } else {
            $notification = array('error' => 'Problem With Updating Floor');
            return redirect()->back()->with($notification);
        }
    }
    public function delete($id){
        $this->floor->findOrFail($id)->delete();
        return redirect()->back()->with('success','Delete successful');
    }
}
