<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\TransferDetail;
use App\Model\TransferBook;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail_index()
    {
      $data['page_title'] = "Detail Transfer";
      $data['transfer_detail'] = TransferDetail::get();
      // dd($data);
      if(count($data['transfer_detail'])>0){
        $data['transfer_detail'] = $data['transfer_detail'][0];
      }
      return view('backend.admin.transfer.index_detail', $data);
    }
    public function book_index()
    {
      $data['page_title'] = "Transfer Booking";
      $data['transfer_book'] = TransferBook::get();
      return view('backend.admin.transfer.index_book', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function detail_store(Request $request)
    {
      $request->validate([
          'title' => 'required',
          'image' => 'required | mimes:jpeg,jpg,png | max:1000',
          'price' => 'required | numeric',
          'detail' => 'required'
      ],
          [
              'title.required' => 'Transfer Title Must not be empty',
              'detail.required' => 'Transfer Detail Must not be empty'
          ]
      );

      $in = Input::except('_token');
      if($request->hasFile('image')){
          $image = $request->file('image');
          $filename = 'transfer_'.time().'.jpg';
          $location = 'assets/backend/image/transfer/' . $filename;
          Image::make($image)->resize(700,350)->save($location);
          $in['image'] = $filename;
      }
      // if($request->hasFile('image')){
      //     $image = $request->file('image');
      //     $filename = 'news_thumb'.time().'.jpg';
      //     $location = 'assets/backend/image/news/' . $filename;
      //     Image::make($image)->resize(350,213)->save($location);
      //     $in['thumb'] = $filename;
      // }


      $in['status'] =  $request->status == 'on' ? '1' : '0';
      $res = TransferDetail::create($in);
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.transfer_detail')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Creating Detail');
          return redirect()->route('admin.transfer_detail')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function detail_update(Request $request)
     {
       $data = TransferDetail::find($request->id);
       $request->validate([
           'title' => 'required',
           'price' => 'required | numeric',
           'detail' => 'required'
       ],
           [
               'title.required' => 'Transfer Title Must not be empty',
               'detail.required' => 'Transfer Detail Must not be empty'
           ]
       );

       $in = Input::except('_token');
       if($request->hasFile('image')){
         if($request->hasFile('image')){
             $image = $request->file('image');
             $filename = 'transfer_'.time().'.jpg';
             $location = 'assets/backend/image/transfer' . $filename;
             Image::make($image)->resize(700,350)->save($location);
             $path = 'assets/backend/image/transfer/';
             @unlink($path.$data->image);
             $in['image'] = $filename;
         }
       }else{
         $in['image'] = $data->image;
       }

       // if($request->hasFile('image')){
       //     $image = $request->file('image');
       //     $filename = 'news_thumb'.time().'.jpg';
       //     $location = 'assets/backend/image/news/' . $filename;
       //     Image::make($image)->resize(350,213)->save($location);
       //     $in['thumb'] = $filename;
       // }


       $in['status'] =  $request->status == 'on' ? '1' : '0';
       $res = $data->fill($in)->save();
       if ($res) {
           $notification = array('success' => 'Update Successfully!');
           return redirect()->route('admin.transfer_detail')->with($notification);
       } else {
           $notification = array('error' => 'Problem With Updating Detail');
           return redirect()->route('admin.transfer_detail')->with($notification);
       }
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function detail($id)
    {
      $data =  TransferBook::where('id',$id)->first();
      return response()->json($data);
    }
}
