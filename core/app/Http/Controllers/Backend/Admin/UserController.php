<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Register;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use Hash;
use App\Http\Helper\MimeCheckRules;
use Image;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     private $user;

     public function __construct(User $user)
     {
         $this->user = $user;
     }
    public function index_guest()
    {
      $data['page_title'] = "All Guest";
      $data['register'] = Register::get();
      // dd($data);
      return view('backend.admin.user.guest.index', $data);
      //
    }
    public function index_member()
    {
      $data['page_title'] = "All Member";
      $data['member'] = User::get();
      // dd($data);
      return view('backend.admin.user.member.index', $data);
      //
    }
    public function view_member($id)
    {
      $data['page_title'] = 'Member';
      $data['member'] = User::findOrFail($id);
      // $data['category'] = BlogCategory::where('id', $this->cat_id)->whereStatus(1)->get();
      return view('backend.admin.user.member.view', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_guest()
    {
      $data['page_title'] = "Create Guest";
      return view('backend.admin.user.guest.add', $data);
    }

    public function create_member()
    {
      $data['page_title'] = "Create Member";
      return view('backend.admin.user.member.add', $data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_guest(Request $request)
    {
      $request->validate([
          'username' => 'required | unique:register,username',
          'password' => 'required | min:6 | confirmed',
          'email' => 'required | email | unique:register,email',
          'first_name' => 'required',
          'last_name' => 'required',
        ]
      );
      $in = Input::except('_token');
      $in['password'] = Hash::make($request->password);

      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      // $logs->id_data = $request->id;
      // $logs->value_past = $data->subject;
      $logs->value_current = $request->first_name.' '.$request->last_name;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'register';
      $logs->save();
      $res = Register::create($in);
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.register')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Creating Guest');
          return redirect()->route('admin.register')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_guest($id)
    {
      $data['page_title'] = 'Edit Guest';
      $data['register'] = Register::findOrFail($id);
      // $data['category'] = BlogCategory::where('id', $this->cat_id)->whereStatus(1)->get();
      return view('backend.admin.user.guest.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_guest(Request $request)
    {
      $data = Register::findOrFail($request->id);
      $request->validate([
          'username' => 'required | unique:register,username',
          'email' => 'required | email',
          'first_name' => 'required',
          'last_name' => 'required',
        ]
      );
      $in = Input::except('_token');

      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $request->id;
      $logs->value_past = $data->first_name.' '.$data->last_name;
      $logs->value_current = $request->first_name.' '.$request->last_name;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'register';
      $logs->save();

      $res = $data->fill($in)->save();
      if ($res) {
          $notification = array('success' => 'Updated Successfully!');
          return redirect()->route('admin.register')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Updateing Guest');
          return redirect()->route('admin.register')->with($notification);
      }
    }
    public function update_member(Request $request,$id)
    {
      $this->validate($request,[
          'first_name'=>'nullable|max:191|string',
          'last_name'=>'nullable|max:191|string',
          'phone'=>'required|max:191|string',
          'email'=>'required|max:191|string',
          'address'=>'nullable|string',
          'sex'=>'required|string',
          'picture'=>['nullable',new MimeCheckRules(['png']),'max:2048','image'],
          'id_card_image'=>['nullable',new MimeCheckRules(['png','jpg']),'max:2048','image'],
          'picture_receipt'=>['nullable',new MimeCheckRules(['png','jpg']),'max:2048','image'],
          'country'=>'required',
          'state'=>'required'
      ]);
      $guests = $this->user->findOrFail($id);
      $guests->first_name = $request->first_name;
      $guests->last_name = $request->last_name;
      $guests->phone = $request->phone;
      $guests->email = $request->email;
      $guests->address = $request->address;
      $guests->sex = $request->sex;
      $guests->dob = $request->dob;
      $guests->country = $request->country;
      $guests->state = $request->state;
      $guests->postal = $request->postal;
      if($request->has('picture')){
          $path_pic = 'assets/backend/image/guest/pic/';
          @unlink($path_pic.$guests->picture);
          $guests->picture = 'pic_'.time().'.png';
          Image::make($request->picture)->save($path_pic.$guests->picture);
      }
      if($request->has('picture_receipt')){
          $path_pic = 'assets/backend/image/guest/receipt/';
          @unlink($path_pic.$guests->picture_receipt);
          $guests->picture_receipt = 'pic_'.time().'.png';
          Image::make($request->picture_receipt)->save($path_pic.$guests->picture_receipt);
      }
      $guests->id_type = $request->id_type;
      $guests->id_number = $request->id_number;
      if($request->has('id_card_image')){
          $path_card_image = 'assets/backend/image/guest/card_image/';
          @unlink($path_card_image.$guests->id_card_image);
          $guests->id_card_image = 'id_'.time().'.'.$request->id_card_image->getClientOriginalExtension();
          Image::make($request->id_card_image)->save($path_card_image.$guests->id_card_image);
      }
      $guests->remarks = $request->remarks;
      $guests->news_agreement = $request->has('news_agreement')?1:0;
      $guests->save();
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $request->id;
      $logs->value_past = $data->first_name.' '.$data->last_name;
      $logs->value_current = $request->first_name.' '.$request->last_name;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'member';
      $logs->save();

      return redirect()->back()->with('success','Member update successful');
    }
    public function store_member(Request $request){
        $this->validate($request,[
            'username'=>'required|max:191|string|unique:users',
            'first_name'=>'required|max:191|string',
            'last_name'=>'required|max:191|string',
            'phone'=>'required|max:191|string',
            'email'=>'required|max:191|string|email|unique:users,email',
            'address'=>'required|string',
            'sex'=>'required|string',
            'password'=>'required|string',
            'picture'=>['nullable',new MimeCheckRules(['png']),'max:2048','image'],
            'id_card_image'=>['nullable',new MimeCheckRules(['png','jpg']),'max:2048','image'],
            'country' => 'required',
            'picture_receipt'=>['required',new MimeCheckRules(['png','jpg']),'max:2048','image'],
            'country'=>'required',
            'state'=>'required'
        ]);
        $guests = new $this->user;
        $guests->username = $request->username;
        $guests->first_name = $request->first_name;
        $guests->last_name = $request->last_name;
        $guests->phone = $request->phone;
        $guests->email = $request->email;
        $guests->dob = $request->dob;
        $guests->address = $request->address;
        $guests->sex = $request->sex;
        $guests->country = $request->country;
        $guests->state = $request->state;
        $guests->postal = $request->postal;
        if($request->has('picture')){
            $path_pic = 'assets/backend/image/guest/pic/';
            $guests->picture = 'pic_'.time().'.png';
            Image::make($request->picture)->save($path_pic.$guests->picture);
        }
        if($request->has('picture_receipt')){
            $path_pic = 'assets/backend/image/guest/receipt/';
            $guests->picture_receipt = 'pic_'.time().'.png';
            Image::make($request->picture_receipt)->save($path_pic.$guests->picture_receipt);
        }
        $guests->password = bcrypt($request->password);
        $guests->id_type = $request->id_type;
        $guests->id_number = $request->id_number;
        if($request->has('id_card_image')){
            $path_card_image = 'assets/backend/image/guest/card_image/';
            $guests->id_card_image = 'id_'.time().'.'.$request->id_card_image->getClientOriginalExtension();
            Image::make($request->id_card_image)->save($path_card_image.$guests->id_card_image);
        }
        $guests->remarks = $request->remarks;
        // $guests->vip = $request->has('vip')?1:0;
        // $guests->status = $request->has('status')?1:0;
        $guests->news_agreement = $request->has('news_agreement')?1:0;
        $guests->user_type = 'member';
        $guests->save();
        $logs = new \App\Model\LogsHistory;
        $logs->activity = 'insert';
        // $logs->id_data = $request->id;
        // $logs->value_past = $data->first_name.' '.$data->last_name;
        $logs->value_current = $request->first_name.' '.$request->last_name;
        $logs->created_by = \Auth::user()->id;
        $logs->table = 'register';
        $logs->save();
        return redirect()->route('admin.member')->with('success','member save successful');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
