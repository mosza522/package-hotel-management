<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\News;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use File;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         $data['page_title'] = "All News";
         $data['news'] = News::get();
         // dd($data);
         return view('backend.admin.news.index', $data);

         //
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Add News';
      return view('backend.admin.news.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'news_title' => 'required',
          'picture' => 'required | mimes:jpeg,jpg,png | max:1000',
          'expire_date' => 'required'
      ],
          [
              'news_title.required' => 'News Title Must not be empty',
              'content.required' => 'News content Must not be empty'
          ]
      );

      $in = Input::except('_token');
      if($request->hasFile('picture')){
          $image = $request->file('picture');
          $filename = 'news_'.time().'.jpg';
          $location = 'assets/backend/image/news/' . $filename;
          Image::make($image)->resize(700,350)->save($location);
          // dd($filename);
          $in['picture'] = $filename;
      }

      // if($request->hasFile('image')){
      //     $image = $request->file('image');
      //     $filename = 'news_thumb'.time().'.jpg';
      //     $location = 'assets/backend/image/news/' . $filename;
      //     Image::make($image)->resize(350,213)->save($location);
      //     $in['thumb'] = $filename;
      // }


      $in['status'] =  $request->status == 'on' ? '1' : '0';
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'insert';
      $logs->value_current = $request->news_title;
      $logs->created_by = \Auth::user()->id;
      $logs->table = "News";
      $logs->save();

      $res = News::create($in);
      if ($res) {
          $notification = array('success' => 'Created Successfully!');
          return redirect()->route('admin.news')->with($notification);
      } else {
          $notification = array('error' => 'Problem With Updating News');
          return redirect()->route('admin.news')->with($notification);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Edit News';
      $data['news'] = News::findOrFail($id);
      // $data['category'] = BlogCategory::where('id', $this->cat_id)->whereStatus(1)->get();
      return view('backend.admin.news.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $data = News::find($request->id);
      $request->validate([
          'news_title' => 'required',
          'content' => 'required',
          'picture' => 'nullable | mimes:jpeg,jpg,png | max:1000',
          'expire_date' => 'required'
      ],
          [
              'news_title.required' => 'News Title Must not be empty',
              'content.required' => 'News Details  must not be empty',
          ]
      );


      $in = Input::except('_token');
      if($request->hasFile('picture')){
          $image = $request->file('picture');
          $filename = 'news_'.time().'.jpg';
          $location = 'assets/backend/image/news/' . $filename;
          Image::make($image)->resize(700,350)->save($location);
          $path = 'assets/backend/image/news/';
          @unlink($path.$data->image);
          $in['picture'] = $filename;
      }


      // if($request->hasFile('image')){
      //     $image = $request->file('image');
      //     $filename = 'post_thumb'.time().'.jpg';
      //     $location = 'assets/backend/image/news/' . $filename;
      //     Image::make($image)->resize(350,213)->save($location);
      //
      //     $path = 'assets/backend/image/news/';
      //     @unlink($path.$data->thumb);
      //     $in['thumb'] = $filename;
      // }
      $in['status'] =  $request->status == 'on' ? '1' : '0';

      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'update';
      $logs->id_data = $request->id;
      $logs->value_past = $data->news_title;
      $logs->value_current = $request->news_title;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'News';
      $logs->save();

      $res = $data->fill($in)->save();

      if ($res) {
          $notification = array('success' => 'Updated Successfully!');
          return back()->with($notification);
      } else {
          $notification = array('error' => 'Problem With Updating News!');
          return back()->with($notification);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $request->validate([
          'id' => 'required'
      ]);
      $data = News::findOrFail($request->id);
      $path = 'assets/backend/image/news/';
      @unlink($path.$data->picture);
      // @unlink($path.$data->thumb);
      $logs = new \App\Model\LogsHistory;
      $logs->activity = 'delete';
      $logs->id_data = $request->id;
      $logs->value_current = $data->news_title;
      $logs->created_by = \Auth::user()->id;
      $logs->table = 'News';
      $logs->save();

      $res =  $data->delete();

      if ($res) {
          $notification = array('success' => 'News Delete Successfully!');
          return back()->with($notification);
      } else {
          $notification = array('error' => 'Problem With Deleting News!');
          return back()->with($notification);
      }
    }
    public function detailNewsContent($id)
    {
      $data =  News::where('id',$id)->select('content')->first();
      return $data->content;
    }
}
