<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Model\Floor;
use App\Model\Reservation;
use App\Model\ReservationNight;
use App\Model\RoomType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\RoomHandle;
use App\Http\Controllers\Controller;
use App\Model\WarningLog;
use App\Mail\sendMail;
use App\Model\PackageMember;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    public function index_old(){
        $data['floor_plan'] = Floor::get();
        $data['room_type'] = RoomType::get();
        $data['date']=\request()->date?Carbon::parse(\request()->date)->format('Y-m-d'):Carbon::now()->format('Y-m-d');
        $data['current_reservation'] = Reservation::whereHas('night',function ($q){
            $q->where('date',Carbon::now()->format('Y-m-d'));
        })->whereNotIn('status',['CANCEL'])->get();

        $data['upcoming_reservation'] = Reservation::whereNotIn('status',['CANCEL'])->where('check_in','>',Carbon::now()->format('Y-m-d'))->get();
        $total_chart = $this->chartData();
        // return view('backend.admin.index',compact('data','total_chart'));
        return view('backend.admin.index',compact('data','total_chart'));
    }
    public function index()
    {
      $user = User::all();
      $userid = [];
      // $dob = "0000-".Carbon::parse($user[0]->dob)->format('m-d');
      foreach ($user as $key => $value) {
        $dob = Carbon::parse("0000-".Carbon::parse($value->dob)->format('m-d'));
        $now = Carbon::parse("0000-".Carbon::now()->format('m-d'));
        // dd($dob->diffInDays($now));
        if($dob->diffInDays($now) <= 30 && (int)$dob->format('m') > (int) $now->format('m') && $value->dob!=""){
          array_push($userid,$value->id);
        }
      }

      $data['birthday30'] = User::whereIn('id',$userid)->get();
      $user_warning=[];
      $check=false;
      $roomhandle = RoomHandle::where('status','=','reserved')
      ->where('slot_id','!=','')
      ->distinct()
      ->get(['slot_id']);
      foreach ($roomhandle as $key => $room_slot) {
        $value = RoomHandle::where('slot_id','=',$room_slot->slot_id)
        ->leftJoin('package_masters','room_handle.package_id','=','package_masters.id')
        ->first();
        //save only slot
        $warning = WarningLog::where('slot_id',$value->slot_id)->first();
        if( Carbon::parse($value->date)->diffInDays(Carbon::now()) <=60
        && Carbon::parse($value->date)>Carbon::now()
        && $warning
        ){
          if($warning->warning == "90"){
            $user = User::find($value->user_id);
            $data_mail = [
              'user'=>$user,
              'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
              'attach'=>null
            ];
            // $mail = new sendMail($data_mail);
            // Mail::to($user->email)->send($mail);
            // $warning->warning = '60';
            // $warning->save();
            $check=true;
            array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
          }
        }elseif (Carbon::parse($value->date)->diffInDays(Carbon::now()) <=90
        && Carbon::parse($value->date)->diffInDays(Carbon::now()) >60
        && Carbon::parse($value->date)>Carbon::now()
        && $warning
        ) {
          if($warning->warning == "180"){
            $user = User::find($value->user_id);
            $data_mail = [
              'user'=>$user,
              'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
              'attach'=>null
            ];
            // $mail = new sendMail($data_mail);
            // Mail::to($user->email)->send($mail);
            // $warning->warning = '90';
            // $warning->save();
            $check=true;
            array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
          }
        }elseif (Carbon::parse($value->date)->diffInDays(Carbon::now()) <=180
        && Carbon::parse($value->date)->diffInDays(Carbon::now()) >90
        && Carbon::parse($value->date)>Carbon::now()
        ) {
            $user = User::find($value->user_id);
            $data_mail = [
              'user'=>$user,
              'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
              'attach'=>null
            ];
            // $mail = new sendMail($data_mail);
            // Mail::to($user->email)->send($mail);
            // $warning= new WarningLog;
            // $warning->user_id = $value->user_id;
            // $warning->warning = '180';
            // $warning->slot_id = $value->slot_id;
            // $warning->save();
            $check=true;
            array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
        }elseif(Carbon::parse($value->date)->diffInDays(Carbon::now()) <=90
        && Carbon::parse($value->date)->diffInDays(Carbon::now()) >60
        && Carbon::parse($value->date)>Carbon::now()){
          $user = User::find($value->user_id);
          $data_mail = [
            'user'=>$user,
            'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
            'attach'=>null
          ];
          // $mail = new sendMail($data_mail);
          // Mail::to($user->email)->send($mail);
          // $warning= new WarningLog;
          // $warning->user_id = $value->user_id;
          // $warning->warning = '90';
          // $warning->slot_id = $value->slot_id;
          // $warning->save();
          $check=true;
          array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
        }elseif(Carbon::parse($value->date)->diffInDays(Carbon::now()) <=60
        && Carbon::parse($value->date)>Carbon::now()
      ){
            $user = User::find($value->user_id);
            $data_mail = [
              'user'=>$user,
              'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
              'attach'=>null
            ];
            // $mail = new sendMail($data_mail);
            // Mail::to($user->email)->send($mail);
            // $warning= new WarningLog;
            // $warning->user_id = $value->user_id;
            // $warning->warning = '60';
            // $warning->slot_id = $value->slot_id;
            // $warning->save();
            $check=true;
            array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
          }
          if(Carbon::parse($value->date)->diffInDays(Carbon::now()) <=180
          && Carbon::parse($value->date)>Carbon::now() && !$check){
            $user = User::find($value->user_id);
            array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
          }
        }

      $data['warning']=[];
      if($user_warning){
        for ($i=0; $i < count($user_warning); $i++) {
          array_push($data['warning'],array(
            'user_id'=> $user_warning[$i]['user_data']->id,
            'fullname'=> $user_warning[$i]['user_data']->first_name.' '.$user_warning[$i]['user_data']->last_name,
            'package_name'=>($user_warning[$i]['slot_data']->package_name != null)?$user_warning[$i]['slot_data']->package_name:'',
            'package_id'=>$user_warning[$i]['slot_data']->package_id,
            'diff_date'=>$user_warning[$i]['diff_date']
          ));
        }
      }
      $userAccrued = PackageMember::leftJoin('users','package_members.user_id','=','users.id')
      ->leftJoin('package_slot','package_members.slot_id','=','package_slot.id')
      ->leftJoin('package_masters','package_slot.package_id','=','package_masters.id')
      ->select('package_members.*','package_slot.start_date','package_masters.package_price','package_masters.package_name','users.first_name','users.last_name')
      ->where('package_member_status','=',0)->get();
      $data['accrued'] = [];

      foreach ($userAccrued as $key => $value) {
        // if (Carbon::parse($value->start_date)->subDays(180) > Carbon::now()) {
          array_push($data['accrued'],[
            'id'  => $value->id,
            'fullname'  =>  $value->first_name.' '.$value->last_name,
            'package_id' => $value->package_id,
            'package_name' => $value->package_name,
            'dept'  =>  $value->dept,
            'deadline'  =>  Carbon::parse($value->start_date)->subDays(180)
          ]);
        }
      // }
      return view('backend.admin.dashboard.index',$data);

    }
    public function chartData(){
        $subscribe = Reservation::whereYear('created_at', '=', date('Y'))->get()->groupBy(function($d) {
            return $d->created_at->format('F');
        });
        $monthly_chart =collect([]);
        foreach (month_arr() as $key => $value) {
            $monthly_chart->push([
                'month' => Carbon::parse(date('Y').'-'.$key)->format('Y-m'),
                'online' =>$subscribe->has($value)?$subscribe[$value]->where('online',1)->count():0,
                'offline' =>$subscribe->has($value)?$subscribe[$value]->where('online',0)->count():0,
            ]);

        }
        return response()->json($monthly_chart->toArray())->content();
    }
}
