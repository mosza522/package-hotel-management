<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;

use Auth;
use App\Model\RoomHandle;
use App\Model\User;
use App\Model\Payments;
use App\Model\RoomsKeeper;
use Auth;
use App\Model\AppRoomsHistoryLogsController;
use App\Model\PackageMaster;
use App\Model\PackageSlot;
class AppRoomsController extends Controller
{
    public function getDateBetweenDate($start_date, $end_date)
    {
        $period = CarbonPeriod::create($start_date, $end_date);

        // Iterate over the period
        foreach ($period as $date) {
            $period->date = $date->format('Y-m-d');
            // echo $period->date;
        }

        // Convert the period to an array of dates
        $dates = $period->toArray();
        // dd($dates);
        return $dates;
    }


    //Get all rooms each day.
    public function getRoomsAllDays()
    {
        $rooms = \App\Model\RoomsKeeper::all();
        return response()->json([
          'data' => $rooms
        ]);
    }

    //Periods rooms checker.
    public function periodsRoomsChecker($arr_date, $booking_need)
    {
        $bool = true;
        // echo $arr_date;
        $dates = \App\Model\RoomsKeeper::whereIn("date", $arr_date) -> get() ->toArray();
        foreach ($dates as $date) {
            if ($date['total_rooms'] < $booking_need) {
                // echo $date['date'].'\n';
                $bool = false;
            }
        }
        return $bool;
    }

    //Histories creator.
    public function saveHistories($user_id, $activity)
    {
        \App\Model\AppRoomsHistoryLogsController::create([
        'user_id' => $user_id,
        'activity' => $activity
      ]);
    }

    //Booking rooms
    public function booking(Request $_request)
    {
        // printf(Auth::user()->username);
        // printf('<br />');
        // printf(Auth::user()->type);
        // printf(\App\Model\User::where('username',Auth::user()->username)->first()->id);
        if(Auth::user()->type == "member"){;
          $user = \App\Model\User::where('username',(Auth::user()->username))->first();
          $request = $_request -> all();
        }else if(Auth::user()->type == "register"){
          return (new HttpResource(["message", "Please verify your profiles."],401));
        }else{
          $user = null;
        }
        // dd($user_id);
        if(isset($user) && $user != null){
          $user = $user -> toArray();
          // dd($user_id['id']);
          $checkin = Carbon::parse($request['checkin_date']);
          $checkout = Carbon::parse($request['checkout_date']);
          $arr_booking_date = self::getDateBetweenDate($checkin, $checkout);
          // var_dump(self::periodsRoomsChecker($arr_booking_date, $request['total_rooms']));
          // die();
          // $transaction_id = uniqid();
          //Check all dates in data that available
          if (self::periodsRoomsChecker($arr_booking_date, $request['total_rooms'])) {
              // dd($request);
              foreach ($arr_booking_date as $date) {
                $transaction_id = Payments::where('payment_id',$request['payment_id'])->first();
                // dd($transaction_id->payment_id);
                  $in_to_rooms_keeper = \App\Model\RoomsKeeper::where('date', $date)->first();
                  for ($i = 0; $i < $request['total_rooms']; $i++) {
                      // $in_room_handle = \App\Model\RoomHandle::where([
                      //   ['date',$date],
                      //   ['guest_name', null],
                      //   ['status', 'available']
                      // ])->first();
                      $in_room_handle = new \App\Model\RoomHandle;
                      $in_room_handle->date = $date;
                      $in_room_handle->user_id = (int)$user['id']; //Who's booking.
                      $in_room_handle->guest_name = $user['username']; //Who's will be guest
                      $in_room_handle->transaction_type = "booking";
                      $in_room_handle->transaction_id = $transaction_id->payment_id;
                      $in_room_handle->status = 'reserved';
                      // printf($in_room_handle);
                      // die();
                      if($transaction_id != null && isset($transaction_id)){
                        $in_room_handle ->save();
                      }else{
                        return (new HttpResource(["message" => "Payment references not found."], 404));
                      }

                      self::saveHistories(
                          $user['username'],
                          //Activities to keep in rooms_histories
                          ("[".$transaction_id->payment_id."] Booking ".$request['total_rooms']." rooms, Periods(".$checkin->format('Y-m-d')." to ".$checkout->format('Y-m-d').").")
                      );
                  }
                  $in_to_rooms_keeper->total_rooms = $in_to_rooms_keeper->total_rooms - $request['total_rooms'];
                  $in_to_rooms_keeper->save();
              }
              // $df_days = $checkin -> diffInDays($checkout);

              // rooms_histories insert
              // self::saveHistories(
              //     $request['username'],
              //     //Activities to keep in rooms_histories
              //     ("[".$transaction_id."] Booking ".$request['total_rooms']." rooms, Periods(".$checkin->format('Y-m-d')." to ".$checkout->format('Y-m-d').").")
              // );

              $msg = "Booking Success";
              // $resp_dt = [
              //   'username' => $request['username'],
              //   'msg' => $msg,
              //   // "room_id" => "",
              //   "checkin_date" => $request['checkin_date'],
              //   "checkout_date" => $request['checkout_date'],
              //   "duration" => $request['duration'],
              //   "total_guests" => $request['total_guests'],
              //   "total_rooms" => $request['total_rooms'],
              //   "amount" => "full",
              //   "total_amount" => $request['total_amount'],
              //   "ountstanding_balance" => 0,
              //   "payment_id" => "PayPal-001",
              //   "payment_status" => "successful"
              // ];
              // $response = ["data"=> $resp_dt, 'message'=>$msg];
              $response = ['message'=>$msg];
              $status_code = 200;
          } else {
              $msg = "Failed : Rooms not available.";
              // $resp_dt = [ "msg"=>$msg];
              $status_code = 406;
              $response = ["msg"=> $msg];
          }
        }else{
          // $resp_dt = "Failed. You are not memebers.";
          $status_code = 406;
          $msg = "Failed. You are not memebers.";
          $response = ["msg"=> $msg];
        };

        return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode($status_code);
    }


    public function confirmReservation(Request $_request)
    {
      // dd(Auth::user()->id);
      // $user_id = $_request->user_id;
      // dd($user_id);
      $room = $_request->room_id;
      // printf($room);
      $user = User::where('username', Auth::user()->username)->first();
      // dd($user);
      if($user){
        $room_to_confirm = RoomHandle::where('id',$room)->first();
        if($room_to_confirm->user_id = $user->id){
          $room_to_confirm->status = 'confirm';
          if($_request->guest_name){
            $activity = "User confirmation successful, Guest has been change from \"".$room_to_confirm->guest_name."\" to \"".$_request->guest_name."\".";
            $room_to_confirm->guest_name = $_request->guest_name;
          }else{
            $activity = "User confirmation successful, Guest name is \"".$room_to_confirm->guest_name."\".";
          }
          // dd($room_to_confirm);
          $room_to_confirm->save();
          $this->saveHistories(Auth::user()->username,$activity);
        }else{
          $status_code = 403;
          $msg = "Confirmation failed.";
          $response = ["msg"=> $msg];
        }
        $status_code = 200;
        $msg = "Confirmation successful.";
        $response = ["msg"=> $msg];
      }else{
        $status_code = 404;
        $msg = "Not found.";
        $response = ["msg"=> $msg];
      }

      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }
    public function cancle(Request $request)
    {
      $user= Auth::user();

      $roomCancle = RoomHandle::where('user_id',$user->id)
        ->where('package_id',null)
        ->where('slot_id',null)
        ->where('date',$request->date)
        ->first();
      if($roomCancle){
        $roomCancle->delete();
        $roomsLogs = new AppRoomsHistoryLogsController;
        $roomsLogs->user_id = $user->id;
        $roomsLogs->activity = 'Cancle room '.$request->date;
        $roomsLogs->save();

        $roomKeeper = RoomsKeeper::where('date',$request->date)->first();
        $roomKeeper->total_rooms = $roomKeeper->total_rooms+1;
        $roomKeeper->save();
        return (new HttpResource(["msg"=> "Cancle room successful."]))
                ->response('Cancle room successful.')
                ->setStatusCode(200);
      }else{
        return (new HttpResource(["msg"=> "no room"]))
                ->response('no room')
                ->setStatusCode(200);
      }



    }
    public function cancleAllPackage(Request $request)
    {
      $user= Auth::user();
      $roomCancle = RoomHandle::where('user_id',$user->id)
        ->where('package_id',$request->package_id)
        ->where('slot_id',$request->slot_id)
        ->where('status','reserved')
        ->get();
        if(count($roomCancle) > 0){
          foreach ($roomCancle as $key => $value) {
            $roomKeeper = RoomsKeeper::where('date',Carbon::parse($value->date)->format('Y-m-d'))->first();
            $roomKeeper->total_rooms = $roomKeeper->total_rooms+1;
            $roomKeeper->save();
            $value->delete();
          }

          // $packageDetail = PackageMaster::find($request->package_id);
          $packageDetail = PackageSlot::leftJoin('package_masters','package_slot.package_id','=','package_masters.id')
            ->where('package_slot.id','=',$request->slot_id)
            ->select('package_masters.*','package_slot.start_date','package_slot.end_date')
            ->first();
          // dd($packageDetail);
          $roomsLogs = new AppRoomsHistoryLogsController;
          $roomsLogs->user_id = $user->id;
          $roomsLogs->activity = 'Cancle Package['.$packageDetail->id
          .'], Periods('.Carbon::parse($packageDetail->start_date)->format('Y-m-d').' to '.Carbon::parse($packageDetail->end_date)->format('Y-m-d').').';
          $roomsLogs->save();

          // $roomKeeper = RoomsKeeper::where('date',$request->date)->first();
          // $roomKeeper->total_rooms = $roomKeeper->total_rooms+1;
          // $roomKeeper->save();
          return (new HttpResource(["msg"=> "Cancle room successful."]))
                  ->response('Cancle room successful.')
                  ->setStatusCode(200);
        }else{
          return (new HttpResource(["msg"=> "no room"]))
                  ->response('no room')
                  ->setStatusCode(200);
        }
    }

}
