<?php

namespace App\Http\Controllers\apis;

use App\Http\Helper\MimeCheckRules;
use App\Model\CodeManager;
use App\Model\GeneralSetting;
use App\Model\User;
use App\Model\AppToken;
use App\Model\Promotion;
use App\Model\PackageMaster;
use App\Model\PackagePeriod;
use App\Model\PackageMember;
use App\Model\PackageMemberDetail;
use App\Model\AvailableSlot;
use App\Model\CouponMaster;
use App\Model\AppliedCouponCode;
use App\Model\BlogCategory;
use App\Model\BlogPost;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;
use Image;
use App\Http\Controllers\apis\Route;
use App\Http\Controllers\apis\AppPaymentsController;
use App\Model\Payments;
use Auth;

class AppPackageController extends Controller
{
    private $posts_cat_id = 1; //Promotions
    public $_appId;
    public $_userId;

    public function index()
    {
        return 'AppPackageController';
    }

    public function addPackage(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        $package_code = (isset($arrayRequests['package_code']))? $arrayRequests['package_code']:"";
        $package_name = (isset($arrayRequests['package_name']))? $arrayRequests['package_name']:"";
        $package_type = (isset($arrayRequests['package_type']))? $arrayRequests['package_type']:"";
        $package_benefit = (isset($arrayRequests['package_benefit']))? $arrayRequests['package_benefit']:"";
        $package_discount_percentage = (isset($arrayRequests['package_discount_percentage']))? $arrayRequests['package_discount_percentage']:0;
        $package_discount = (isset($arrayRequests['package_discount']))? $arrayRequests['package_discount']:0;
        $package_price = (isset($arrayRequests['package_price']))? $arrayRequests['package_price']:0;
        $package_currency = (isset($arrayRequests['package_currency']))? $arrayRequests['package_currency']:"";
        $package_description = (isset($arrayRequests['package_description']))? $arrayRequests['package_description']:"";
        $package_agreement = (isset($arrayRequests['package_agreement']))? $arrayRequests['package_agreement']:"";
        $package_image = (isset($arrayRequests['package_image']))? $arrayRequests['package_image']:"";

        $chkPackages = PackageMaster::where(function ($q) use ($package_code) {
            if ($package_code != "") {
                return $q->where('package_code', $package_code);
            }
        })->get();
        $chkPackage = json_decode(json_encode($chkPackages));
        if (empty($chkPackage)) {
            if (!empty($package_image)) {
                $img = time().'.'.$package_image->getClientOriginalExtension();
                $image = Image::make($package_image->getRealPath());
                $destinationPath = storage_path('app/images/Packages/');
                $package_image->move($destinationPath, $img);

                $arrayRequests['package_image'] = $img;
            }

            $getPackages = PackageMaster::create($arrayRequests);
            $getPackage = json_decode(json_encode($getPackages));
            if (!empty($getPackage)) {
                $msg = 'Package create success';
                $status_code = 200;
            } else {
                $msg = 'Package create failed';
                $status_code = 400;
            }
        } else {
            $msg = 'Package code is exists';
            $status_code = 400;
        }

        $response = ['message' => $msg];
        // echo "<pre>";
        // print_r($arrayRequests);
        // echo "</pre>";

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode($status_code);
    }

    public function getPackage(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";

        $getPackages = PackageMaster::where('package_status', 1)->get();
        $getPackage = json_decode(json_encode($getPackages));

        // echo "<pre>";
        // print_r($getPackage);
        // echo "</pre>";
        if (!empty($getPackage)) {
            $imgUrl = url('core/storage/app/images/Packages');
            foreach ($getPackage as $pK => $pV) {
                $response[] = [
                    'appId' => $appId,
                    'userId' => $userId,
                    'packageId' => $pV->id,
                    'packageCode' => $pV->package_code,
                    'packageName' => $pV->package_name,
                    'packageType' => $pV->package_type,
                    'packageBenefit' => $pV->package_benefit,
                    'packageDiscountPercentage' => $pV->package_discount_percentage,
                    'packageDiscount' => $pV->package_discount,
                    'packagePrice' => $pV->package_price,
                    'packageCurrency' => $pV->package_currency,
                    'packageDetail' => $pV->package_description,
                    // 'packageAgreement' => $pV->package_agreement,
                    'packageImage' => [
                        'name' => $pV->package_code,
                        'description' => $pV->package_name,
                        'imgURL' => $imgUrl."/".$pV->package_image,
                    ]
                ];
            }
        }
        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function getPackageDetail(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        $packageId = (isset($arrayRequests['packageId']))? $arrayRequests['packageId']:"";

        $getPackages = PackageMaster::where('id', $packageId)->where('package_status', 1)->get();
        $getPackage = json_decode(json_encode($getPackages));
        // echo "<pre>";
        // print_r($getPackage);
        // echo "</pre>";
        if (!empty($getPackage)) {
            $imgUrl = url('core/storage/app/images/Packages');
            $response = [
                'appId' => $appId,
                'userId' => $userId,
                'packageId' => $getPackage[0]->id,
                'packageCode' => $getPackage[0]->package_code,
                'packageName' => $getPackage[0]->package_name,
                'packageType' => $getPackage[0]->package_type,
                'packageBenefit' => $getPackage[0]->package_benefit,
                'packageDiscountPercentage' => $getPackage[0]->package_discount_percentage,
                'packageDiscount' => $getPackage[0]->package_discount,
                'packagePrice' => $getPackage[0]->package_price,
                'packageCurrency' => $getPackage[0]->package_currency,
                'packageDetail' => $getPackage[0]->package_description,
                'packageAgreement' => $getPackage[0]->package_agreement,
                'packageImage' => [
                    'name' => $getPackage[0]->package_code,
                    'description' => $getPackage[0]->package_name,
                    'imgURL' => $imgUrl."/".$getPackage[0]->package_image,
                ]
            ];
        }

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function registerPackage(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $this->_appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $this->_userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        $packageId = (isset($arrayRequests['packageId']))? $arrayRequests['packageId']:"";
        $packageStartDate = (isset($arrayRequests['packageStartDate']))? $arrayRequests['packageStartDate']:date('Y-m-d');

        $getPackages = PackageMaster::where('id', $packageId)->where('package_status', 1)->get();
        $getPackage = json_decode(json_encode($getPackages));
        // echo "<pre>";
        // print_r($getPackage);
        // echo "</pre>";
        $getUserPackages = PackageMember::where('user_id', $this->_userId)->where('package_member_status', 1)->get();
        $getUserPackage = json_decode(json_encode($getUserPackages));
        // if(!empty($getPackage) && empty($getUserPackage)){
        if (!empty($getPackage)) {
            $packages = $getPackage[0];
            $packageBenefit = explode(',', $getPackage[0]->package_benefit);
            // echo "<pre>";
            // print_r($packageBenefit);
            // echo "</pre>";
            $packageDiscountPercentage = $getPackage[0]->package_discount_percentage;
            $packageDiscount = $getPackage[0]->package_discount;
            $yearCount = 1;
            $tmpYear = "";
            $yearPeriod = count($packageBenefit);
            $dateN = date('Y-m-d');
            // $dateNP = date('Y-m-d',strtotime($dateN . "+1 days"));
            $datePeriod = date_create($dateN);
            $sYearPeriod = date_format($datePeriod, 'Y-m-d 00:00:00');
            date_add($datePeriod, date_interval_create_from_date_string($yearPeriod.' years'));
            date_sub($datePeriod, date_interval_create_from_date_string('1 days'));
            $eYearPeriod = date_format($datePeriod, 'Y-m-d 23:59:59');
            // echo $sYearPeriod." : ".$eYearPeriod."<br />";
            $createCoupon = [];
            $time = time();
            $stampTime = date('His');
            foreach ($packageBenefit as $bK => $benefitNight) {
                $dateNow = $packageStartDate;
                if ($tmpYear != "") {
                    // $dateNow = $tmpYear;
                    $dateNow = date('Y-m-d', strtotime($tmpYear . "+1 days"));
                }
                $yearStep = $bK+1;
                $yearSteps = ($yearStep==1 ? $yearStep.'st' : ($yearStep==2 ? $yearStep.'nd' : ($yearStep==3 ? $yearStep."rd" : $yearStep."th")));
                $dateY = date_create($dateNow);
                $sYearDate = date_format($dateY, 'Y-m-d 00:00:00');
                date_add($dateY, date_interval_create_from_date_string('1 years'));
                date_sub($dateY, date_interval_create_from_date_string('1 days'));
                $eYearDate = date_format($dateY, 'Y-m-d 23:59:59');
                $tmpYear = $eYearDate;
                // echo $sYearDate." : ".$eYearDate."<br />";
                $tmpDate = "";
                for ($c=0;$c<$benefitNight;$c++) {
                    $dateP = $dateNow;
                    $datePackageCode = date('YmdHis', strtotime($dateNow.$stampTime. "+".$c." days"));
                    if ($tmpDate != "") {
                        // $dateNow = $tmpDate;
                        $dateP = date('Y-m-d', strtotime($tmpDate . "+1 days"));
                    }
                    // echo $datePackageCode."<br />";
                    $dateB = date_create($dateP);
                    $sDate = date_format($dateB, 'Y-m-d 00:00:00');
                    // date_add($dateB, date_interval_create_from_date_string($c.' days'));
                    // date_sub($dateB, date_interval_create_from_date_string('1 days'));
                    $eDate = date_format($dateB, 'Y-m-d 23:59:59');
                    $tmpDate = $eDate;
                    // echo $sDate." : ".$eDate."<br />";
                    $cStep = $c+1;
                    $arrayCreate = [
                        'offer_title' => $getPackage[0]->package_name." ".$yearSteps." year"." (days ".$cStep.")",
                        'description' => $getPackage[0]->package_name." ".$benefitNight." night "." ".$yearSteps." year"." (days ".$cStep.") start ".$sDate."-".$eDate,
                        'image' => $getPackage[0]->package_image,
                        'period_start_time' => $sDate,
                        'period_end_time' => $eDate,
                        'code' => $getPackage[0]->package_code."_".$benefitNight."_".$yearSteps."y"."_d".$cStep."_".$this->_userId."_".$datePackageCode,
                        'type' => "PERCENTAGE",
                        'value' => 100,
                        'min_amount' => 0,
                        'max_amount' => 500000,
                        'limit_per_user' => 0,
                        'limit_per_coupon' => 0,
                        'status' => 1,
                    ];
                    $createCoupon[] = CouponMaster::create($arrayCreate);
                }
            }
            // echo "<pre>";
            // print_r($createCoupon);
            // echo "</pre>";
            if (!empty($createCoupon)) {
                $arrayCreatePackageMember = [
                    'user_id' => $this->_userId,
                    'package_id' => $getPackage[0]->id,
                    'package_start_time' => $sYearPeriod,
                    'package_end_time' => $eYearPeriod,
                    'package_member_status' => 0,
                ];
                // echo "<pre>";
                // print_r($arrayCreatePackageMember);
                // echo "</pre>";
                $createPackageMember = PackageMember::create($arrayCreatePackageMember);
                $packageMemberId = $createPackageMember->id;
                foreach ($createCoupon as $cK => $coupons) {
                    $arrayPackageMemberDetail = [
                        'package_member_id' => $packageMemberId,
                        'coupon_id' => $coupons->id,
                        'package_coupon' => 'FREENIGHT',
                        'status' => 1,
                    ];
                    $createPackageMemberDetail[] = PackageMemberDetail::create($arrayPackageMemberDetail);
                }
                // echo "<pre>";
                // print_r($createPackageMemberDetail);
                // echo "</pre>";
                // Create Coupon % off vip
                if ($packageDiscountPercentage > 0) {
                    $arrayCreateOff = [
                        'offer_title' => $getPackage[0]->package_name." (".$packageDiscountPercentage."% off)",
                        'description' => $getPackage[0]->package_name." ".$packageDiscountPercentage."% off start ".$sYearPeriod."-".$eYearPeriod,
                        'image' => $getPackage[0]->package_image,
                        'period_start_time' => $sYearPeriod,
                        'period_end_time' => $eYearPeriod,
                        'code' => $getPackage[0]->package_code."_".$packageDiscountPercentage."_".$this->_userId."_".$time,
                        'type' => "PERCENTAGE",
                        'value' => $packageDiscountPercentage,
                        'min_amount' => 0,
                        'max_amount' => 500000,
                        'limit_per_user' => 0,
                        'limit_per_coupon' => 0,
                        'status' => 1,
                    ];
                    $createCouponOff = CouponMaster::create($arrayCreateOff);

                    $arrayPackageMemberOff = [
                        'package_member_id' => $createPackageMember->id,
                        'coupon_id' => $createCouponOff->id,
                        'package_coupon' => 'ADDITIONAL',
                        'status' => 1,
                    ];
                    $createPackageMemberDetailOff = PackageMemberDetail::create($arrayPackageMemberOff);
                }
                if ($packageDiscount > 0) {
                    $arrayCreateDiscount = [
                        'offer_title' => $getPackage[0]->package_name." (".$packageDiscount.$getPackage[0]->package_currency.")",
                        'description' => $getPackage[0]->package_name." ".$packageDiscount.$getPackage[0]->package_currency." start ".$sYearPeriod."-".$eYearPeriod,
                        'image' => $getPackage[0]->package_image,
                        'period_start_time' => $sYearPeriod,
                        'period_end_time' => $eYearPeriod,
                        'code' => $getPackage[0]->package_code."_".$packageDiscount."_".$this->_userId."_".$time,
                        'type' => "FIXED",
                        'value' => $packageDiscount,
                        'min_amount' => 0,
                        'max_amount' => 500000,
                        'limit_per_user' => 0,
                        'limit_per_coupon' => 0,
                        'status' => 1,
                    ];

                    $createCouponDiscount = CouponMaster::create($arrayCreateDiscount);

                    $arrayPackageMemberDiscount = [
                        'package_member_id' => $createPackageMember->id,
                        'coupon_id' => $createCouponDiscount->id,
                        'package_coupon' => 'ADDITIONAL',
                        'status' => 1,
                    ];
                    $createPackageMemberDetailDiscount = PackageMemberDetail::create($arrayPackageMemberDiscount);
                }

                // Create Package Period By coupon id
                $memberPackage = "";
                if (!empty($createPackageMemberDetail)) {
                    foreach ($createPackageMemberDetail as $vC) {
                        $couponMembers[] = $vC->id;
                    }
                    $memberPackage = implode(',', $couponMembers);
                    if (!empty($createPackageMemberDetailOff)) {
                        $memberPackage .= ','.$createPackageMemberDetailOff->id;
                    }
                    if (!empty($createPackageMemberDetailDiscount)) {
                        $memberPackage .= ','.$createPackageMemberDetailDiscount->id;
                    }
                    //update Package member
                    $updatePackageMember = PackageMember::where('id', $packageMemberId)->update(['package_member' => $memberPackage]);
                }
            }

            if (!empty($updatePackageMember)) {
                // Update VIP after payment success
                // $updateVIP = User::where('id', $userId)->update(['vip' => 1]);
            }

            // After Register Package send payment to Mobile
            // $response = [
            //     'appId' => $appId,
            //     'userId' => $userId,
            //     'packageId' => $getPackage[0]->id,
            //     'packageCode' => $getPackage[0]->package_code,
            //     'packageName' => $getPackage[0]->package_name,
            //     'packageType' => $getPackage[0]->package_type,
            //     'packageBenefit' => $getPackage[0]->package_benefit,
            //     'packagePrice' => $getPackage[0]->package_price,
            //     'packageCurrency' => $getPackage[0]->package_currency,
            //     'packageDetail' => $getPackage[0]->package_description,
            //     'packageAgreement' => $getPackage[0]->package_agreement,
            //     'packageImage' => [
            //         'name' => $getPackage[0]->package_code,
            //         'description' => $getPackage[0]->package_name,
            //         'imgURL' => 'http://www.freeimageslive.com/galleries/nature/abstract/pics/snow_surface.jpg',
            //     ]
            // ];
        } else {
            if (!empty($getUserPackage)) {
                $msg = "Package already";
                $response = ['message' => $msg];
                return (new HttpResource($response))
                    ->response($msg)
                    ->setStatusCode(400);
            }
        }

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function registerPackageNew(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $this->_appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $this->_userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        $packageId = (isset($arrayRequests['packageId']))? $arrayRequests['packageId']:"";
        $slotCode = (isset($arrayRequests['slotCode']))? $arrayRequests['slotCode']:"";
        $slotWeek = (isset($arrayRequests['slotWeek']))? $arrayRequests['slotWeek']:"";
        $packageStartDate = (isset($arrayRequests['packageStartDate']))? $arrayRequests['packageStartDate']:date('Y-m-d');

        $getPackages = PackageMaster::where('id', $packageId)->where('package_status', 1)->get();
        $getPackage = json_decode(json_encode($getPackages));
        $getSlots = AvailableSlot::where('slot_code', $slotCode)->where('slot_status', 1)->get();
        $getSlot = json_decode(json_encode($getSlots));
        // echo "<pre>";
        // print_r($getSlot);
        // echo "</pre>";
        $chkSlotUsage = 0;
        // $getUserPackages = PackageMember::where('user_id', $userId)->where('package_member_status', 1)->get();
        // $getUserPackage = json_decode(json_encode($getUserPackages));
        // if(!empty($getPackage) && empty($getUserPackage)){
        if (!empty($getPackage) && !empty($getSlot)) {
            $chkSlotUsage = PackageMemberDetail::where('slot_id', $getSlot[0]->id)->count();
            if ($chkSlotUsage>0) {
                $msg = "Package already in use";
                $response = ['message' => $msg];
                return (new HttpResource($response))
                    ->response($msg)
                    ->setStatusCode(400);
            }
            $packages = $getPackage[0];
            $packageBenefit = explode(',', $getPackage[0]->package_benefit);
            $packageDiscountPercentage = $getPackage[0]->package_discount_percentage;
            $packageDiscount = $getPackage[0]->package_discount;
            // echo "<pre>";
            // print_r($packageBenefit);
            // echo "</pre>";
            $yearCount = 1;
            $tmpYear = "";
            $yearPeriod = count($packageBenefit);
            $slotY = $getSlot[0]->slot_year;
            $slotYears = AvailableSlot::where('slot_week', '=', $slotWeek)->where('slot_year', '>=', $slotY)->select('*')->orderBy('slot_year', 'ASC')->limit($yearPeriod)->get();
            $slotYear = json_decode(json_encode($slotYears));
            // echo "<pre>";
            // print_r($slotYear);
            // echo "</pre>";

            $dateN = date('Y-m-d');
            // $dateNP = date('Y-m-d',strtotime($dateN . "+1 days"));
            $datePeriod = date_create($dateN);
            $sYearPeriod = date_format($datePeriod, 'Y-m-d 00:00:00');
            date_add($datePeriod, date_interval_create_from_date_string($yearPeriod.' years'));
            date_sub($datePeriod, date_interval_create_from_date_string('1 days'));
            $eYearPeriod = date_format($datePeriod, 'Y-m-d 23:59:59');
            // echo $sYearPeriod." : ".$eYearPeriod."<br />";
            $createCoupon = [];
            $slotMember = [];
            $stampTime = time();
            $time = time();
            $stampTime = date('His');
            foreach ($slotYear as $slotK => $slotV) {
                $packageStartDate = $slotV->slot_start_date;
                $dateNow = date('Y-m-d', strtotime($packageStartDate));

                $yearStep = $slotK+1;
                $yearSteps = ($yearStep==1 ? $yearStep.'st' : ($yearStep==2 ? $yearStep.'nd' : ($yearStep==3 ? $yearStep."rd" : $yearStep."th")));
                $dateY = date_create($dateNow);
                $sYearDate = date_format($dateY, 'Y-m-d 00:00:00');
                date_add($dateY, date_interval_create_from_date_string('1 years'));
                date_sub($dateY, date_interval_create_from_date_string('1 days'));
                $eYearDate = date_format($dateY, 'Y-m-d 23:59:59');
                $tmpYear = $eYearDate;
                // echo $sYearDate." : ".$eYearDate."<br />";
                $tmpDate = "";
                $benefitNight = $packageBenefit[$slotK];
                for ($c=0;$c<$benefitNight;$c++) {
                    $dateP = $dateNow;
                    $datePackageCode = date('YmdHis', strtotime($dateNow.$stampTime. "+".$c." days"));
                    if ($tmpDate != "") {
                        // $dateNow = $tmpDate;
                        $dateP = date('Y-m-d', strtotime($tmpDate . "+1 days"));
                    }
                    // echo $datePackageCode."<br />";
                    $dateB = date_create($dateP);
                    $sDate = date_format($dateB, 'Y-m-d 00:00:00');
                    // date_add($dateB, date_interval_create_from_date_string($c.' days'));
                    // date_sub($dateB, date_interval_create_from_date_string('1 days'));
                    $eDate = date_format($dateB, 'Y-m-d 23:59:59');
                    $tmpDate = $eDate;
                    // echo $sDate." : ".$eDate."<br />";
                    $cStep = $c+1;
                    $arrayCreate = [
                        'offer_title' => $getPackage[0]->package_name." ".$yearSteps." year"." (days ".$cStep.")",
                        'description' => $getPackage[0]->package_name." ".$benefitNight." night "." ".$yearSteps." year"." (days ".$cStep.") start ".$sDate."-".$eDate,
                        'image' => $getPackage[0]->package_image,
                        'period_start_time' => $sDate,
                        'period_end_time' => $eDate,
                        'code' => $getPackage[0]->package_code."_".$benefitNight."_".$yearSteps."y"."_d".$cStep."_".$this->_userId."_".$datePackageCode,
                        'type' => "PERCENTAGE",
                        'value' => 100,
                        'min_amount' => 0,
                        'max_amount' => 500000,
                        'limit_per_user' => 0,
                        'limit_per_coupon' => 0,
                        'status' => 1,
                    ];
                    $createCoupon[] = CouponMaster::create($arrayCreate);
                    // $createCoupon[] = $arrayCreate;
                    $slotMember[] = $slotV->id;
                }
            }
            // echo "<pre>";
            // print_r($createCoupon);
            // echo "</pre>";
            if (!empty($createCoupon)) {
                $arrayCreatePackageMember = [
                    'user_id' => $this->_userId,
                    'package_id' => $getPackage[0]->id,
                    'package_start_time' => $sYearPeriod,
                    'package_end_time' => $eYearPeriod,
                    'package_member_status' => 0,
                ];
                $createPackageMember = PackageMember::create($arrayCreatePackageMember);
                // $createPackageMember = $arrayCreatePackageMember;
                // echo "<pre>";
                // print_r($createPackageMember);
                // echo "</pre>";
                $packageMemberId = $createPackageMember->id;
                // $packageMemberId = $getPackage[0]->id;
                foreach ($createCoupon as $cK => $coupons) {
                    $arrayPackageMemberDetail = [
                        'package_member_id' => $packageMemberId,
                        'coupon_id' => $coupons->id,
                        'slot_id' => $slotMember[$cK],
                        'package_coupon' => 'FREENIGHT',
                        'status' => 1,
                    ];
                    $createPackageMemberDetail[] = PackageMemberDetail::create($arrayPackageMemberDetail);
                    // $createPackageMemberDetail[] = $arrayPackageMemberDetail;
                }
                // echo "<pre>";
                // print_r($createPackageMemberDetail);
                // echo "</pre>";
                // Create Coupon % off vip
                if ($packageDiscountPercentage > 0) {
                    $arrayCreateOff = [
                        'offer_title' => $getPackage[0]->package_name." (".$packageDiscountPercentage."% off)",
                        'description' => $getPackage[0]->package_name." ".$packageDiscountPercentage."% off start ".$sYearPeriod."-".$eYearPeriod,
                        'image' => $getPackage[0]->package_image,
                        'period_start_time' => $sYearPeriod,
                        'period_end_time' => $eYearPeriod,
                        'code' => $getPackage[0]->package_code."_".$packageDiscountPercentage."_".$this->_userId."_".$time,
                        'type' => "PERCENTAGE",
                        'value' => $packageDiscountPercentage,
                        'min_amount' => 0,
                        'max_amount' => 500000,
                        'limit_per_user' => 0,
                        'limit_per_coupon' => 0,
                        'status' => 1,
                    ];
                    $createCouponOff = CouponMaster::create($arrayCreateOff);

                    $arrayPackageMemberOff = [
                        'package_member_id' => $createPackageMember->id,
                        'coupon_id' => $createCouponOff->id,
                        'package_coupon' => 'ADDITIONAL',
                        'status' => 1,
                    ];
                    $createPackageMemberDetailOff = PackageMemberDetail::create($arrayPackageMemberOff);
                }
                if ($packageDiscount > 0) {
                    $arrayCreateDiscount = [
                        'offer_title' => $getPackage[0]->package_name." (".$packageDiscount.$getPackage[0]->package_currency.")",
                        'description' => $getPackage[0]->package_name." ".$packageDiscount.$getPackage[0]->package_currency." start ".$sYearPeriod."-".$eYearPeriod,
                        'image' => $getPackage[0]->package_image,
                        'period_start_time' => $sYearPeriod,
                        'period_end_time' => $eYearPeriod,
                        'code' => $getPackage[0]->package_code."_".$packageDiscount."_".$this->_userId."_".$time,
                        'type' => "FIXED",
                        'value' => $packageDiscount,
                        'min_amount' => 0,
                        'max_amount' => 500000,
                        'limit_per_user' => 0,
                        'limit_per_coupon' => 0,
                        'status' => 1,
                    ];

                    $createCouponDiscount = CouponMaster::create($arrayCreateDiscount);

                    $arrayPackageMemberDiscount = [
                        'package_member_id' => $createPackageMember->id,
                        'coupon_id' => $createCouponDiscount->id,
                        'package_coupon' => 'ADDITIONAL',
                        'status' => 1,
                    ];
                    $createPackageMemberDetailDiscount = PackageMemberDetail::create($arrayPackageMemberDiscount);
                }

                // Create Package Period By coupon id
                $memberPackage = "";
                if (!empty($createPackageMemberDetail)) {
                    foreach ($createPackageMemberDetail as $vC) {
                        $couponMembers[] = $vC->id;
                    }
                    $memberPackage = implode(',', $couponMembers);
                    if (!empty($createPackageMemberDetailOff)) {
                        $memberPackage .= ','.$createPackageMemberDetailOff->id;
                    }
                    if (!empty($createPackageMemberDetailDiscount)) {
                        $memberPackage .= ','.$createPackageMemberDetailDiscount->id;
                    }
                    //update Package member
                    $updatePackageMember = PackageMember::where('id', $packageMemberId)->update(['package_member' => $memberPackage]);
                }
            }
        } else {
            if (!empty($getUserPackage)) {
                $msg = "Package already";
                $response = ['message' => $msg];
                return (new HttpResource($response))
                    ->response($msg)
                    ->setStatusCode(400);
            }
        }

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function getPayment(Request $request)
    {
        $response = [];
        $arrayRequests = $request->all();
        if (!empty($arrayRequests)) {
            // Update VIP after payment success
            // $updateVIP = User::where('id', $userId)->update(['vip' => 1]);
        }
        return new HttpResource($response);
    }

    public function getMyPackage(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        // $packageId = (isset($arrayRequests['packageId']))? $arrayRequests['packageId']:"";
        $dateNow = date('Y-m-d H:i:s');
        // $dateNP = date('Y-m-d H:i:s',strtotime($dateNow . "-1 days"));
        // echo $dateNow;
        $chkUserPackages = PackageMember::query()
        ->leftJoin('package_masters', 'package_members.package_id', '=', 'package_masters.id')
        ->where('package_members.user_id', $userId)
        ->where('package_members.package_start_time', '<=', $dateNow)
        ->where('package_members.package_end_time', '>=', $dateNow)
        ->where('package_member_status', '1')
        ->select(
            'package_members.id as package_member_id',
            'package_members.user_id',
            'package_members.package_id',
            'package_members.package_member',
            'package_members.package_start_time',
            'package_members.package_end_time',
            'package_members.package_member_status',
            'package_masters.id',
            'package_masters.package_code',
            'package_masters.package_name',
            'package_masters.package_type',
            'package_masters.package_benefit',
            'package_masters.package_discount_percentage',
            'package_masters.package_discount',
            'package_masters.package_price',
            'package_masters.package_currency',
            'package_masters.package_status',
            'package_masters.package_description',
            'package_masters.package_agreement',
            'package_masters.package_image'
        )
        ->get();
        $chkUserPackage = json_decode(json_encode($chkUserPackages));
        // echo "<pre>";
        // print_r($chkUserPackage);
        // echo "</pre>";

        if (!empty($chkUserPackage)) {
            foreach ($chkUserPackage as $uPackK => $uPackV) {
                $packageMember = explode(',', $uPackV->package_member);
                $getPackageMemberDetail = [];
                if (count($packageMember) > 0) {
                    foreach ($packageMember as $pMemberId) {
                        $getPackageMembers = PackageMemberDetail::query()
                        ->leftJoin('coupon_masters', 'package_member_details.coupon_id', '=', 'coupon_masters.id')
                        ->where('package_member_details.id', $pMemberId)->where('package_member_details.slot_id', '!=', '0')
                        // ->where('period_end_time', '>=', $dateNow)
                        ->get();
                        $pTmp = json_decode(json_encode($getPackageMembers));
                        if (!empty($pTmp)) {
                            $getPackageMember[] = $pTmp[0];
                        }
                    }
                }
                $getPackageMemberAdditionals = PackageMemberDetail::query()
                ->leftJoin('coupon_masters', 'package_member_details.coupon_id', '=', 'coupon_masters.id')
                ->where('package_member_details.package_member_id', $uPackV->package_member_id)
                ->where('package_member_details.slot_id', '0')
                ->where('package_member_details.package_coupon', 'ADDITIONAL')
                ->get();
                $getPackageMemberAdditional = json_decode(json_encode($getPackageMemberAdditionals));
                // echo "<pre>";
                // print_r($getPackageMember);
                // echo "</pre>";
                // echo "dddd";
                // echo "<pre>";
                // print_r($getPackageMemberAdditional);
                // echo "</pre>";
                $imgUrl = url('core/storage/app/images/Packages');
                $response = [
                    'appId' => $appId,
                    'userId' => $userId,
                    'packageId' => $uPackV->id,
                    'packageCode' => $uPackV->package_code,
                    'packageName' => $uPackV->package_name,
                    'packageType' => $uPackV->package_type,
                    'packageBenefit' => $uPackV->package_benefit,
                    'packagePrice' => $uPackV->package_price,
                    'packageCurrency' => $uPackV->package_currency,
                    'packageDetail' => $uPackV->package_description,
                    'packageAgreement' => $uPackV->package_agreement,
                    'packageImage' => [
                        'name' => $uPackV->package_code,
                        'description' => $uPackV->package_name,
                        'imgURL' => $imgUrl."/".$uPackV->package_image,
                    ],
                    'packageMember' => $getPackageMember,
                    'packageAdditionalMember' => $getPackageMemberAdditional[0],
                ];
            }
        }
        // $response = [
        //     'userId' => $userId,
        //     'appId' => $appId,
        //     'packageId' => '1',
        //     'packageName' => 'p1',
        //     'packageDescription' => 'package Description 1',
        //     'packagePrice' => 1,
        //     'packageCurrency' => 'USD',
        //     'packageStart' => '2020-02-01',
        //     'packageEnd' => '2025-02-01',
        //     'packageCode' => 'exclusive',
        //     'packageType' => 'vip',
        //     'packageImage' => [
        //         'name' => 'img1',
        //         'description' => 'image 1',
        //         'imgURL' => 'http://www.freeimageslive.com/galleries/nature/abstract/pics/snow_surface.jpg',
        //     ]
        // ];

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function getMyAvailablePackage(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        // $packageId = (isset($arrayRequests['packageId']))? $arrayRequests['packageId']:"";
        $dateNow = date('Y-m-d H:i:s');
        // $dateNP = date('Y-m-d H:i:s',strtotime($dateNow . "-1 days"));
        // echo $dateNow;
        $chkUserPackages = PackagePeriod::query()
        ->leftJoin('package_masters', 'package_periods.package_id', '=', 'package_masters.id')
        ->where('user_id', $userId)
        ->where('package_period_start_time', '<=', $dateNow)
        ->where('package_period_end_time', '>=', $dateNow)
        ->where('status', '1')
        ->get();
        $chkUserPackage = json_decode(json_encode($chkUserPackages));
        // echo "<pre>";
        // print_r($chkUserPackage);
        // echo "</pre>";

        if (!empty($chkUserPackage)) {
            $packageCoupon = explode(',', $chkUserPackage[0]->package_coupon);
            $getPackagePeriod = [];
            if (count($packageCoupon) > 0) {
                foreach ($packageCoupon as $couponId) {
                    $chkUsagePackages = AppliedCouponCode::where('coupon_id', $couponId)
                    ->where('user_id', $userId)
                    ->get();
                    $chkUsagePackage = json_decode(json_encode($chkUsagePackages));
                    if (empty($chkUsagePackage)) {
                        $getPackagePeriods = CouponMaster::where('id', $couponId)
                        ->where('period_end_time', '>=', $dateNow)
                        ->where('period_start_time', '<=', $dateNow)
                        ->get();
                        $pTmp = json_decode(json_encode($getPackagePeriods));
                        if (!empty($pTmp)) {
                            $getPackagePeriod = $pTmp[0]->offer_title;
                        }
                    }
                }
            }
            $getPackagePeriodAdditionals = CouponMaster::where('id', $chkUserPackage[0]->package_additional_coupon)->get();
            $getPackagePeriodAdditional = json_decode(json_encode($getPackagePeriodAdditionals));
            switch ($getPackagePeriodAdditional[0]->type) {
                case 'PERCENTAGE':
                    $txtUnit = "%";
                break;
                default:
                    $txtUnit = "";
            }
            // echo "<pre>";
            // print_r($getPackagePeriod);
            // echo "</pre>";
            // echo "<pre>";
            // print_r($getPackagePeriodAdditional);
            // echo "</pre>";
            $imgUrl = url('core/storage/app/images/Packages');
            $response = [
                'appId' => $appId,
                'userId' => $userId,
                'packageId' => $chkUserPackage[0]->id,
                'packageCode' => $chkUserPackage[0]->package_code,
                'packageName' => $chkUserPackage[0]->package_name,
                'packageCoupon' => $getPackagePeriod,
                'packageAdditionalCoupon' => number_format($getPackagePeriodAdditional[0]->value)." ".$txtUnit,
            ];
        }

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function getMyAvailablePackageDetail(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        // $packageId = (isset($arrayRequests['packageId']))? $arrayRequests['packageId']:"";
        $dateNow = date('Y-m-d H:i:s');
        // $dateNP = date('Y-m-d H:i:s',strtotime($dateNow . "-1 days"));
        // echo $dateNow;
        $chkUserPackages = PackagePeriod::query()
        ->leftJoin('package_masters', 'package_periods.package_id', '=', 'package_masters.id')
        ->where('user_id', $userId)
        ->where('package_period_start_time', '<=', $dateNow)
        ->where('package_period_end_time', '>=', $dateNow)
        ->where('status', '1')
        ->get();
        $chkUserPackage = json_decode(json_encode($chkUserPackages));
        // echo "<pre>";
        // print_r($chkUserPackage);
        // echo "</pre>";

        if (!empty($chkUserPackage)) {
            $packageCoupon = explode(',', $chkUserPackage[0]->package_coupon);
            $getPackagePeriod = [];
            if (count($packageCoupon) > 0) {
                foreach ($packageCoupon as $couponId) {
                    $chkUsagePackages = AppliedCouponCode::where('coupon_id', $couponId)
                    ->where('user_id', $userId)
                    ->get();
                    $chkUsagePackage = json_decode(json_encode($chkUsagePackages));
                    if (empty($chkUsagePackage)) {
                        $getPackagePeriods = CouponMaster::where('id', $couponId)
                        ->where('period_end_time', '>=', $dateNow)
                        ->get();
                        $pTmp = json_decode(json_encode($getPackagePeriods));
                        if (!empty($pTmp)) {
                            $getPackagePeriod[] = $pTmp[0];
                        }
                    }
                }
            }
            $getPackagePeriodAdditionals = CouponMaster::where('id', $chkUserPackage[0]->package_additional_coupon)->get();
            $getPackagePeriodAdditional = json_decode(json_encode($getPackagePeriodAdditionals));
            // echo "<pre>";
            // print_r($getPackagePeriod);
            // echo "</pre>";
            // echo "<pre>";
            // print_r($getPackagePeriodAdditional);
            // echo "</pre>";
            $imgUrl = url('core/storage/app/images/Packages');
            $response = [
                'appId' => $appId,
                'userId' => $userId,
                'packageId' => $chkUserPackage[0]->id,
                'packageCode' => $chkUserPackage[0]->package_code,
                'packageName' => $chkUserPackage[0]->package_name,
                'packageType' => $chkUserPackage[0]->package_type,
                'packageBenefit' => $chkUserPackage[0]->package_benefit,
                'packagePrice' => $chkUserPackage[0]->package_price,
                'packageCurrency' => $chkUserPackage[0]->package_currency,
                'packageDetail' => $chkUserPackage[0]->package_description,
                'packageAgreement' => $chkUserPackage[0]->package_agreement,
                'packageImage' => [
                    'name' => $chkUserPackage[0]->package_code,
                    'description' => $chkUserPackage[0]->package_name,
                    'imgURL' => $imgUrl."/".$chkUserPackage[0]->package_image,
                ],
                'packageCoupon' => $getPackagePeriod,
                'packageAdditionalCoupon' => $getPackagePeriodAdditional[0],
            ];
        }

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function getPromotion(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";

        $getPromotions = BlogPost::where('cat_id', $this->posts_cat_id)->latest()->get();
        // echo "<pre>";
        // print_r($getPromotions);
        // echo "</pre>";
        foreach ($getPromotions as $pK => $promotions) {
            $response[] = [
                'promotionId' => $promotions['id'],
                'promotionName' => $promotions['title'],
                'promotionDetail' => $promotions['details'],
                'promotionAgreements' => $promotions['agreements'],
                'promotionThumbnail' => [
                    'name' => $promotions['title'],
                    'description' => $promotions['title'],
                    'imgURL' => asset('assets/backend/image/blog/post/'.$promotions['thumb']),
                ]
            ];
        }

        // $response = [
        //     [
        //         'promotionId' => '1',
        //         'promotionName' => 'p1',
        //         'promotionDetail' => 'promotion detail 1',
        //         'promotionImage' => [
        //             'name' => 'img1',
        //             'description' => 'image 1',
        //             'imgURL' => 'http://www.freeimageslive.com/galleries/nature/abstract/pics/snow_surface.jpg',
        //         ]
        //     ],
        //     [
        //         'promotionId' => '2',
        //         'promotionName' => 'p2',
        //         'promotionDetail' => 'promotion detail 2',
        //         'promotionImage' => [
        //             'name' => 'img2',
        //             'description' => 'image 2',
        //             'imgURL' => 'http://www.freeimageslive.com/galleries/nature/abstract/pics/winter_grass.jpg',
        //         ]
        //     ],
        // ];

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }


    public function getPromotionDetail(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $userId = (isset($arrayRequests['userId']))? $arrayRequests['userId']:"";
        $promotionsId = (isset($arrayRequests['promotionsId']))? $arrayRequests['promotionsId']:"";

        $getPromotions = BlogPost::where('id', $promotionsId)->get();
        // echo "<pre>";
        // print_r($getPromotions);
        // echo "</pre>";
        $promotions = json_decode(json_encode($getPromotions));
        $response[] = [
            'promotionId' => $promotions[0]->id,
            'promotionName' => $promotions[0]->title,
            'promotionDetail' => $promotions[0]->details,
            'promotionAgreements' => $promotions[0]->agreements,
            'promotionThumbnail' => [
                'name' => $promotions[0]->title,
                'description' => $promotions[0]->title,
                'imgURL' => asset('assets/backend/image/blog/post/'.$promotions[0]->thumb),
            ],
            'promotionImage' => [
                'name' => $promotions[0]->title,
                'description' => $promotions[0]->title,
                'imgURL' => asset('assets/backend/image/blog/post/'.$promotions[0]->image),
            ]
        ];

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }


    public function someapp()
    {
        $getPackages = PackageMaster::where('package_status', 1)->get();
        $getPackage = json_decode(json_encode($getPackages));
        // echo "<pre>";
        // print_r($getPackage);
        // echo "</pre>";

        if (!empty($getPackage)) {
            $imgUrl = url('core/storage/app/images/Packages');
            foreach ($getPackage as $pK => $pV) {
                $response[] = [
                  'appId' => "test",
                  'userId' => "1",
                  'packageId' => $pV->id,
                  'packageCode' => $pV->package_code,
                  'packageName' => $pV->package_name,
                  'packageType' => $pV->package_type,
                  'packageBenefit' => $pV->package_benefit,
                  'packageDiscountPercentage' => $pV->package_discount_percentage,
                  'packageDiscount' => $pV->package_discount,
                  'packagePrice' => $pV->package_price,
                  'packageCurrency' => $pV->package_currency,
                  'packageDetail' => $pV->package_description,
                  // 'packageAgreement' => $pV->package_agreement,
                  'packageImage' => [
                      'name' => $pV->package_code,
                      'description' => $pV->package_name,
                      'imgURL' => $imgUrl."/".$pV->package_image,
                  ]
              ];
            }
        }

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        // return new HttpResource($response);
        return $response;
    }


    //=============== API Additional section 17 Jan 2020 ===============//
    //Periods rooms checker. Check availbale rooms in TBL::rooms_keeper
    public function periodsRoomsChecker($arr_date, $booking_need)
    {
        $bool = true;
        // echo $arr_date;
        $dates = \App\Model\RoomsKeeper::whereIn("date", $arr_date) -> get() ->toArray();
        // dd($dates);
        foreach ($dates as $date) {
            if ($date['total_rooms'] < $booking_need) {
                $bool = false;
            }
        }
        // dd($bool);
        return $bool;
    }


    //Get date between date to array. Get date between start_date to end_date.
    public function getDateBetweenDate($start_date, $end_date)
    {
        $period = CarbonPeriod::create($start_date, $end_date);

        // Iterate over the period
        foreach ($period as $date) {
            $period->date = $date->format('Y-m-d');
            // echo $period->date;
        }

        // Convert the period to an array of dates
        $dates = $period->toArray();
        return $dates;
    }


    //Histories writer.
    public function saveHistories($user_id, $activity)
    {
        \App\Model\AppRoomsHistoryLogsController::create([
        'user_id' => $user_id,
        'activity' => $activity
      ]);
    }


    // Room Booking
    public function bookingRoomsBySlot($request)
    {
      // dd($request);
      // die();
        // dd($request['total_rooms']);
        // $arr_request = $request -> all();
        $checkin = Carbon::parse($request['start_date']);
        $checkout = Carbon::parse($request['end_date']);
        $arr_booking_date = self::getDateBetweenDate($checkin, $checkout);
        //Check all dates in data that available
        if (self::periodsRoomsChecker($arr_booking_date, $request['total_rooms'])) {
            // dd($request);
            // $transaction_id = uniqid();

            foreach ($arr_booking_date as $date) {
                $in_to_rooms_keeper = \App\Model\RoomsKeeper::where('date', $date)->first();
                // dd($request['total_rooms']);
                for ($i = 0; $i < $request['total_rooms']; $i++) {
                    // $in_room_handle = \App\Model\RoomHandle::where([
                    //   ['guest_name', null],
                    //   ['date', $date],
                    //   ['status', 'available']
                    // ])->first();
                    // $in_room_handle->user_id = (int)$request['user_id'];
                    // $in_room_handle->guest_name = $request['username'];
                    // $in_room_handle->transaction_id = $transaction_id;
                    // $in_room_handle->status = 'reserved';
                    // $in_room_handle ->save();
                    $in_room_handle = new \App\Model\RoomHandle;
                    $in_room_handle->date = $date;
                    // dd($request['id']);
                    $in_room_handle->user_id = (int)$request['user_id']; //Who's booking.
                    $in_room_handle->guest_name = $request['username']; //Who's will be guest
                    $in_room_handle->transaction_type = "package";
                    $in_room_handle->package_id = $request['package_id'];
                    $in_room_handle->slot_id = $request['week_no']; // Save slot that user choose.
                    $in_room_handle->transaction_id = $request['reference_order'];
                    $in_room_handle->status = 'reserved';
                    $in_room_handle ->save();
                }
                $in_to_rooms_keeper->total_rooms = $in_to_rooms_keeper->total_rooms - $request['total_rooms'];
                $in_to_rooms_keeper->save();

            }
            // $df_days = $checkin -> diffInDays($checkout);

            // rooms_histories insert
            self::saveHistories(
                $request['username'],
              //Activities to keep in rooms_histories
              ("Package[".$request['package_id']."] Booking ".$request['total_rooms']." rooms, Periods(".$checkin->format('Y-m-d')." to ".$checkout->format('Y-m-d').").")
            );

            $msg = "Booking Success";
            $resp_dt = [
            'username' => $request['username'],
            'msg' => $msg,
            // "room_id" => "",
            // "checkin_date" => $request['checkin_date'],
            // "checkout_date" => $request['checkout_date'],
            "duration" => $checkin -> diffInDays($checkout),
            // "total_guests" => $request['total_guests'],
            "total_rooms" => $request['total_rooms'],
            "amount" => "full",
            // "total_amount" => $request['total_amount'],
            "ountstanding_balance" => 0,
            "payment_id" => "PayPal-001",
            "payment_status" => "successful"
          ];
        } else {
            $msg = "Failed : Rooms not available.";
            $resp_dt = [ "msg"=>$msg ];
        }
        // return response()->json(
        //     $resp_dt
        // );
    }


    // Show package slot
    public function showSlot()
    {
        $slots = \App\Model\PackageSlot::query()->get();
        $slots = $slots -> toArray();
        // dd($slots);
        return response()->json(
          $slots
        );
    }


    // Show package slot by package_id
    public function getSlotsByPackageSlots($package_id, $week_no)
    {
        // dd($package_id, $week_no);
        $arr_slots = \App\Model\PackageSlot::where([
          'package_id' => $package_id,
          'week_no' => $week_no
          ]) -> get();
        $arr_slots = $arr_slots->toArray();
        // return response()->json(
        // $arr_slots
        // );
        return $arr_slots;
    }


    public function getSlotsByPackageId($package_id)
    {
        // dd($package_id, $week_no);
        $arr_slots = \App\Model\PackageSlot::where([
          'package_id' => $package_id,
          // 'week_no' => $week_no
          ]) -> get();
        $arr_slots = $arr_slots->toArray();
        // return response()->json(
        // $arr_slots
        // );
        return $arr_slots;
    }


    // 1. Check date in slot of package that available.
    public function checkDateInPeriods($package_id, $week_no, $booking_need)
    {
      // dd($package_id);
        $arr_slots = \App\Model\PackageSlot::where([
          'package_id' => $package_id,
          'week_no' => $week_no
        ]) -> get();
        // dd($arr_slots);
        $bool = true;
        foreach ($arr_slots as $key => $value) {
            $arr_date = self::getDateBetweenDate(
                $arr_slots[$key]["start_date"],
                $arr_slots[$key]["end_date"]
            );
            if (self::periodsRoomsChecker($arr_date, $booking_need)) {
                $arr_slots[$key]["status_rooms_on_periods"] = "available";
            } else {
                $arr_slots[$key]["status_rooms_on_periods"] = "unavailable";
                $bool = false;
            }
        }
        // return response()->json(
        //   $arr_slots
        // );
        // dd($bool);
        return $bool;
    }


    // 2. Check payment that success or incomplete.
    public function packagePaymentChecker($user_id, $packages_id, $amount)
    {
      $payment = new \App\Model\Payments;
      // dd($payment->fpayment($amount));

      $package = PackageMaster::where('id', $packages_id)->first();
      // dd((float)$package->package_price);

      // This variables for test only.
      // $package = PackageMaster::where('')
      $payment_id = "PaymentRef.".uniqid();
      $amount = (((float)$package->package_price)*25)/100; // (price * percent) /100
      $payment_status = "complete";

      $payment = [
        "payment_id" => $payment_id,
        "amount" => $amount,
      ];
      // dd($payment);
      ///////////////////////////////

      if ($payment_id != null || $amount != 0 || $payment_status != "incomplete") {
          // Payment complete.
          $payment_status = "complete";
      } else {
          // Need to complete payment block.
          $payment_status = "incomplete";
      }
      return $payment_status;
    }


    // 3. Book all date in each slot.
    public function bookingAllSlotsInPackage($user_id, $username, $package_id, $week_no, $total_rooms, $reference_order)
    {
        // dd($reference_order);
        $arr_slots = self::getSlotsByPackageSlots($package_id, $week_no);
        $arr_slots = collect($arr_slots)->sortBy('year')->toArray();
        // dd($arr_slots);
        $package_start_date = $arr_slots[0]['start_date'];
        $package_end_date = $arr_slots[count($arr_slots)-1]['end_date'];

        foreach ($arr_slots as $slot) {
          $slot['username'] = $username;
          $slot['total_rooms'] = $total_rooms;
          $slot['user_id'] = $user_id;
          $slot['package_id'] = $package_id;
          $slot['reference_order'] = $reference_order;
          // dd($slot);
          self::bookingRoomsBySlot($slot);
        }
        $resp = [
          'package_start_date' => $package_start_date,
          'package_end_date' => $package_end_date,
        ];
        return $resp;
    }


    // 4. Insert To Table package_member
    public function insertToPackageMembers($dt)
    {
      // dd(\App\Model\PackageSlot::select('id')->where('week_no',$dt['week_no'])->first()->toArray());
      $in = new PackageMember;
      // dd($dt['user_id']);
      $in->user_id = (int)$dt['user_id'];
      $in->package_id = $dt['package_id'];
      $in->slot_id = \App\Model\PackageSlot::select('id')->where('week_no',$dt['week_no'])->first()['id'];
      $in->package_member_status = 1;
      $in->package_start_time = $dt['package_start_time'];
      $in->package_end_time = $dt['package_end_time'];
      $in->payment_id = $dt['payment_id'];
      $in->dept = $dt['dept'];
      $in->save();
      // PackageMember::create([
      //   'user_id' => $dt['user_id'],
      //   'package_id' => $dt['package_id'],
      //   'slot_id' => \App\Model\PackageSlot::select('id')->where('week_no',$dt['week_no'])->first(),
      //   'package_member_status' => 1,
      //   'package_start_time' => $dt['package_start_time'],
      //   'package_end_time' => $dt['package_end_time'],
      //   'payment_id' => uniqid()
      // ]);
      // dd($resp);
    }


    public function payment($user_id, $arr_amount, $package_id, $reference_order)
    {
      // dd($amount*1.00);
      $fake_payment = new AppPaymentsController;

      $payment_dt=[
        "amount" => $arr_amount['amount'],
        "currency"=> "THB",
        "description" => "Package ".$package_id,
        "source_type" => "card",
        "mode"=> "token",
        "token"=> "ABC-12345", // Fake token for test
        "reference_order"=> $reference_order
      ];

      // *** Fake payment ***
      $resp = $fake_payment->fpayment($payment_dt);
      // $resp = json_decode($resp);
      if($resp->status = 'S'){
        // dd($resp->getData());
        $payment_tbl = new Payments;
        $payment_tbl->user_id = $user_id;
        $payment_tbl->gateway_id = 0;
        $payment_tbl->amount = $arr_amount['amount'];
        $payment_tbl->usd_amo = 0;
        $payment_tbl->payment_id = $reference_order;
        $payment_tbl->save();

        $resp = $resp->getData();
        return $resp;
      }else{
        return 0;
      }
    }


    // Package booking MAIN controller
    public function mainPackageBooking(Request $request)
    {

      // $user = User::where('username',Auth::user()->username)->first();
      // dd($package);
      // // die();
      // $payment_status = self::packagePaymentChecker($arr_request['username'], $arr_request['package_id']);

      // $payment_status = self::packagePaymentChecker(
      //   $user->id,
      //   $arr_request['package_id'],
      //   $arr_request['amount']
      // );

      // dd($payment_resp);
      // die();

      if(Auth::user()->type == "member"){;
        $user = \App\Model\User::where('username',(Auth::user()->username))->first();
        $arr_request = $request -> all();
        $available_slot_checker = self::checkDateInPeriods($arr_request['package_id'],$arr_request['week_no'],$arr_request['total_rooms']);
        $package = PackageMaster::where('id',$arr_request['package_id'])->first();
      }else if(Auth::user()->type == "register"){
        return (new HttpResource(["message", "Please verify your profiles."],401));
      }else{
        $user = null;
      }

      if(isset($user) && $user != null){
        $user = $user -> toArray();
        // dd($user);
        // die();
        if ($available_slot_checker) {
          if($arr_request['amount']['amount_type'] == 'full'){
            // dd('hit');
            if((float)$arr_request['amount']['amount'] == (float)$package->package_price){
              $payment_resp = $this->payment($user['id'], $arr_request['amount'], $arr_request['package_id'], uniqid());
              $dept = 0;
            }else{
              $status_code = 400;
              $msg = "Failed.";
              $response = ["message"=> $msg];
              return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode($status_code);
            }
          }else if($arr_request['amount']['amount_type'] == 'deposit'){
            // dd('missed');
            $min_amount = (25 * (float)$package->package_price)/100;
            // dd($total_amount);
            if((float)$arr_request['amount']['amount'] >= $min_amount && (float)$arr_request['amount']['amount'] <= (float)$package->package_price){
              // dd($user['id']);
              $payment_resp = $this->payment($user['id'], $arr_request['amount'], $arr_request['package_id'], uniqid());
              $dept = ((float)$package->package_price - (float)$arr_request['amount']['amount']);
            }else{
              // dd('missed');
              $status_code = 400;
              $msg = "Failed.";
              $response = ["message"=> $msg];
              return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode($status_code);
            }
          }else{
            $status_code = 400;
            $msg = "Failed.";
            $response = ["message"=> $msg];
            return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
          }

          if($payment_resp->status == "S"){
            // Process booking
            $bookingAllSlotsInPackage_resp = self::bookingAllSlotsInPackage(
              $user['id'],
              $user['username'],
              $arr_request["package_id"],
              $arr_request['week_no'],
              $arr_request['total_rooms'],
              $payment_resp->reference_order
            );
            // dd($bookingAllSlotsInPackage_resp);
            // die();
            // dd($user['id']);
            $arr_dt_to_package_members = [
              'user_id' => $user['id'],
              'package_id' => $arr_request['package_id'],
              'week_no' => $arr_request['week_no'],
              'package_start_time' => $bookingAllSlotsInPackage_resp['package_start_date'],
              'package_end_time' => $bookingAllSlotsInPackage_resp['package_end_date'],
              'payment_id' => $payment_resp->reference_order,
              'dept' => $dept
            ];
            // dd($arr_dt_to_package_members);
            // die();

            self::insertToPackageMembers($arr_dt_to_package_members);

          }else{
            $status_code = 500;
            $msg = "Payment Failed.";
            $response = ["message"=> $msg];
            return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
          }
            $status_code = 200;
            $msg = "Package booking success.";
            $data = PackageMember::where("user_id", $user["id"])
                    ->where("package_id", $package->id)->get();
            $response = [
              "message"=> $msg,
              "data" => $data
            ];
            return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
        }else{
          $status_code = 500;
          $msg = "Payment failed.";
          $response = ["message"=> $msg];
          return (new HttpResource($response))
            ->response($msg)
            ->setStatusCode($status_code);
        }
      }else{
        $status_code = 404;
        $msg = "Failed user not found.";
        $response = ["message"=> $msg];
        return (new HttpResource($response))
          ->response($msg)
          ->setStatusCode($status_code);
      }
    }


    public function getMyPackageV2()
    {
      // dd(Auth::user()->username);
      $username = Auth::user()->username;
      $url = "/src/img";
      // dd($username);
      $user = User::select('id')->where('username', $username)->first();
      $packages = PackageMember::where('user_id', $user['id'])->get();
      // dd($packages);
      foreach ($packages as $package) {
        $package_detail = PackageMaster::where("id", $package['package_id'])->first();
        $package_detail = $package_detail->toArray();
        // dd($package_detail['package_description']);
        $package["packageDetail"] = $package_detail['package_description'];
        $package["packageAgreement"] = $package_detail['package_agreement'];
        $package["packageImage"] = [
          "name" => $package_detail['package_name'],
          "description" => $package_detail['package_name'],
          "imgURL" => $url.$package_detail['package_image']
        ];
      }

      $status_code = 200;
      $msg = "Success";
      $response = ["data"=> $packages];
      return (new HttpResource($response))
        ->response($msg)
        ->setStatusCode($status_code);
    }


    public function getPromotionsV2()
    {
      $now = \Carbon\Carbon::today();
      $promotions = Promotion::where('expire_date','<',$now)
                              ->where('status',1)
                              ->get();
      // dd($promotions);
      if($promotions && $promotions != []){
        $promotions = $promotions->toArray();
        // dd($promotions);
        $status_code = 200;
        $msg = "Success";
        $response = ["data"=> $promotions];
      }else{
        $status_code = 404;
        $msg = "Data not found.";
        $response = ["msg"=> $msg];
      }


      return (new HttpResource($response))
        ->response($msg)
        ->setStatusCode($status_code);
    }
}
