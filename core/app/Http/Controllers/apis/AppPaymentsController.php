<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;


use App\Model\Payments;
use App\Model\PackageMember;
use App\Model\User;

class AppPaymentsController extends Controller
{
    // ====== TEST ONLY FUNCTION ======
    public function fpayment($request)
    {
      $response = [
        'reference_order' => $request['reference_order'],
        'resultResponse' => "",
        'customerName' => 'www.pisona.com',
        'amount' => $request['amount'],
        'saleId' => uniqid(),
        'status' => 'S'
      ];
      // dd($response['reference_order']);
      return response()->json($response);
    }


    public function paymore(Request $request)
    {
      // printf($request->username);
      $user = User::where('username', $request->username)->first();
      $user_package = PackageMember::where('user_id', $user->id)
                      ->where('id', $request->id)->first();
      // dd($user_package);
      if($user){
        if($user_package->dept > 0){
          // dd($user_package->dept);
          printf($request->amount);

          // === This section is payment only for test === //
          $payment_tbl = new Payments;
          $payment_tbl->user_id = $user->id;
          $payment_tbl->gateway_id = 0;
          $payment_tbl->amount = $request['amount'];
          $payment_tbl->usd_amo = 0;
          $payment_tbl->payment_id = $user_package->payment_id;
          $payment_tbl->save();
          // ============================================= //

          // Update package_members dept
          $user_package->dept = (float)$user_package->dept - (float)$request['amount'];
          $user_package->save();
          // dd($user_package);

        }else{
          return (new HttpResource(["message" => "Package not found"], 404));
        }
      }else{
        return (new HttpResource(["message" => "User not found."], 404));
      }

      // printf($user_package);
      // die();
    }

}
