<?php

namespace App\Http\Controllers\apis;
use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class AppMyOwnController extends Controller
{
  //To get own rooms
  public function getMyRooms($username)
  {
    $arr_rooms = \App\Model\RoomHandle::where('user_id',$username)->get();
    // dd($arr_rooms->toArray());
    return $arr_rooms;
  }


  public function main($username)
  {
    $user_id = \App\Model\User::where('username', $username) ->first();
    // dd($user_id['id']);
    $dt = self::getMyRooms($user_id['id']);
    // $msg = "Username or Password is incorrect";
    $response = ['data' => $dt];
    return (new HttpResource($response))
        ->response($dt)
        ->setStatusCode(200);
  }


  public function getAllMine($username)
  {
    $user_id = \App\Model\User::where('username', $username) ->first();
    // $booked_rooms = self::getMyRooms($user_id['id']);
    $arr_rooms = \App\Model\RoomHandle::where('user_id',$user_id['id'])->get();
    // dd($arr_rooms->where('transaction_type','package')->groupBy('transaction_id')->toArray());
    // dd($arr_rooms->toArray());
    // $dt = $arr_rooms->where('transaction_type','package')->groupBy('transaction_type')->sortBy('transaction_id');
    // $dt = $dt->groupBy('transaction_id');
    $package_rooms = \App\Model\RoomHandle::where([['user_id',$user_id['id']], ['transaction_type','package']])
    ->get();
    $package_rooms_year = [];
    $year_package = [];
    $year = "";
    $year_old = "";
    $check = [];
    $count = false;
    foreach ($package_rooms as $key => $value) {
      if(Carbon::parse($value->date)->format('Y') != $year){
          $year = Carbon::parse($value->date)->format('Y');
          if($count == true){
            // dd($year);
            $package_rooms_year[$year_old]=$year_package;
            $year_package=[];
          }
          array_push($year_package,$value);

        }else{
          $year_old = Carbon::parse($value->date)->format('Y');
          array_push($year_package,$value);
        }
        $count=true;
        if($key == count($package_rooms)-1){
          $package_rooms_year[$year_old]=$year_package;
          $year_package=[];
        }
    }
    // return response()->json($package_rooms_year);
    $booked_rooms = \App\Model\RoomHandle::where([['user_id',$user_id['id']], ['transaction_type','booking']])->get();
    // $booked_rooms = $booked_rooms->groupBy('transaction_id');
    $booked_rooms = $booked_rooms->sortBy('date');
    $booked_rooms->makeHidden(['package_id']);
    // dd($booked_rooms);
    $dt['package'] = $package_rooms_year;
    $dt['booking'] = $booked_rooms;
    // dd($dt['package']);
    $response = ['data' => $dt];
    return response()->json($response);
    // return (new HttpResource($response))
    //     ->response($dt)
    //     ->setStatusCode(200);
  }
}
