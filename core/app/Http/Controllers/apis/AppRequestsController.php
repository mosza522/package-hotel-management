<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;

//Model
use App\Model\Posts;
use App\Model\User;
use App\Model\RoomHandle;
use App\Model\Requests;

class AppRequestsController extends Controller
{
    // Request to posts/offer something
    public function postOffer(Request $_request)
    {
      $request = $_request->all();
      // dd($request);
      $post_id = (isset($request['post_id']))? $request['post_id']:"";
      $username = (isset($request['username']))? $request['username']:"";
      $content = (isset($request['content']))? $request['content']:"";
      $transaction_id = (isset($request['transaction_id']))? $request['transaction_id']:"";
      // $conent = (isset())

      $usr = User::where('username',$username)->first();
      if($username != ""){
        if($usr){
          $post = Posts::where('id', $post_id)->first();
          if($post){
            // dd($username);
            $response = Requests::create([
              'request_sender'=>$username,
              'post_id' => $post['id'],
              'content' => $content,
              'status' => null,
              'transaction_id' => $transaction_id
            ]);
            $status_code = 201;
            $msg = "Success";
            $response = ["data"=> $response];
          }else{
            $status_code = 404;
            $msg = "Not found.";
            $response = ["message"=> $msg];
          }
        }else{
          $status_code = 400;
          $msg = "Username not found.";
          $response = ["message"=> $msg];
        }
      }else{
        $status_code = 400;
        $msg = "Username not found.";
        $response = ["message"=> $msg];
      }

      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }


    // public function getMyPackages()
    // {
    //   //To get my requsets
    // }
}
