<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;

class AppAvailableRoomsController extends Controller
{
    //Get available rooms by days.
    public function availableOnDate($date)
    {
        // $arrayRequests = $request->all();
        // var_dump($request);
        // $total_available = \App\Model\RoomHandle::where('date','!=', $date) -> count();
        $total_room = \App\Model\Room::count(); //Get total room in the system.
        $pending_room = \App\Model\RoomHandle::where('date', '=', $date) -> count(); //Get rooms in use.
        $available_room = $total_room-$pending_room;

        return response()->json([
          'total_rooms' => $total_room,
          'available' => $available_room,
          'pending' => $pending_room
      ]);
    }

    // Checking Available rooms in rooms_keeper table.
    public function listAvailable()
    {
      $rooms = \App\Model\RoomsKeeper::all();
      return $rooms;
    }

    //Get available checker.
    // public function availableChecker()
    // {
    //   $checkin = Carbon::parse();
    //   $checkout = Carbon::parse();
    //   $df_days = $checkin -> diffInDays($checkout);
    //
    //   for($checkin <= $checkout){
    //
    //   }
    //
    //   return $available;
    // }
}
