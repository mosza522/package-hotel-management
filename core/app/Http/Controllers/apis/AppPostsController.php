<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;
use Mail;
use Auth;
//Model
use App\Model\Posts;
use App\Model\User;
use App\Model\RoomHandle;
use App\Model\Requests;
use App\Model\Exchange;

class AppPostsController extends Controller
{
    public function getAllPosts()
    {
      // dd(Auth::user());
      if($posts = Posts::where('status','1')->get()){
        $status_code = 200;
        $msg = "Success";
        $response = ["data"=> $posts];
      }else{
        $status_code = 400;
        $msg = "Failed, Not found any post.";
        $response = ["data"=> $posts];
      }
      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }


    public function getPostByUsername($username)
    {
      $user = User::where('username', $username)->first();
      // dd($user['status']);
      if($user)
      {
        // dd($user);
        // $user = $user->toArray();
        $posts = Posts::where([['user_id','=',$user['id']], ['status','=','1']])->get();
        $posts = $posts->toArray();
        if($posts)
        {
          foreach ($posts as $key => $value) {
            // dd($key);
            $requests = Requests::where('post_id',$posts[$key]['id'])->get();
            $requests = $requests->toArray();
            $posts[$key]['request']=[];
            // dd($requests);
            if($requests){
              $posts[$key]['request']=$requests;
            }
          }
          // dd($posts);
          $status_code = 200;
          $msg = "successful";
          $response = ["data"=> $posts];
        }else{
          $status_code = 204;
          $msg = "Sorry, Not found any posts.";
          $response = ["message"=> $msg];
        }
      }else{
        $status_code = 400;
        $msg = "Sorry, Not found any posts.";
        $response = ["message"=> $msg];
      }
      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }


    public function postsOffer(Request $_request)
    {
      $request = $_request->toArray();
      // dd($request);
      $user_id = (isset($request['username']))? (User::where('username', $request['username'])->first())['id']:"";
      $title = (isset($request['title']))? $request['title']:"";
      $image = (isset($request['image']))? $request['image']:"";
      $details = (isset($request['details']))? $request['details']:"";
      $agreements = (isset($request['agreements']))? $request['agreements']:"";
      $status = (isset($request['status']))? $request['status']:"";
      $post_type = (isset($request['post_type']) && in_array($request['post_type'], ['sale','exchange', 'any']))? $request['post_type']:"";
      $transaction_id = (isset($request['transaction_id']))? $request['transaction_id']:"";
      $content = (isset($request['content']))? $request['content']:"";

      // $offering = RoomHandle::where('transaction_id', $transaction_id)->get();
      // $offering = $offering->toArray();
      // dd($offering);
      $checker = Posts::where('transaction_id', $transaction_id)->first();
      $check_in_rooms_handle = RoomHandle::where([['transaction_id',$transaction_id],['user_id',$user_id]])->get();
      // dd($check_in_rooms_handle->toArray());
      if($checker){
        $status_code = 406;
        $msg = "You can't post offer this room.";
        $response = ["message"=> $msg];
      }else{
        Posts::create([
          'user_id' => $user_id,
          'title' => $title,
          'image' => $image,
          'details' => $details,
          'agreements' => $agreements,
          'status' => $status,
          'post_type' => $post_type,
          'transaction_id' => $transaction_id,
          'content' => $content
        ]);
        $status_code = 201;
        $msg = "Post Success";
        $response = ["message"=> $msg];
      }

      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }


    public function rejectRequest(Request $_request)
    {
      $request = $_request->toArray();
      try {
        $reject = Requests::where('id',$request['request_id'])->first();
        if($reject){
          $reject->status = 'rejected';
          $reject->save();
          $status_code = 200;
          $msg = "Rejected";
          $response = ["message"=> $msg];
        }else {
          $status_code = 400;
          $msg = "Bad Request";
          $response = ["message"=> $msg];
        }
      } catch (\Exception $e) {
        $status_code = 500;
        $msg = "Something went wrong.";
        $response = ["message"=> $msg];
      }
      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }


    public function writeToExchagne($event)
    {
      try {
      // dd($event);
        foreach ($event as $key => $value) {
          if($event[$key]['transaction_id']){
              // Exchange::create([
              //   'from_user_id' => 1,
              //   'to_user_id' => 2,
              //   'transaction_id' => 'testtest'
              // ]);
              $in_exchange = new Exchange;
              $in_exchange->from_user_id = $event[$key]['owner_change_from'];
              $in_exchange->to_user_id = $event[$key]['owner_change_to'];
              $in_exchange->transaction_id = $event[$key]['transaction_id'];
              $in_exchange->save();
              // dd($in_exchange);
          }
        }
        return 1;
      } catch (\Exception $e) {
        return $e;
      }
    }


    public function setStatusPostToZero($post_id)
    {
      // dd($post_id);
      try {
        $post_row = Posts::where('id', $post_id)->first();
        $post_row->status = 0;
        $post_row->save();

        return 1;
      } catch (\Exception $e) {
        return $e;
      }
    }


    public function setStatusRoomsHandleToNull($transaction_id)
    {
      $rooms = RoomHandle::where('transaction_id', $transaction_id)->get();
      foreach ($rooms as $room) {
        $room->post_status = null;
        $room->save();
      }
    }


    public function setStatusOtherRequestsToReject($post_id)
    {
      try {
        // dd(">>>".$post_id);
        $request_rows = Requests::where('post_id',$post_id)
        ->where('status', null)
        // ->where('status','!=', 'accepted')
        ->get();
        // $request_rows = $request_rows->where('status', null)->get();
        // dd($request_rows->toArray());
        foreach ($request_rows as $request_row) {
            $request_row->status = 'rejected';
            self::setStatusOtherRoomsHandleToNull($request_row['transaction_id']);
            $request_row->save();
        }
        return 1;
      } catch (\Exception $e) {
        // dd($e);
        return $e;
      }
    }


    public function setStatusAcceptRequestToAccept($request_id)
    {
      // dd($request_id);
      try {
        $request_row = Requests::where('id', $request_id)->first();
        $request_row->status = "accepted";
        $request_row->save();
        self::setStatusOtherRoomsHandleToNull($request_row['transaction_id']);

        return 1;
      } catch (\Exception $e) {
        return $e;
      }

    }


    public function roomsExchange($trading_item)
    {
      // dd($trading_item);
      // $bool= 0;

      // Poster -> Requester
      try {
        $rooms = RoomHandle::where('transaction_id', $trading_item['post_owner']['transaction_from_roomhandle'])->get();
        // dd($rooms);
        $rooms->user_id = $trading_item['request_owner']['owner_id'];
        $rooms->guest_name = $trading_item['request_owner']['username'];
        $rooms->post_status = null;
        $event['exchange_1'] = [
          'transaction_id' => $trading_item['post_owner']['transaction_from_roomhandle'],
          'owner_change_from' => $trading_item['post_owner']['owner_id'],
          'owner_change_to' => $trading_item['request_owner']['owner_id']
        ];
        // dd($event);

        // Requester -> Poster
        $rooms_request = RoomHandle::where('transaction_id', $trading_item['request_owner']['transaction_from_roomhandle'])->get();
        // dd($rooms_request);
        $rooms_request->user_id = $trading_item['post_owner']['owner_id'];
        $rooms_request->guest_name = $trading_item['post_owner']['username'];
        $rooms_request->post_status = null;
        $event['exchange_2'] = [
          'transaction_id' => $trading_item['request_owner']['transaction_from_roomhandle'],
          'owner_change_from' => $trading_item['request_owner']['owner_id'],
          'owner_change_to' => $trading_item['post_owner']['owner_id']
        ];

        self::setStatusAcceptRequestToAccept($trading_item['request_owner']['request_id']);
        self::setStatusPostToZero($trading_item['post_owner']['post_id']);
        self::setStatusOtherRequestsToReject($trading_item['post_owner']['post_id']);
        // Save Data That Exchange
        self::writeToExchagne($event);
        $rooms->save();
        $rooms_request->save();

        return 1;
      } catch (\Exception $e) {
        $msg = $e;
        return 0;
      }
      // dd($rooms_request->toArray());
    }


    public function acceptRequests(Request $_request)
    {
      $req = $_request->toArray();
      $post_id = (isset($req['post_id']))? $req['post_id']:"";
      $request_id = (isset($req['request_id']))? $req['request_id']:"";

      if($post_id && $request_id){
        // dd($post_id);
        $post = Posts::where('id',$post_id)->first();
        $post_request = Requests::where('id', $request_id)
                        ->where('post_id',$post['id'])
                        ->first();
        if($post && $post_request){
          // dd($post_request);
          // Change status of reqeusts.status to acceptReuests.
          $post_request->status = 'accept';
          // Change status of posts.status to 0.
          $post->status = 0;
          // dd($post->id);
          $trading_arr['post_owner'] = [
            'transaction_from_roomhandle' => $post->transaction_id,
            'post_id' => $post->id,
            'owner_id' => (int)($post->user_id),
            'username' => (string)(User::where('id',$post->user_id)->first()['username'])
          ];

          $trading_arr['request_owner'] = [
            'transaction_from_roomhandle' => $post_request->transaction_id,
            'request_id' => $post_request->id,
            'owner_id' => (int)(User::where('username',$post_request->request_sender)->first()['id']),
            'username' => (string)($post_request->request_sender)
          ];

          if(self::roomsExchange($trading_arr)){
            $status_code = 200;
            $data = "successful";
          }else{
            $status_code = 400;
            $msg = "Bad Requests.";
          }
          // dd($trading_arr);
          // Save data
          // $post_request -> save();
          // $post -> save();

          //======= EMAIL SENDER =======//
          // Mail::raw('Text to e-mail', function($message)
          // {
          //     $message->from('thatsanai@buzzfreeze.com', 'Laravel');
          //
          //     $message->to('thatsanai@buzzfreeze.com');
          //     // ->cc('');
          // });

          // echo "HTML Email Sent. Check your inbox.";

          $status_code = 200;
          $data = "successful";
        }else{
          $status_code = 400;
          $msg = "Bad Requests.";
        }
      }else{
        $status_code = 400;
        $msg = "Bad Requests.";
      }

      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }

    public function deletePosts(Request $_request)
    {
      $post_id = $_request->input('post_id');
      $username = $_request->input('username');

      if($post_id && $username){
        $owner = User::where('username',$username)->first();
        $post = Posts::where('id',$post_id)
                ->where('user_id',$owner->id)
                ->first();
        $requets_tbl = Requests::where('post_id',$post_id)->get();
        // dd($post);
        if($post){

          // Remove posts
          // $post->delete();

          // Close Posts
          $post->status = 0;
          $post->save();

          //Set all requsts to rejected.
          if($requets_tbl){
            foreach ($requets_tbl as $key => $value) {
              $requets_tbl[$key]->status = 'rejected';
              $requets_tbl[$key]->save();
            }
          }

          $status_code = 200;
          $msg = "Delete successful";
          $response = ["message"=> $msg];
        }else{
          $status_code = 404;
          $msg = "Data Not Found";
          $response = ["message"=> $msg];
        }
      }else{
        $status_code = 404;
        $msg = "Data Not Found";
        $response = ["message"=> $msg];
      }

      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }
}
