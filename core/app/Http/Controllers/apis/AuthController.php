<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\Register;
use App\Model\User;
use Hash;
class AuthController extends Controller
{
    public function login(Request $request)
    {
      $loginData = $request->validate([
        'username' => 'required',
        'password' => 'required'
      ]);
      $check = Register::where('username',$request->username)->first();
      if($check){
        if($check->type == 'register'){
          if(Hash::check( $request->password,$check->password )){
            Auth::guard('register')->attempt($loginData);
            $user = Auth::guard('register')->user();
            $accessToken = $user->createToken('authTokenRe');
            return response(['user' => $user,'access_token'=>$accessToken]);
          }
        }
        else if($check->type == 'member'){
          $checkMember = User::where('username',$request->username)->first();
          if(Hash::check( $request->password,$check->password )){
            Auth::attempt($loginData);
            $user = Auth::user();
            $accessToken = $user->createToken('authToken');
            return response(['user' => $user,'access_token'=>$accessToken]);
          }
        }
      }
      return response(['message'=>'Invalid credentials']);


      // if($request->guard == "api-user"){
      //   if(!auth()->attempt($loginData))
      //   {
      //     return response(['message'=>'Invalid credentials']);
      //   }
      //   $user = Auth::user();
      //   $accessToken =$user->createToken('authToken');
      //   return response(['user' => $user,'access_token'=>$accessToken]);
      //
      // }else if($request->guard == "api-register"){
      //   if(Auth::guard('register')->attempt($loginData)){
      //       $user = Auth::guard('register')->user();
      //       $accessToken = $user->createToken('authTokenRe');
      //       return response(['user' => $user,'access_token'=>$accessToken]);
      //   }
      //   else {
      //     return response(['message'=>'Invalid credentials']);
      //   }
      //
      // }
  }
}
