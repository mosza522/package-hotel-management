<?php

namespace App\Http\Controllers\apis;

use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\CarTransfer;
use App\Model\CarTransaction;

class AppCarTransferController extends Controller
{
    public function getCarTransferType()
    {
        $cars_tansfer = CarTransfer::get();
        // dd(count($cars_tansfer));
        if($cars_tansfer)
        {
          $cars_tansfer = $cars_tansfer->toArray();
          $src_img = "http://pisonatest.topwork.asia/assets/backend/image/transfer/";

          foreach ($cars_tansfer as $k => $v)
          {
            $cars_tansfer[$k]['saloon_picture'] = $src_img.$cars_tansfer[$k]['saloon_picture'];
            $cars_tansfer[$k]['van_picture'] = $src_img.$cars_tansfer[$k]['van_picture'];
          }
        }else{
          $response = ['msg' => 'Data not found.'];
          return (new HttpResource($response))
            ->response($response)
            ->setStatusCode(404);
        }

        $response = ['data' => $cars_tansfer];
        return (new HttpResource($response))
          ->response($response)
          ->setStatusCode(200);
    }

    public function bookingCartransfer(Request $request)
    {
        $req = $request->all();

        $user_id = (isset($req['username']))? (\App\Model\User::where('username' ,$req['username'])->first())['id']:"";
        // dd($user_id);
        $arrival_time = (isset($req['arrival_time']))? $req['arrival_time']:"";
        $seats = (isset($req['seat']))? $req['seat']:"";
        $flight_number = (isset($req['flight_number']))? $req['flight_number']:"";
        $airlines = (isset($req['airlines']))? $req['airlines']:"";
        $first_name = (isset($req['first_name']))? $req['first_name']:"";
        $last_namey = (isset($req['last_name']))? $req['last_name']:"";
        $passport_id = (isset($req['passport_id']))? $req['passport_id']:"";
        $email = (isset($req['email']))? $req['email']:"";
        $phone = (isset($req['phone']))? $req['phone']:"";
        $another_contact = (isset($req['another_contact']))? $req['another_contact']:"";
        $payment_id = (isset($req['payment_id']))? $req['payment_id']:"";
        // $car_id = (isset($req['car_type']))? (CarTransfer::select('car_id')->where('car_type', $req['car_type']))->first():"";
        $car_type = (isset($req['car_type']))? $req['car_type']:"";
        // dd($car_type);

        if (CarTransaction::create([
          'user_id' => $user_id,
          'time' => $arrival_time,
          'car_type' => $car_type,
          'flight_number' => $flight_number,
          'arrival_time' => $arrival_time,
          'payment_id' => $payment_id,
          'airlines' => $airlines,
          'another_contact' => $another_contact
        ])) {
          $response = ['message' => 'Booking Successful.'];
          $status_code = 201;
        } else {
          $response = ['message' => 'Booking failed something went wrong.'];
          $status_code = 400;
        }
        return (new HttpResource($response))
          ->response($response)
          ->setStatusCode($status_code);
    }
}
