<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;

use App\Model\Payments;

class AppPaymentsHistory extends Controller
{
    // Payments History section
    public function paymentsHistory($user_id)
    {
      $histories = Payments::where('user_id', $user_id)->get();
      // dd($histories->isEmpty());

      if(!$histories->isEmpty()){
        $response = $histories;
        $status_code = 200;
        $msg = "successful";
      }else{
        $status_code = 404;
        $msg = "Data not found.";
        $response = ["message" => $msg];
      }

      return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }

}
