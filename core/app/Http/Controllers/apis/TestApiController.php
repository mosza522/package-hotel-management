<?php

namespace App\Http\Controllers\apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
class TestApiController extends Controller
{
    // Just for test //
    public function test()
    {
      // $dt = \App\Model\Test::all();

      $user = \App\Model\Test::create(
        [
          'user_id' => '1',
          'date' => '2020-1-23 00:00:00',
          'guest_name' => 'Tester02',
          'status' => "active",
        ]
      );

      // $rec -> user_id = '1';
      // $rec -> date = '2020-1-22 00:00:00';
      // $rec -> guest_name = 'Tester01';
      // $rec -> status = "active";
      //
      // $rec -> create();

      // $arr['user_id'] = "1";
      // $arr['date'] = "2020-1-22 00:00:00";
      // $arr['guest_name'] = "Tester01";
      // $arr['status'] = "active";

      // $created = \App\Model\Test::create();

      return 0;
    }
}
