<?php

namespace App\Http\Controllers\apis;

use App\Http\Helper\MimeCheckRules;
use App\Model\CodeManager;
use App\Model\GeneralSetting;
use App\Model\User;
use App\Model\AppUser;
use App\Model\AppToken;
use App\Model\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\HttpResource;
use App\Http\Resources\HeaderResource;

use Log;

// == Log Notify Note ==
// Log::emergency($error);
// Log::alert($error);
// Log::critical($error);
// Log::error($error);
// Log::warning($error);
// Log::notice($error);
// Log::info($error);
// Log::debug($error);

use Image;

class AppRegisterController extends Controller
{
    // private $user;

    // public function __construct(User $user)
    // {
    //     $this->user = $user;
    // }

    public function index()
    {
        return '200';
    }

    public function getAppID()
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        // $charLength = 24;
        // $response = [];//Generate a random string.
        // $appId = openssl_random_pseudo_bytes($charLength);
        // //Convert the binary data into hexadecimal representation.
        // $appIds = bin2hex($appId);
        // //Print it out for example purposes.
        // // echo $token;
        // $arrayCreate = ['appId' => $appIds];
        $appId = $this->createAppID();
        $response = [
            'appId' => $appId['app_id']
        ];
        // $response = AppToken::first();
        // echo "<pre>";
        // print_r($response);
        // echo "</pre>";

        // return (new HttpResource ($arrayCreate))
        //     ->response()
        //     ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return new HttpResource($response);
    }

    public function createAppID()
    {
        $charLength = 24;
        $response = [];//Generate a random string.
        $appId = openssl_random_pseudo_bytes($charLength);
        //Convert the binary data into hexadecimal representation.
        $appIds = bin2hex($appId);
        //Print it out for example purposes.
        // echo $token;
        $arrayCreate = ['app_id' => $appIds];
        $response = AppToken::create($arrayCreate);
        return $response;
    }

    public function loginMember(Request $request)
    {
        // dd($request);
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $msg = "";
        $arrayRequests = $request->all();

        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $username = (isset($arrayRequests['username']))? $arrayRequests['username']:"";
        $email = (isset($arrayRequests['email']))? $arrayRequests['email']:"";
        $password = (isset($arrayRequests['password']))? $arrayRequests['password']:"";
        $passwordEncrypt = password_hash($password, PASSWORD_BCRYPT);
        // $password_encrypt = Hash::make($password);
        // if($appId == ""){
        //     $aId = $this->getAppID();
        //     $get_appId = json_decode(json_encode($aId), true);
        //     $appId = $get_appId['appId'];
        // }

        // $arrayRequests['username'] = $username;
        // $arrayRequests['email'] = $email;
        // $arrayRequests['password'] = $password;
        // $arrayRequests['password_encrypt'] = $password_encrypt;
        // echo "<pre>";
        // print_r($arrayRequests);
        // echo "</pre>";
        // echo $password_encrypt;
        // echo "<br />";

        $getLogins = User::where(function ($q) use ($username, $email, $passwordEncrypt) {
            if ($username != "") {
                return $q->where('username', $username);
            } elseif ($email != "") {
                return $q->where('email', $email);
            }
        })->get();
        // dd($getLogins);
        // error_log($getLogins);

        $getLogin = json_decode(json_encode($getLogins));

        if (!empty($getLogin)) {
            // (password_verify('rasmuslerdorf', $hash))
            // echo $getLogin[0]->password." : ".$password_encrypt;
            // echo "<br />";
            // echo($password ."\n". $getLogin[0]->password);
            if (password_verify($password, $getLogin[0]->password)) {
                $msg = "Login Success";
                $users = new User();
                $users->sex = $getLogin[0]->sex;
                $gender = $users->sex();
                $imgUrl = url('core/storage/app/images/passportID/thumbnail');
                $response = [
                    'appId' => $appId,
                    'userId' => $getLogin[0]->id,
                    'username' => $getLogin[0]->username,
                    'email' => $getLogin[0]->email,
                    'fullName' => $getLogin[0]->full_name,
                    'phone' => $getLogin[0]->phone,
                    'sex' => $gender,
                    'picture' => $imgUrl.'/'.$getLogin[0]->picture,
                    'message' => $msg
                ];
            } else {
                $msg = "Username or Password is incorrect";
                $response = ['message' => $msg];
                return (new HttpResource($response))
                    ->response($msg)
                    ->setStatusCode(400);
            }
        } else {
            if ($username == "" && $email == "") {
                $msg = "Username or Email should not be empty";
            } else {
                $msg = "Username or Password is incorrect";
            }
            $response = ['message' => $msg];
            return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode(400);
        }

        // $response = [
        //     'userId' => '1',
        //     'appId' => $appId,
        // ];

        // return (new HttpResource ($response))
        //     ->response()
        //     // ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return (new HttpResource($response))
                    ->response($msg)
                    ->setStatusCode(200);
    }


    // Register Member to the System
    public function registerMember(Request $request)
    {
        // Log::info($request);
        // error_log($request->get('username'));
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $msg = "";
        $arrayRequests = $request->all();

        // == Original ==
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $username = (isset($arrayRequests['username']))? $arrayRequests['username']:"";
        $email = (isset($arrayRequests['email']))? $arrayRequests['email']:"";
        $phone = (isset($arrayRequests['phone']))? $arrayRequests['phone']:"";
        $password = (isset($arrayRequests['password']))? $arrayRequests['password']:"";
        $confirmPassword = (isset($arrayRequests['confirmPassword']))? $arrayRequests['confirmPassword']:"";
        $passportId = (isset($arrayRequests['passportId']))? $arrayRequests['passportId']:"";
        $firstName = (isset($arrayRequests['firstName']))? $arrayRequests['firstName']:"";
        $lastName = (isset($arrayRequests['lastName']))? $arrayRequests['lastName']:"";
        $sex = (isset($arrayRequests['sex']))? $arrayRequests['sex']:"";
        $picture = (isset($arrayRequests['picture']))? $arrayRequests['picture']:"";

        // echo "<pre>";
        // print_r($picture);
        // echo "</pre>";

        $chkUs = User::where('username', $username)->get();
        $chkU = json_decode(json_encode($chkUs));
        if (!empty($chkU)) {
            $msg = 'Username Exists';
            $response = ['message' => $msg];
            return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode(400);
        }

        $chkEs = User::where('email', $email)->get();
        $chkE = json_decode(json_encode($chkEs));
        if (!empty($chkE)) {
            $msg = 'Email Exists';
            $response = ['message' => $msg];
            return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode(400);
        }


        $chkPass = ($password == $confirmPassword)?true:false;
        if (!$chkPass) {
            $msg = 'Password not match';
            $response = ['message' => $msg];
            return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode(400);
        } else {
            // $arrayRequests['password'] = Hash::make($password);
            $arrayRequests['password'] = password_hash($password, PASSWORD_BCRYPT);
        }


        if (!empty($picture)) {
            $img = time().'.'.$picture->getClientOriginalExtension();
            error_log($img);
            $image = Image::make($picture->getRealPath());
            $thumbnailPath = storage_path('app/images/passportID/thumbnail');
            $image->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbnailPath.'/'.$img);
            // $destinationPath = public_path('images/passportID');
            $destinationPath = storage_path('app/images/passportID/');
            $picture->move($destinationPath, $img);

            $arrayRequests['picture'] = $img;
        }


        // foreach ($arrayRequests as $key => $value) {
        //   // code...
        //   error_log(($key."::".$value));
        // }

        // Test add user to the System
        // $verified = array(
        //   "appId" => "78huh7hgbybhyujuhh8uh8iuh8i",
        // 	"username" => "tester02",
        // 	"email" => "tester02@gmail.com",
        // 	"password" => "tester1990",
        // 	"passport_id" => "1234567890123",
        // 	"first_name" => "test02",
        // 	"last_name" => "tester02",
        // 	"sex" => "M",
        // 	"picture" => ""
        // );

        $createUser = User::create($arrayRequests);
        // echo "<pre>";
        // print_r($createUser);
        // echo "</pre>"
        if ($createUser) {
            $msg = 'Register Success';
            $users = new User();
            $users->sex = $createUser->sex;
            $gender = $users->sex();
            $imgUrl = url('core/storage/app/images/passportID/thumbnail');

            $response = [
                'appId' => $appId,
                'userId' => $createUser->id,
                'username' => $createUser->username, //username
                'email' => $createUser->email,
                'fullName' => $createUser->full_name,
                'phone' => $createUser->phone,
                'sex' => $gender,
                'picture' => $imgUrl.'/'.$createUser->picture,
                'message' => $msg
            ];
        }
        // $registerMember = User::create($arrayRequests);

        // return (new HttpResource ($response))
        //     ->response()
        //     // ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode(200);
    }


    public function resetPasswordMember(Request $request)
    {
        $headerResource = new HeaderResource();
        $headerBearer = $headerResource->getBearerToken();
        $response = [];
        $msg = "";
        $arrayRequests = $request->all();
        $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $username = (isset($arrayRequests['username']))? $arrayRequests['username']:"";
        $email = (isset($arrayRequests['email']))? $arrayRequests['email']:"";
        $oldPassword = (isset($arrayRequests['oldPassword']))? $arrayRequests['oldPassword']:"";
        $password = (isset($arrayRequests['password']))? $arrayRequests['password']:"";
        $confirmPassword = (isset($arrayRequests['confirmPassword']))? $arrayRequests['confirmPassword']:"";
        $passwordEncrypt = password_hash($password, PASSWORD_BCRYPT);
        // $arrayRequests['username'] = $username;
        // $arrayRequests['email'] = $email;
        // $arrayRequests['password'] = $password;
        // $arrayRequests['password_encrypt'] = $password_encrypt;
        // echo "<pre>";
        // print_r($arrayRequests);
        // echo "</pre>";
        // echo $appId;
        // echo "<br />";

        $getUsers = User::where(function ($q) use ($username, $email) {
            if ($username != "") {
                return $q->where('username', $username);
            } elseif ($email != "") {
                return $q->where('email', $email);
            }
        })->get();
        $getUser = json_decode(json_encode($getUsers));
        // echo "<pre>";
        // print_r($getUser);
        // echo "</pre>";
        if (!empty($getUser)) {
            if ($password == $confirmPassword) {
                if (password_verify($oldPassword, $getUser[0]->password)) {
                    $users = new User();
                    $users->sex = $getUser[0]->sex;
                    $gender = $users->sex();
                    $imgUrl = url('core/storage/app/images/passportID/thumbnail');
                    $resetPass = User::where(function ($q) use ($username, $email, $passwordEncrypt) {
                        if ($username != "") {
                            return $q->where('username', $username);
                        } elseif ($email != "") {
                            return $q->where('email', $email);
                        }
                    })->update(['password' => $passwordEncrypt]);
                    // echo "<pre>";
                    // print_r($resetPass);
                    // echo "</pre>";
                    $msg = "Reset Password Success";
                    if ($resetPass) {
                        $response = [
                            'appId' => $appId,
                            'userId' => $getUser[0]->id,
                            'username' => $getUser[0]->username,
                            'email' => $getUser[0]->email,
                            'fullName' => $getUser[0]->full_name,
                            'phone' => $getUser[0]->phone,
                            'sex' => $gender,
                            'picture' => $imgUrl.'/'.$getUser[0]->picture,
                            'message' => $msg
                        ];
                    }
                } else {
                    $msg = "Old password is incorrect";
                    $response = ['message' => $msg];
                    return (new HttpResource($response))
                        ->response($msg)
                        ->setStatusCode(400);
                }
            } else {
                $msg = "Password not match";
                $response = ['message' => $msg];
                return (new HttpResource($response))
                    ->response($msg)
                    ->setStatusCode(400);
            }
        } else {
            if ($username == "" && $email == "") {
                $msg = "Username or Email should not be empty";
            } else {
                $msg = "Username or Password is incorrect";
            }
            $response = ['message' => $msg];
            return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode(400);
        }

        // return (new HttpResource ($response))
        //     ->response()
        //     // ->setStatusCode(200)
        //     ->header('X-token', $headerBearer);
        return (new HttpResource($response))
                ->response($msg)
                ->setStatusCode(200);
    }


    //=================================== Mobile Register Controller ===================================
    // 1. Validate registeration request
    public function dataValidation($arrayRequests)
    {
        // $appId = (isset($arrayRequests['appId']))? $arrayRequests['appId']:"";
        $username = (isset($arrayRequests['username']))? $arrayRequests['username']:null;
        $email = (isset($arrayRequests['email']))? $arrayRequests['email']:null;
        // $phone = (isset($arrayRequests['phone']))? $arrayRequests['phone']:"";
        $confirmPassword = (isset($arrayRequests['confirmPassword']))? $arrayRequests['confirmPassword']:null;
        $password = (isset($arrayRequests['password'])&&$arrayRequests['password']==$arrayRequests['confirmPassword'])? $arrayRequests['password']:null;
        // $passportId = (isset($arrayRequests['passportId']))? $arrayRequests['passportId']:"";
        $first_name = (isset($arrayRequests['first_name']))? $arrayRequests['first_name']:null;
        $last_name = (isset($arrayRequests['last_name']))? $arrayRequests['last_name']:null;
        // $sex = (isset($arrayRequests['sex']))? $arrayRequests['sex']:"";
        $news_agreement = (isset($arrayRequests['news_agreement']))? $arrayRequests['news_agreement']:null;

        if (isset($username)&&isset($email)&&isset($password)&&isset($confirmPassword)&&isset($first_name)&&isset($last_name)&&isset($news_agreement)) {
            // dd($username);
          $valid = [
            'username' => $username,
            'password'=> $password,
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'news_agreement' => $news_agreement
          ];
        } else {
            $valid = false;
        }
        return $valid;
    }


    // 2. Check users duplicate
    public function duplicateChecker($user)
    {
        if (\App\Model\Register::where('username', $user['username'])->exists()) {
            // printf("Username already exists.");
            $resp = 409;
        } else {
            // printf("Miss");
            $resp = 200;
        }
        return $resp;
    }


    // 3. Create register
    public function createRegister($user)
    {
        // dd(Hash::make($user['password']));
        // dd($user);
      if (\App\Model\Register::create([
        'username' => $user['username'],
        'password' => Hash::make($user['password']),
        'email' => $user['email'],
        'first_name' => $user['first_name'],
        'last_name' => $user['last_name'],
        'news_agreement' => $user['news_agreement']
      ])) {
            $response = 201;
        } else {
            $response = 409;
        }
        // dd($write_to_db);
        return $response;
    }


    // 4. Write logs
    public function mobileUsersLogsWriter($username, $activity)
    {
      \App\Model\UsersLog::create([
      'username' => $username,
      'user_type' => 'register',
      'activity' => $activity
    ]);
    }


    // Main controller
    public function regist(Request $request)
    {
        // dd($request);
        $validate_state = self::dataValidation($request);
        $check_duplicate_state = self::duplicateChecker($validate_state);
        if ($check_duplicate_state==200) {
            $creation_state = self::createRegister($validate_state);
            self::mobileUsersLogsWriter($validate_state['username'], "[".$validate_state['username']."] Regist to system.");
            $msg = "Register complete.";
            $status_code = $creation_state;
        } else {
            $msg = "Username already exists.";
            $status_code = 409;
        }

        $response = [
          'message' => $msg
        ];
        return (new HttpResource($response))
              ->response($msg)
              ->setStatusCode($status_code);
    }


    public function verifyUser(Request $_request)
    {
      $request = $_request->all();

      $user_in_register_tbl = Register::where('username',$request['username'])->first();
      if($user_in_register_tbl){
        User::create([
          'username' => $user_in_register_tbl['username'],
          'email' => $user_in_register_tbl['email'],
          'first_name' => $user_in_register_tbl['first_name'],
          'last_name' => $user_in_register_tbl['last_name'],
          'news_agreement' => $user_in_register_tbl['news_agreement'],
          'first_name' => $user_in_register_tbl['first_name'],
          'password' => $user_in_register_tbl['password'],

          'id_number' =>  $request['information']['passport_id'],
          'sex' => (($request['information']['gender'] == 'male')? 'M':'F'),
          'phone' => $request['information']['phone_number'],
          'dob' => $request['information']['dob'],
          'address' => $request['information']['address'],
          'picture' => "",
          'user_type' => "guest",
          'country' => $request['information']['country'],
          'state' => $request['information']['state'],
          'postal' => $request['information']['zip_code'],
          'city' => $request['information']['city']
        ]);
        $user_in_register_tbl->type = 'member';
        $user_in_register_tbl->save();
        // $user_in_register_tbl->delete();

        $response = [
          "data" => $user_in_register_tbl,
        ];
        $status_code = 200;
      }else{
        $response = [
          "message" => "User not register.",
        ];
        $status_code = 400;
      }
      return (new HttpResource($response))
              ->response($response)
              ->setStatusCode($status_code);;
    }
}
