<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
  public function VisaCallBack(Request $request)
  {
    dd($request->all());
  }
  public function VisaNotify(Request $request)
  {
    dd($request->all());
  }
  public function UnionCallBack(Request $request)
  {
    dd($request->all());
  }
  public function UnionNotify(Request $request)
  {
    dd($request->all());
  }
}
