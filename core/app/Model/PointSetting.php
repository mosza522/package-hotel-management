<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PointSetting extends Model
{
  protected $table	= 'point_setting';
  public $timestamps 	= false;
  protected $fillable = [
      'volume'
  ];
}
