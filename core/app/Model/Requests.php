<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
  protected $table	= 'requests';
  public $timestamps 	= false;
  protected $fillable = [
    'request_sender',
    'post_id',
    'content',
    'status',
    'create_at',
    'transaction_id'
  ];
}
