<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    public $timestamps = false;

    // // To Ref. the table in the DB.
    // // Test API check room available
    // protected $table = 'room_handle';
    // protected $fillable = [
    //     'user_id', 'date', 'guest_name', 'status'
    // ];


    // Test API booking rooms.
    protected $table = "room_handle";
    protected $fillable = [
      'user_id',
      'date',
      'guest_name',
      'status'
    ];
}
