<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ENews extends Model
{
  protected $table	= 'e_news';
  public $timestamps 	= true;
  protected $fillable = [
      'subject', 'message', 'header', 'to_user'
  ];
  public function attach_path()
  {
      if ($this->attach !== null){
        return asset('assets/backend/attach'.$this->attach);
      }
  }
}
