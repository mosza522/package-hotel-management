<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoomHandle extends Model
{
  protected $table = 'room_handle';
  public $timestamps = false;
  protected $fillable = [
      'user_id',
      'transaction_id',
      'transaction_type',
      'package_id',
      'guest_name',
      'status',
      'post_status'
  ];
}
