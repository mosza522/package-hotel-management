<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WarningLog extends Model
{
    public $timestamps = true;
}
