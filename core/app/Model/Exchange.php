<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
  protected $table = 'exchange';
  public $timestamps = false;
  protected $fillable = [
      'from_user_id',
      'to_user_id',
      'transaction_id'
  ];
}
