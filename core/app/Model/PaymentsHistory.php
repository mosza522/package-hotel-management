<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentsHistory extends Model
{
  protected $table	= 'payments_history';
  // public $timestamps 	= false;
  protected $fillable = [
      'user_id',
      'amount',
      'transaction_id'
  ];
}
