<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CarTransfer extends Model
{
  protected $table	= 'cars_transfers';
  public $timestamps 	= false;
  protected $fillable = [
      'van_price','saloon_price', 'van_description', 'saloon_description', 'van_picture' , 'saloon_picture'
  ];
}
