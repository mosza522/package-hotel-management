<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
  protected $table	= 'promotion';
  public $timestamps 	= true;
  protected $fillable = [
      'news_title', 'content', 'picture', 'status','price', 'expire_date', 'duration', 'discount', 'special_price', 'agreement'
  ];
}
