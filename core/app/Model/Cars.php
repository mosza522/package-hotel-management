<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
  protected $table	= 'Cars';
  public $timestamps 	= false;
  protected $fillable = [
      'color', 'plate', 'model', 'brand', 'seats', 'luggage'
  ];
}
