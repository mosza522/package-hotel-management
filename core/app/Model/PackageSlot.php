<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PackageSlot extends Model
{
  protected $table = "package_slot";
  public $timestamps = false;
}
