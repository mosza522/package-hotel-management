<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentDeposite extends Model
{
    protected $table = "payment_deposite";
    public $timestamps = false;
}
