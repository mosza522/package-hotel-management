<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  protected $table	= 'posts';
  // public $timestamps 	= false;
  protected $fillable = [
      'title',
      'image',
      'thumb',
      'details',
      'agreements',
      'status',
      'hit',
      'user_id',
      'content',
      'post_type',
      'transaction_id'
  ];
}
