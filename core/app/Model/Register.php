<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Register extends Authenticatable
{
  use Notifiable,HasApiTokens;
  protected $guard = 'api-register';
  protected $table = "register";
  public $timestamps = false;
  protected $fillable = [
      'username',
      'email',
      'password',
      'first_name',
      'last_name',
      'news_agreement'
  ];
  protected $hidden = [
         'remember_token',
    ];
  public function AauthAcessToken(){
    return $this->hasMany('\App\OauthAccessToken');
  }
}
