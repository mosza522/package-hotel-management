<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Trexology\Pointable\Contracts\Pointable;
use Trexology\Pointable\Traits\Pointable as PointableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,PointableTrait;
    protected $appends =['full_name'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'first_name',
        'last_name',
        'phone',
        'dob',
        'address',
        'sex',
        'picture',
        'id_type',
        'id_number',
        'point',
        'city',
        'news_agreement',
        'user_type',
        'country',
        'state'
    ];
    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
    public function picture_path()
    {
        if ($this->picture === null) {
            return asset('assets/backend/image/no-img.png');
        }
        return asset('assets/backend/image/guest/pic/'.$this->picture);
    }
    public function id_card_path()
    {
        if ($this->picture === null) {
            return asset('assets/backend/image/no-img.png');
        }
        return asset('assets/backend/image/guest/card_image/'.$this->id_card_image);
    }
    public function receipt_path()
    {
        if ($this->picture_receipt === null) {
            return asset('assets/backend/image/no-img.png');
        }
        return asset('assets/backend/image/guest/receipt/'.$this->picture_receipt);
    }
    public function sex()
    {
        if ($this->sex === 'M') {
            return 'Male';
        }
        if ($this->sex === 'F') {
            return 'Female';
        }
        if ($this->sex === 'O') {
            return 'Other';
        }
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 'password', 'remember_token',
         'remember_token',
    ];
    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'user_id');
    }
    public function payment()
    {
        return $this->hasMany(Payment::class, 'user_id')->where('status', 1);
    }
    public function AauthAcessToken(){
      return $this->hasMany('\App\OauthAccessToken');
    }
}
