<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransferDetail extends Model
{
  protected $table	= 'transfer_detail';
  public $timestamps 	= false;
  protected $fillable = [
      'title', 'image', 'detail', 'status','price'
  ];
}
