<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  protected $table	= 'News';
  public $timestamps 	= true;
  protected $fillable = [
      'news_title', 'picture', 'content', 'status', 'expire_date'
  ];
}
