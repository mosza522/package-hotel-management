<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoomsKeeper extends Model
{
    //
    protected $table = "rooms_keeper";
    public $timestamps = false;
    protected $fillable = [
      'total_rooms',
      'all_rooms'
    ];
}
