<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsersLog extends Model
{
    protected $table = "users_log";
    public $timestamps = false;
    protected $fillable = [
      'username',
      'user_type',
      'activity'
    ];
}
