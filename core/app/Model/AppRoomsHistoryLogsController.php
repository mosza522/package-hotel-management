<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppRoomsHistoryLogsController extends Model
{
    //
    protected $table = 'rooms_history_logs';
    public $timestamps = false;
    protected $fillable = [
        'user_id', 'activity'
    ];
}
