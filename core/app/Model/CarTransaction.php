<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CarTransaction extends Model
{
  protected $table	= 'cars_transaction';
  public $timestamps 	= false;
  protected $fillable = [
      'user_id',
      'car_type',
      'time',
      'flight_number',
      'arrival_time',
      'payment_id',
      'airlines',
      'another_contact'
  ];
}
