<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      // ini_set('max_execution_time', 160);
      // set_time_limit(300);
      $data = $this->data;
      // dd($data);
      if($data['attach'] !== null){
        return $this->view('mail',$data)
              ->subject($data['detail']['subject'])
              ->from($data['detail']['header'])
              ->attach($data['attach']->getRealPath(), array(
                    'as' => $data['attach']->getClientOriginalName(),
                )
              );
      }
      return $this->view('mail',$data)
            ->subject($data['detail']['subject'])
            ->from($data['detail']['header']);

        // return $this->from()
        // ->subject('test subject')
        // ->view('xxxx')
        // ->with('data',)
    }
}
