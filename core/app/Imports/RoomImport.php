<?php

namespace App\Imports;

use App\Model\Room;
use Maatwebsite\Excel\Concerns\ToModel;

class RoomImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Room([
            'number' => $row[0],
            'floor_id' => $row[1],
            'room_type_id' => $row[2],
            'status' => $row[3]=='on' ?'1' :'0'
        ]);
    }
}
