<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Floor;
use App\Model\Reservation;
use App\Model\ReservationNight;
use App\Model\RoomType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\RoomHandle;
use App\Http\Controllers\Controller;
use App\Model\WarningLog;
use App\Mail\sendMail;
use Illuminate\Support\Facades\Mail;

class sendMailToMember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendMailToMember:Daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminer for confirm stay';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $user = User::all();
        $userid = [];
        // $dob = "0000-".Carbon::parse($user[0]->dob)->format('m-d');
        foreach ($user as $key => $value) {
          $dob = Carbon::parse("0000-".Carbon::parse($value->dob)->format('m-d'));
          $now = Carbon::parse("0000-".Carbon::now()->format('m-d'));
          // dd($dob->diffInDays($now));
          if($dob->diffInDays($now) <= 30 && (int)$dob->format('m') > (int) $now->format('m') && $value->dob!=""){
            array_push($userid,$value->id);
          }
        }

        $data['birthday30'] = User::whereIn('id',$userid)->get();
        $user_warning=[];
        $check=false;
        $roomhandle = RoomHandle::where('status','=','reserved')
        ->where('slot_id','!=','')
        ->distinct()
        ->get(['slot_id']);
        foreach ($roomhandle as $key => $room_slot) {
          $value = RoomHandle::where('slot_id','=',$room_slot->slot_id)
          ->leftJoin('package_masters','room_handle.package_id','=','package_masters.id')
          ->first();
          //save only slot
          $warning = WarningLog::where('slot_id',$value->slot_id)->first();
          if( Carbon::parse($value->date)->diffInDays(Carbon::now()) <=60
          && Carbon::parse($value->date)>Carbon::now()
          && $warning
          ){
            if($warning->warning == "90"){
              $user = User::find($value->user_id);
              $data_mail = [
                'user'=>$user,
                'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
                'attach'=>null
              ];
              $mail = new sendMail($data_mail);
              Mail::to($user->email)->send($mail);
              $warning->warning = '60';
              $warning->save();
              $check=true;
              array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
            }
          }elseif (Carbon::parse($value->date)->diffInDays(Carbon::now()) <=90
          && Carbon::parse($value->date)->diffInDays(Carbon::now()) >60
          && Carbon::parse($value->date)>Carbon::now()
          && $warning
          ) {
            if($warning->warning == "180"){
              $user = User::find($value->user_id);
              $data_mail = [
                'user'=>$user,
                'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
                'attach'=>null
              ];
              $mail = new sendMail($data_mail);
              Mail::to($user->email)->send($mail);
              $warning->warning = '90';
              $warning->save();
              $check=true;
              array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
            }
          }elseif (Carbon::parse($value->date)->diffInDays(Carbon::now()) <=180
          && Carbon::parse($value->date)->diffInDays(Carbon::now()) >90
          && Carbon::parse($value->date)>Carbon::now()
          ) {
              $user = User::find($value->user_id);
              $data_mail = [
                'user'=>$user,
                'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
                'attach'=>null
              ];
              $mail = new sendMail($data_mail);
              Mail::to($user->email)->send($mail);
              $warning= new WarningLog;
              $warning->user_id = $value->user_id;
              $warning->warning = '180';
              $warning->slot_id = $value->slot_id;
              $warning->save();
              $check=true;
              array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
          }elseif(Carbon::parse($value->date)->diffInDays(Carbon::now()) <=90
          && Carbon::parse($value->date)->diffInDays(Carbon::now()) >60
          && Carbon::parse($value->date)>Carbon::now()){
            $user = User::find($value->user_id);
            $data_mail = [
              'user'=>$user,
              'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
              'attach'=>null
            ];
            $mail = new sendMail($data_mail);
            Mail::to($user->email)->send($mail);
            $warning= new WarningLog;
            $warning->user_id = $value->user_id;
            $warning->warning = '90';
            $warning->slot_id = $value->slot_id;
            $warning->save();
            $check=true;
            array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
          }elseif(Carbon::parse($value->date)->diffInDays(Carbon::now()) <=60
          && Carbon::parse($value->date)>Carbon::now()
        ){
              $user = User::find($value->user_id);
              $data_mail = [
                'user'=>$user,
                'detail'=>['message'=>'เรียนสมากชิก PISONA กรุณายืนยันการเข้าพักเพื่อสิทธิประโยชน์สูงสุดของท่าน','subject'=>'กรุณายืนยันการเข้าพัก','header'=>'no-reply@pisona.com'],
                'attach'=>null
              ];
              $mail = new sendMail($data_mail);
              Mail::to($user->email)->send($mail);
              $warning= new WarningLog;
              $warning->user_id = $value->user_id;
              $warning->warning = '60';
              $warning->slot_id = $value->slot_id;
              $warning->save();
              $check=true;
              array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
            }
            if(Carbon::parse($value->date)->diffInDays(Carbon::now()) <=180
            && Carbon::parse($value->date)>Carbon::now() && !$check){
              $user = User::find($value->user_id);
              array_push($user_warning,array("user_data"=>$user,'slot_data'=>$value,'diff_date'=>Carbon::parse($value->date)->diffInDays(Carbon::now())));
            }
          }
        echo "done";
    }
}
